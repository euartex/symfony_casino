<?php

namespace Casino\NewsBundle\Entity;

use Doctrine\ORM\EntityRepository;

class NewsRepository extends EntityRepository
{
    public function findBySlug($slug)
    {
        return $this->getEntityManager()
            ->getRepository('CasinoNewsBundle:News')
            ->createQueryBuilder('n')
            ->setParameter('slug',$slug)
            ->where('n.slug = :slug')
            ->getQuery()->getSingleResult();
    }
}