<?php

namespace Casino\NewsBundle\Controller;

use Casino\DefaultBundle\Controller\BaseAdminController;
use APY\DataGridBundle\Grid\Source\Entity;
use APY\DataGridBundle\Grid\Action\RowAction;
use Symfony\Component\HttpFoundation\Request;

class DefaultAdminController extends BaseAdminController
{
    public function indexAction()
    {
        $source = new Entity('CasinoNewsBundle:News');
        $grid = $this->get('grid');
        $grid->setSource($source);
        $grid->setId('id');
        $grid->setLimits([5, 10, 15]);

        $grid->hideColumns(
            ['seo', 'description', 'content', 'photo']
        );

        $rowAction = new RowAction('Edit', 'casino_admin_news_edit');
        $rowAction->setRouteParameters(['slug']);
        $grid->addRowAction($rowAction);

        if ($grid->isReadyForRedirect()) {
            return $grid->getGridResponse();
        }

        return $this->render(
            'CasinoNewsBundle:DefaultAdmin:index.html.twig',
            ['grid' => $grid]
        );
    }

    public function editAction(Request $request, $slug=null)
    {
        $em = $this->getDoctrine()->getManager();
        if ($slug) {
            /**
             * @var \Casino\NewsBundle\Entity\News
             */
            $news = $em->getRepository('CasinoNewsBundle:News')
                ->findBySlug($slug);
        } else {
            $news = new \Casino\NewsBundle\Entity\News();
        }

        $form = $this->createFormBuilder($news)
            ->add('slug', 'text')
            ->add('file')
            ->add('name', 'text')
            ->add('description', 'text', ['required'=>true])
            ->add('content', 'text')
            ->add('locale', 'text')
            ->add(
                'is_active',
                'checkbox'
            )
            ->getForm();

        $form->handleRequest($request);
        if ($form->isValid()) {
            //$newsData = $form->getData();
            $news->upload();
            $em->persist($news);
            $em->flush();

            return $this->redirect(
                $this->generateUrl(
                    'casino_admin_news_edit',
                    ['slug' => $news->getSlug()]
                )
            );
        }

        return $this->render(
            'CasinoNewsBundle:DefaultAdmin:edit.html.twig',
            ['form' => $form->createView(), 'obj' => $news]
        );
    }
}
