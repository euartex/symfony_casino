<?php

namespace Casino\NewsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('CasinoNewsBundle:Default:index.html.twig', []);
    }
}
