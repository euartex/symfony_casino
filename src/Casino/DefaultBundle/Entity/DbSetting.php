<?php

namespace Casino\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DbSetting
 *
 * @ORM\Table(
 *  name="db_setting"
 * )
 * @ORM\Entity
 */
class DbSetting
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @ORM\Id
     */
    private $name = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="text", nullable=true)
     */
    private $value;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return DbSetting
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return DbSetting
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }
}
