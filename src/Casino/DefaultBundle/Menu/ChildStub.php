<?php

namespace Casino\DefaultBundle\Menu;

/**
 * Used only to simulate setAttribute calls when rights are not granted
 * @package Casino\DefaultBundle\Menu
 */
class ChildStub
{
    public function setAttribute($name, $value)
    {
        return $this;
    }
}
