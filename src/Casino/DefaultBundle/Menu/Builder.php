<?php

namespace Casino\DefaultBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class Builder extends ContainerAware
{
    public function adminMenu(FactoryInterface $factory, array $options)
    {
        $authChecker = $this->container->get('security.authorization_checker');
        $request = $this->container->get('request');
        $routeName = $request->get('_route');

        /**
         * @var \Knp\Menu\ItemInterface
         */
        $menu = $factory->createItem('root');

        $menu->setChildrenAttribute('class', 'nav navbar-nav');

        $menu->addChild('Dashboard', ['route' => 'casino_admin_homepage'])
            ->setAttribute('icon', 'icon icon-home');

//        if ($authChecker->isGranted('ROLE_ADMIN') || $authChecker->isGranted('ROLE_CONTENT_MANAGER')) {

        $menu->addChild('Users')
            ->setAttribute('dropdown', true)
            ->setAttribute('icon', 'icon icon-users');

        $menu['Users']->addChild('Users', ['route' => 'casino_admin_users'])
            ->setAttribute('icon', 'icon icon-users');

        $menu['Users']->addChild('Users documents', ['route' => 'docs_admin_index'])
            ->setAttribute('icon', 'icon icon-users');

        $menu['Users']->addChild('Users sessions', ['route' => 'casino_admin_sessions'])
            ->setAttribute('icon', 'icon icon-users');

        $menu['Users']->addChild('Groups', ['route' => 'casino_admin_group'])
            ->setAttribute('icon', 'icon icon-tag');

        $menu['Users']->addChild('Mailing', ['route' => 'casino_admin_mailing'])
            ->setAttribute('icon', 'icon icon-envelope');

//        if ($securityContext->isGranted('ROLE_CONTENT_MANAGER')) {
//            $menu->addChild('News', ['route' => 'casino_admin_news'])
//                ->setAttribute('icon', 'fa fa-comments');
//
//            $menu->addChild('Pages', ['route' => 'admin_page'])
//                ->setAttribute('icon', 'icon icon-docs');
//        }

        $menu->addChild('Games')
            ->setAttribute('dropdown', true)
            ->setAttribute('icon', 'fa icon-control-play');

        $menu->addChild('Pages', ['route' => 'admin_page'])
            ->setAttribute('dropdown', false)
            ->setAttribute('icon', 'fa icon-docs');

        $menu['Games']->addChild('Games', ['route' => 'admin_game'])
            ->setAttribute('icon', 'icon icon-control-play');

        $menu['Games']->addChild('Game logs', ['route' => 'admin_gamelog'])
            ->setAttribute('icon', 'icon icon-control-play');

        $menu['Games']->addChild('Winning Statistics', ['route' => 'admin_winning_statistics'])
            ->setAttribute('icon', 'icon icon-bar-chart');

        $menu->addChild('Bonuses')
            ->setAttribute('dropdown', true)
            ->setAttribute('icon', 'icon icon-control-play');

        $menu['Bonuses']->addChild('Bonuses', ['route' => 'admin_bonus_bonus'])
            ->setAttribute('icon', 'icon icon-control-play');
        $menu['Bonuses']->addChild('Promocodes', ['route' => 'promocode-admin'])
            ->setAttribute('icon', 'icon icon-control-play');

        $menu['Bonuses']->addChild('Give bonus', ['route' => 'admin_bonus_give'])
            ->setAttribute('icon', 'icon icon-users');

        $menu->addChild('Transactions')
            ->setAttribute('dropdown', true)
            ->setAttribute('icon', 'fa fa-usd');

        $menu['Transactions']->addChild('Deposits', ['route' => 'admin_treasury_deposit'])
            ->setAttribute('icon', 'icon icon-login')
            ->setAttribute('divider_append', true);
        $menu['Transactions']->addChild('Transactions', ['route' => 'user_treasury_transactions'])
            ->setAttribute('icon', 'icon icon-login')
            ->setAttribute('divider_append', true);

        $menu->addChild('Settings')
            ->setAttribute('dropdown', true)
            ->setAttribute('icon', 'icon icon-wrench');

        $menu['Settings']->addChild('Treasury', ['route' => 'casino_treasury_admin_setting'])
            ->setAttribute('icon', 'fa fa-usd')
            ->setAttribute('divider_append', false);

        $menu['Settings']->addChild('Mailing', ['route' => 'casino_admin_mailing'])
            ->setAttribute('icon', 'icon icon-envelope')
            ->setAttribute('divider_append', false);

        $menu['Settings']->addChild('Mail templates', ['route' => 'email-template'])
            ->setAttribute('icon', 'icon icon-envelope')
            ->setAttribute('divider_append', false);

        $menu['Settings']->addChild(
            'Winning Statistics Notifications',
            ['route' => 'casino_admin_winning_statistics_email']
        )
            ->setAttribute('icon', 'icon icon-envelope')
            ->setAttribute('divider_append', false);

        $menu['Settings']->addChild('Main page marquee settings', ['route' => 'casino_admin_site_preferences'])
            ->setAttribute('icon', 'fa fa-exchange')
            ->setAttribute('divider_append', false);

        $menu['Settings']->addChild('Main page slider', ['route' => 'casino_media_slider'])
            ->setAttribute('icon', 'fa fa-exchange')
            ->setAttribute('divider_append', false);

        if (!count($menu['Settings']->getChildren())) {
            $menu->removeChild('Settings');
        }
//        }

//        if ($authChecker->isGranted('ROLE_ADMIN')) {
        $menu['Users']->addChild('Administrators', ['route' => 'casino_admin_administrators'])
            ->setAttribute('icon', 'icon icon-energy');
        $menu['Transactions']->addChild('Payouts', ['route' => 'admin_withdraw'])
            ->setAttribute('icon', 'icon icon-logout')
            ->setAttribute('divider_append', true);
//        }
//        if ($authChecker->isGranted('ROLE_SUPER_ADMIN')) {
        $menu['Users']->addChild('Admin roles', ['route' => 'casino_admin_roles'])
            ->setAttribute('icon', 'icon icon-energy');
        $keyList = [
            'Transactions',
            'Users',
            'Settings',
            'Bonuses',
            'Games',
            'Pages',
        ];
        foreach ($keyList as $key) {
            if (isset($menu[$key]) && !count($menu[$key]->getChildren())) {
                $menu->removeChild($key);
            }
        }
//        }

        switch ($routeName) {
            case 'casino_admin_news_edit':
                $menu->getChild('News')->setCurrent(true);
                break;
            
            case 'casino_admin_users_edit':
            case 'casino_admin_group_edit':
            case 'casino_admin_group_add':
            case 'casino_admin_users':
            case 'casino_admin_group':
            case 'casino_admin_administrators':
            case 'casino_admin_administrators_edit':
            case 'casino_admin_mailing':
                $menu->getChild('Users')->setCurrent(true);
                break;
            case 'admin_page_new':
            case 'admin_page_create':
            case 'admin_page_edit':
            case 'admin_page_update':
                $menu->getChild('Pages')->setCurrent(true);
            break;
            case 'admin_game_new':
            case 'admin_game_create':
            case 'admin_game_edit':
            case 'admin_game_update':
                $menu->getChild('Games')->setCurrent(true);
                break;
        }

        return $menu;
    }
}
