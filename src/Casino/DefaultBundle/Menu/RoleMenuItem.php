<?php

namespace Casino\DefaultBundle\Menu;

use Knp\Menu\ItemInterface;
use Knp\Menu\MenuItem;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Overriding MenuItem
 */
class RoleMenuItem extends MenuItem implements ItemInterface
{
    use ContainerAwareTrait;

    public function addChild($child, array $options = array())
    {
        $accessManager = $this->container->get('casino.access_manager');
        if (!isset($options['route']) || $accessManager->isUserAllowed($options['route'])) {
            return parent::addChild($child, $options);
        } else {
            return new ChildStub();
        }
    }
}
