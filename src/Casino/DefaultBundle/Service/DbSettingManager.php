<?php

namespace Casino\DefaultBundle\Service;

use Casino\DefaultBundle\Entity\DbSetting;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class DbSettingManager
 * @package Casino\DefaultBundle\Service
 */
class DbSettingManager
{
    use ContainerAwareTrait;

    /**
     * @param string $name
     * @return string
     */
    public function get($name)
    {
        $dbSettingRepo = $this->container->get('doctrine')->getRepository('CasinoDefaultBundle:DbSetting');
        /** @var DbSetting $entry */
        $entry = $dbSettingRepo->findOneBy(['name' => $name]);

        return $entry ? $entry->getValue() : '';
    }

    /**
     * @param string $name
     * @param string $value
     * @return DbSettingManager
     */
    public function set($name, $value)
    {
        $manager = $this->container->get('doctrine')->getManager();
        $dbSettingRepo = $manager->getRepository('CasinoDefaultBundle:DbSetting');
        /** @var DbSetting $entry */
        if (!$entry = $dbSettingRepo->findOneBy(['name' => $name])) {
            $entry = new DbSetting();
            $entry->setName($name)
                ->setValue($value);
        }
        $entry->setValue($value);
        $manager->persist($entry);
        $manager->flush();

        return $this;
    }
}
