<?php

namespace Casino\DefaultBundle\Service;

use Casino\UserBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Check if current user is allowed to access given/current route
 * @package Casino\DefaultBundle\Service
 */
class AccessManager
{
    use ContainerAwareTrait;

    /**
     * @param string $route
     * @return bool
     */
    public function isUserAllowed($route = null)
    {
        /** @var User $user */
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $request = $this->container->get('request');
        $dbSettingService = $this->container->get('casino.db_setting_manager');
        $rolesString = $dbSettingService->get('roles');
        $roles = unserialize($rolesString);
        $userRoles = $user->getRoles();
        $currentRoute = $route ?: $request->get('_route');
        if (in_array('ROLE_SUPER_ADMIN', $userRoles)) {
            return true;
        }
        foreach ($userRoles as $currentRole) {
            if (isset($roles[$currentRole])) {
                foreach ($roles[$currentRole] as $routeKey => $item) {
                    // handle multiple-route keys like 'admin_user_edit,admin_user_update'
                    foreach (explode(',', $routeKey) as $singleRoute) {
                        if ($singleRoute == $currentRoute || $currentRoute == 'casino_admin_homepage') {
                            return true;
                        }
                    }
                }
            }
            // if not exact match was found, looks for wildcard routes
            // example: route 'admin_user_type' will be allowed if 'admin_user*' is allowed
            if (isset($roles[$currentRole])) {
                foreach ($roles[$currentRole] as $route => $value) {
                    if (strpos($route, '*') && strpos(substr($route, 0, -1), $currentRoute) === 0) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
