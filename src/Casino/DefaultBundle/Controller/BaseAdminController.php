<?php

namespace Casino\DefaultBundle\Controller;

use APY\DataGridBundle\Grid\Action\RowAction;
use APY\DataGridBundle\Grid\Column\TextColumn;
use APY\DataGridBundle\Grid\Source\Entity;
use Casino\GameBundle\Entity\Game;
use Casino\GameBundle\Form\GameType;
use Casino\UserBundle\Entity\User;
use Doctrine\ORM\QueryBuilder;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;

/**
 * Base admin controller.
 *
 */
class BaseAdminController extends Controller
{
    public function setContainer(ContainerInterface $container = null)
    {
        parent::setContainer($container);
        if (!$this->container->get('casino.access_manager')->isUserAllowed()) {
            throw $this->createAccessDeniedException('You are not allowed to perform this action.');
        }
    }
}
