<?php

namespace Casino\DefaultBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    public function defaultAction(Request $request)
    {
        return $this->redirect(
            $this->generateUrl(
                'casino_default_homepage'
            )
        );
    }

    public function setLangAction(Request $request, $lang)
    {
        return $this->redirect(
            $this->generateUrl(
                'casino_default_homepage',
                ['_locale'=>$lang]
            )
        );
    }

    public function indexAction(Request $request)
    {
        $marquee = unserialize($this->get('craue_config')->get('spinBar'));
        if (!is_array($marquee)) {
            $marquee = ['switcher'=>true, 'text'=>'***WELCOME TO LOTEDO CASINO***'];
        }

        /** @var array $slidesData */
        $slides = unserialize($this->get('craue_config')->get('main.slider'));

        return $this->render(
            'CasinoDefaultBundle:Default:index.html.twig',
            [
                'dont_show_games_profile' => true,
                'marquee' => $marquee,
                'slides' => $slides,
            ]
        );
    }

    /**
     * @param string $key
     * @return string
     */
    public function textBlockAction($key)
    {
        $em = $this->getDoctrine()->getManager();
        $locale = $this->get('request_stack')->getMasterRequest()->getLocale();
        $content = $em->getRepository('CasinoPagesBundle:Page')->findOneBy(['slug' => $key])->getContent($locale);
        return new Response($content);
    }

    public function loginIndexAction(Request $request)
    {
        $slides = unserialize($this->get('craue_config')->get('main.slider'));
        return $this->render('CasinoDefaultBundle:Default:index.html.twig', ['showLogin'=>true, 'slides'=>$slides]);
    }
}
