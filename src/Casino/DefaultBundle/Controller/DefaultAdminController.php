<?php

namespace Casino\DefaultBundle\Controller;

use Casino\GameBundle\Entity\GameLogAggregation;
use Craue\ConfigBundle\Entity\Setting;
use Symfony\Component\HttpFoundation\Request;

class DefaultAdminController extends BaseAdminController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        return $this->render('CasinoDefaultBundle:DefaultAdmin:index.html.twig', ['name' => 'test']);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function preferencesAction(Request $request)
    {
        $switcherData = unserialize($this->get('craue_config')->get('spinBar'));
        $form = $this->createFormBuilder()
            ->add('switcher', 'checkbox', array('label' => 'Show', 'data' => $switcherData['switcher'], 'required' => false))
            ->add('text', 'text', array('label' => 'Text', 'attr' => ['value' => $switcherData['text']]))
            ->add('submit', 'submit', array('label' => 'Update'))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->get('craue_config')->set('spinBar', serialize($form->getData()));

            $request->getSession()->getFlashBag()->add(
                'notice',
                'Your changes were saved!'
            );

            return $this->redirect($this->generateUrl(
                'casino_admin_site_preferences'
            ));
        }
        return $this->render('CasinoDefaultBundle:DefaultAdmin:prefs.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function rolesAction(Request $request)
    {
        $dbSettingService = $this->container->get('casino.db_setting_manager');
        $routes = $this->getParameter('role_routes');
        $roles = $this->getParameter('security.role_hierarchy.roles');
        unset($roles['ROLE_SUPER_ADMIN']);
        if ($rolesString = $dbSettingService->get('roles')) {
            $roleData = unserialize($rolesString);
        } else {
            $roleData = [];
            $dbSettingService->set('roles', '');
        }
        $formBuilder = $this->createFormBuilder();
        foreach (array_keys($roles) as $key) {
            foreach ($routes as $route) {
                $formBuilder->add($key, 'checkbox', array('label' => $key));
            }
        }
        $form = $formBuilder->getForm();

        $form->handleRequest($request);
        if ($request->isMethod('post')) {
            $roleData = [];
            $formData = $request->get('roles');
            foreach ($formData as $role => $item) {
                foreach ($item as $route => $value) {
                    $roleData[$role][$route] = true;
                }
            }
            $dbSettingService->set('roles', serialize($roleData));

            $request->getSession()->getFlashBag()->add(
                'notice',
                'Your changes were saved!'
            );
        }
        return $this->render(
            'CasinoDefaultBundle:DefaultAdmin:roles.html.twig',
            [
                'form' => $form->createView(),
                'roles' => $roles,
                'roleData' => $roleData,
                'routes' => $routes,
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function winningStatisticsEmailAction(Request $request)
    {
        $dbSettingManager = $this->container->get('casino.db_setting_manager');
        $emailSettings = unserialize($dbSettingManager->get(GameLogAggregation::DB_KEY_SETTINGS));
        $getSetting = function($key) use($emailSettings) {
            return isset($emailSettings[$key]) ? $emailSettings[$key] : false;
        };
        $form = $this->createFormBuilder()
            ->add(
                'enabled',
                'checkbox',
                [
                    'label' => 'Enable notifications',
                    'data' => $getSetting('enabled'),
                    'required' => false,
                ]
            )
            ->add(
                'emails',
                'textarea',
                [
                    'label' => 'Emails (one per line)',
                    'data' => $getSetting('emails'),
                ]
            )
            ->add(
                'threshold',
                'text',
                [
                    'label' => 'Hourly Loosing Threshold (USD)',
                    'attr' => ['value' => $getSetting('threshold')],
                ]
            )
            ->add(
                'emailSubject',
                'text',
                [
                    'label' => 'Email Subject',
                    'attr' => ['value' => $getSetting('emailSubject')],
                ]
            )
            ->add(
                'emailText',
                'textarea',
                [
                    'label' => 'Email Text',
                    'data' => $getSetting('emailText'),
                ]
            )
            ->add(
                'submit',
                'submit',
                ['label' => 'Save']
            )
            ->getForm();

        $form->handleRequest($request);
        if ($form->isValid()) {
            $dbSettingManager->set(GameLogAggregation::DB_KEY_SETTINGS, serialize($form->getData()));

            $request->getSession()->getFlashBag()->add(
                'notice',
                'Your changes were saved!'
            );

            return $this->redirect($this->generateUrl(
                'casino_admin_winning_statistics_email'
            ));
        }

        return $this->render('CasinoDefaultBundle:DefaultAdmin:winningStatisticsEmail.html.twig', ['form' => $form->createView()]);
    }

}
