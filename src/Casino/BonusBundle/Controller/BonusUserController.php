<?php

namespace Casino\BonusBundle\Controller;

use APY\DataGridBundle\Grid\Row;
use APY\DataGridBundle\Grid\Source\Entity;
use Casino\BonusBundle\Entity\Bonuses;
use Casino\TreasuryBundle\Entity\Deposit;
use Casino\TreasuryBundle\Entity\TreasuryDepositBonuses;
use Casino\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;

/**
 * User`s Bonuses controller.
 *
 */
class BonusUserController extends Controller
{

    public function indexAction(){
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if (!is_object($user)) {
            return $this->redirect(
                $this->generateUrl(
                    'fos_user_security_login'
                )
            );
        }

        $em = $this->getDoctrine()->getManager();
        $bonusAccs = $em->getRepository('CasinoTreasuryBundle:TreasuryDepositBonuses')->findBy( array('user' => $user ) );

        $source = new Entity('CasinoTreasuryBundle:TreasuryDepositBonuses');
        $source->setData($bonusAccs);

        $source->manipulateRow(
            function (Row $row)
            {
                $wage = $row->getField('wage');
                $totalWage = $row->getField('totalWage');
                if ( ($wage >0) && ($totalWage > 0)){
                    $wagePercents = round( ( $row->getField('wage') / $row->getField('totalWage') * 100 ), 2, PHP_ROUND_HALF_DOWN);
                } else {
                    $wagePercents = 0;
                }
                $row->setField('wage', $wagePercents);
                return $row;
            }
        );

        $grid = $this->get('grid');
        $grid->setSource($source);
        $column = $grid->getColumn('createdAt');
        $column->setTitle('Creation date');
        $column = $grid->getColumn('isCashedOut');
        $column->setTitle('Cashed out');
        $column = $grid->getColumn('isActive');
        $column->setTitle('Active');
        $column = $grid->getColumn('lifespan');
        $column->setTitle('Expiration date');
        $grid->getColumn('isActive')->manipulateRenderCell(
            function($value, $row) {
                return ($value == 'true')? 'yes': 'no';
            }
        );
        $grid->getColumn('wage')->manipulateRenderCell( function($value) {return $value . '%';});
        $grid->getColumn('amount')->manipulateRenderCell(
            function($value, $row) {
                return $value.'$';
            }
        );
        $grid->getColumn('isCashedOut')->manipulateRenderCell(
            function($value, $row) {
                return ($value == 'true')? 'yes': 'no';
            }
        );
        $grid->setId('id');
        $grid->setLimits([15, 30, 50]);

        $grid->hideColumns(
            [
                'id','totalWage','balanceBefore', 'changedAt'
            ]
        );

        if ($grid->isReadyForRedirect()) {
            return $grid->getGridResponse();
        }
        return $this->render(
            'CasinoTreasuryBundle:Default:index.html.twig',
            ['grid' => $grid, 'title' => 'Your bonuses list']
        );
    }

    /**
     * Finds and displays a Bonus entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $bonus = $em->getRepository('CasinoTreasuryBundle:TreasuryDepositBonuses')->find($id);

        if (!$bonus) {
            throw $this->createNotFoundException('Unable to find Bonus entity.');
        }

        $bonus->rights = $this->getBonusRights($bonus);
        if (in_array($bonus->rights, array('owner', 'admin'))) {

        }

        return $this->render(
            'CasinoBonusBundle:BonusUser:show.html.twig',
            [
                'entity' => $bonus,
            ]
        );
    }


    /**
     * Deactivates bonus.
     *
     */
    public function abandonAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var TreasuryDepositBonuses $bonus */
        $bonus = $em->getRepository('CasinoTreasuryBundle:TreasuryDepositBonuses')->find($id);
        if (!$bonus) {
            throw $this->createNotFoundException('Unable to find Bonus entity.');
        }
        $bonusRights = $this->getBonusRights($bonus);
        if (in_array($bonusRights, array('owner', 'admin'))) {
            $bonus->setIsActive(0);
            $suspendReason = ($bonusRights === 'owner') ? TreasuryDepositBonuses::BY_USER : TreasuryDepositBonuses::BY_ADMIN;
            $bonus->setExpireReason($suspendReason);
            $em->persist($bonus);
            $em->flush();
        }

        return $this->render(
            'CasinoBonusBundle:BonusUser:show.html.twig',
            [
                'entity' => $bonus,
            ]
        );
    }

    public function getBonusRights($userBonusEntity)
    {

        $securityContext = $this->get('security.context');
        $currentUser = $securityContext->getToken()->getUser();

        if (!is_object($currentUser)) {
            return $this->redirect(
                $this->generateUrl(
                    'fos_user_security_login'
                )
            );
            //throw new Exception('user not found');
        }
        $currentUID = $currentUser->getId();

        $bonusUID = $this->getUID($userBonusEntity);
        if ($securityContext->isGranted('ROLE_ADMIN')) {
            $rights = 'admin';
        } elseif ($currentUID === $bonusUID) {
            $rights = 'owner';
        } else {
            $rights = 'none';
        }
        return $rights;
    }

    /**
     * Get UID from entity field or throught related deposit
     *
     * @param $entity object
     * @return mixed
     */
    public function getUID(TreasuryDepositBonuses $userBonusEntity)
    {
        if (!is_object($userBonusEntity->getUser()) && !is_object($userBonusEntity->getDeposit())) {
            throw new \Exception('Userless bonus!');
        }
        $user = (is_object($userBonusEntity->getUser())) ? $userBonusEntity->getUser() : $userBonusEntity->getDeposit()->getUser();
        return $user->getId();
    }

    /*
     * "Change current Bonus to other" Action
     */
    public function pickBonusAction(Request $request)
    {
        /** @var User $user */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if (!is_object($user)) {

            return $this->redirect(
                $this->generateUrl(
                    'fos_user_security_login'
                )
            );
        }

        $em = $this->getDoctrine()->getManager();
        $userBonuses = $user->getAvBonuses();
        $publicBonuses = $em->getRepository('CasinoBonusBundle:Bonuses')->findBy( array('isPublic' => true ) );
        $avialableBonuses = array_merge($userBonuses->toArray(), $publicBonuses);


        /** @var Form $promoForm */
        $promoForm = $this->container->get('form.factory')
            ->createNamedBuilder('promoform', 'form', array(), array())
            ->setMethod('POST')
            ->add('code','text', ['required'=>true, 'label' => 'Enter the code', 'attr' => ['maxlength' => 8]])
            ->add('submit', 'submit', array(
                'label' => 'Redeem code',
                'attr' => array('class' => 'submit button_decoration wide-btn'),
            ))
            ->getForm();

        /** @var Form $bonusForm */
        $bonusForm = $this->container->get('form.factory')->createNamedBuilder('bonusform', 'form', array(), array())
            ->setAction($this->generateUrl('casino_users_get_bonuses'))
            ->setMethod('POST')
            ->add('bonus',
                'entity', array(
                    'label' => 'Select bonus to switch from existing one',
                    //'empty_value' => 'Select Action',
                    'required' => true,
                    'class' => 'CasinoBonusBundle:Bonuses',
                    'property' => 'inputChoise',
                    'choices' => $avialableBonuses,
                    'expanded' => true
                ));

        if (!$user->getCurrentBonus()) {
            $submitLabel = 'Get the bonus';

        } else {
            $submitLabel = 'Switch to binus';
            $bonusForm->add('awareness',
                'checkbox', array(
                    'label'    => 'Do you really want to SWITCH to another bonus?',
                    'label_attr'=>['id'=>'bonus_switch_label'],
                    'required' => true,
                ));
        }

        $bonusForm->add('amount', 'money', array(
                'currency' => 'USD',
                'required' => false,
                'attr' => array(
                    'placeholder' => 'optional for fixed bonus',
                    'class' => 'qwest',
                    'row_class' => 'optional'
                )
            ))
            ->add('switch_to_bonus', 'submit', array(
                'label'=> $submitLabel,
                'attr' => array('class' => 'submit button_decoration wide-btn'),
            ));


        $bonusForm = $bonusForm->getForm();

        //handle promocode form first
        if ($request->request->has('promoform')) {
            $promoForm->handleRequest($request);
            $this->handlePromoForm($promoForm, $user);
        }

        
        //handle bonus form then
        if ($request->request->has('bonusform')) {
            $bonusForm->handleRequest($request);
            if ( $bonusForm->get('amount')->getData() <= 0 && $bonusForm->get('bonus')->getData()->getType() == Bonuses::BONUS_TYPE_FLOAT_AMOUNT) {
                $bonusForm->get('amount')->addError(new FormError('Must be greater than 0'));
            }

            if ($bonusForm->isValid()) {
                $pickedBonusId = intval($request->get('bonusform')['bonus']);
                $pickedAmount = floatval($request->get('bonusform')['amount']);

                /** @var Bonuses $uab */
                foreach ($avialableBonuses as $uab) {
                    if  ( ($pickedBonusId !== 0) && ($pickedBonusId == $uab->getId())) {

                        $bonusType = $uab->getType();
                        if ( $bonusType == Bonuses::BONUS_TYPE_FIXED_AMOUNT ) {
                            $amount = $uab->getAmount();
                            $price = $uab->getPrice();
                        } elseif ( $bonusType == Bonuses::BONUS_TYPE_FLOAT_AMOUNT ) {
                            $amount = $pickedAmount + $pickedAmount * ($uab->getBuyRate() / 100);
                            $price = $pickedAmount;
                        } else {
                            throw new \Exception('Wrong bonus type',404);
                        }

                        if ( $user->getBalance() < $price ) {
                                $bonusForm->get('amount')->addError(new FormError('Insufficient founds!'));
                        } else {

                            $dateNow = new \DateTime();
                            $dateNow->modify("+1 month");

                            $newUserBonus = new TreasuryDepositBonuses();
                            $newUserBonus->setBonus($uab);
                            $newUserBonus->setUser($user);
                            $newUserBonus->setIsActive(true);
                            $newUserBonus->setLifespan($dateNow);
                            $newUserBonus->setAmount($amount);
                            $newUserBonus->setTotalWage($amount * ($uab->getWageRate() / 100));
                            if (is_object($uab->getPromocode())){
                                $newUserBonus->setPromocode($uab->getPromocode());
                            }
                            $em->persist($newUserBonus);

                            //deactivate previous active bonuses
                            $query = $em->getRepository('CasinoTreasuryBundle:TreasuryDepositBonuses')->createQueryBuilder('tdb')
                                ->update('CasinoTreasuryBundle:TreasuryDepositBonuses','tdb')
                                ->set('tdb.isActive', ':ia')   //Here is the change
                                ->set('tdb.expireReason', ':er')   //Here is the change
                                ->where('tdb.user = :user')
                                ->andWhere('tdb.isActive = true')
                                ->setParameter(':user', $user)
                                ->setParameter(':ia', false)
                                ->setParameter(':er', TreasuryDepositBonuses::BY_USER)
                                ->getQuery();
                            $query->execute();

                            $userDeposit = new Deposit();
                            $userDeposit->setBalanceBefore($user->getBalance());
                            $userDeposit->setStatus('completed');
                            $userDeposit->setTo(Deposit::TO_BONUS);
                            $userDeposit->setFrom(Deposit::FROM_REAL);
                            $userDeposit->setAmount($price);
                            $userDeposit->setBalanceAfter($user->getBalance() - $price);
                            $userDeposit->setDetailsJson(json_encode( array('from'=>'user','dest'=> 'bonus', 'id' => $uab->getId() )));
                            $userDeposit->setUser($user);
                            $em->persist($userDeposit);

                            $user->addDeposit($userDeposit);
                            $user->setCurrentBonus($newUserBonus);
                            $user->setBonusBalance($amount);
                            $user->setBalance($user->getBalance() - $price);
                            $user->removeAvBonus($uab);

                            $em->persist($user);
                            $em->flush();
                        }
                        break;
                    }
                }
                return $this->redirect(
                    $this->generateUrl(
                        'casino_users_get_bonuses'
                    )
                );
            }
        }



        $userBonusesHistory = $em->getRepository('CasinoTreasuryBundle:TreasuryDepositBonuses')->findBy( array('user' => $user ), array('id' => 'DESC'), 4 );

        $source = new Entity('CasinoTreasuryBundle:TreasuryDepositBonuses');
        $source->setData($userBonusesHistory);


        $userBonusesHistoryGrid = $this->get('grid');
        $userBonusesHistoryGrid->setSource($source);
        $userBonusesHistoryGrid->hideFilters();
        $isActiveCol = $userBonusesHistoryGrid->getColumn('isActive');
        $isActiveCol->setTitle('Active');
        $isActiveCol->manipulateRenderCell(
            function($value, $row) {
                return ($value == 'true')? 'yes': 'no';
            }
        );
        $userBonusesHistoryGrid->getColumn('isCashedOut')->manipulateRenderCell(
            function($value, $row) {
                return ($value == 'true')? 'yes': 'no';
            }
        );
        $userBonusesHistoryGrid->getColumn('isCashedOut')->setTitle('Cashed out');
        $expireCell = $userBonusesHistoryGrid->getColumn('expireReason');
        $expireCell->setTitle('Expired by');
        $expireCell ->manipulateRenderCell(
            function($value, $row) {
                $res = '';
                switch ($value){
                    case TreasuryDepositBonuses::BY_USER:
                        $res = 'user';
                        break;
                    case TreasuryDepositBonuses::BY_TIME:
                        $res = 'time';
                        break;
                    case TreasuryDepositBonuses::BY_ADMIN:
                        $res = 'system';
                        break;
                    case TreasuryDepositBonuses::BY_WAGER:
                        $res = 'wager completed';
                        break;
                }
                return $res;
            }
        );
        $userBonusesHistoryGrid->setId('id');
        //$userBonusesHistoryGrid->isPagerSectionVisible();


        $userBonusesHistoryGrid->hideColumns(
            [
                'id','wage','totalWage','balanceBefore', 'changedAt', 'createdAt'
            ]
        );


        if ($userBonusesHistoryGrid->isReadyForRedirect()) {
            return $userBonusesHistoryGrid->getGridResponse();
        }

        return $this->render(
            'CasinoBonusBundle:BonusUser:pick-bonus.html.twig',
            ['currentBonus' => $user->getCurrentBonus(),
                'bonusForm' => $bonusForm->createView(),'promoForm'=>$promoForm->createView(),
                'balance' => $user->getBalance(),
                'grid' => $userBonusesHistoryGrid]
        );
    }

    private function handlePromoForm(Form $promoForm, User $user){
        if ($promoForm->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $promocode = $em->getRepository('CasinoBonusBundle:Promocode')->findOneBy(
                ['code'=>$promoForm->get('code')->getData()]);

            //check existence of code
            if (!is_object($promocode)){
                $promoForm->get('code')->addError(new FormError('Wrong code!'));
            } else {

                //check constraints by users, groups, time
                $constErrors = $this->promocodeConstraintsErrors($promocode, $user);
                if (count($constErrors) > 0 ){
                    foreach ($constErrors as $error){
                        $promoForm->get('code')->addError($error);
                    }
                } else {

                    //check is code attached to bonus
                    $bonusByPromocode = $em->getRepository('CasinoBonusBundle:Bonuses')->findOneBy( ['promocode'=>$promocode]);
                    if (is_object($bonusByPromocode)){

                        //check if user used this code before
                        $userPromocodes = $em->getRepository('CasinoTreasuryBundle:TreasuryDepositBonuses')->findOneBy( ['promocode'=>$promocode, 'user' => $user]);
                        if (is_object($userPromocodes)){
                            $promoForm->get('code')->addError(new FormError('Already used!'));
                        } else {

                            //check if user already has access to this bonus
                            $userBonusByCodeId = $bonusByPromocode->getId();
                            $closure = function($key, $element) use ($userBonusByCodeId){
                                return $element->getId() == $userBonusByCodeId;
                            };
                            if ($user->getAvBonuses()->exists($closure)){
                                $promoForm->get('code')->addError(new FormError('You already have this bonus!'));
                            } else {
                                $user->addAvBonus($bonusByPromocode);
                                $em->persist($user);
                                $em->flush();
                            }
                        }
                    } else {
                        $promoForm->get('code')->addError(new FormError('Code without bonus!'));
                    }
                }
            }
        }
    }

    public function bonusListAction(){
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if (!is_object($user)) {
            return $this->redirect(
                $this->generateUrl(
                    'fos_user_security_login'
                )
            );
        }

        $em = $this->getDoctrine()->getManager();
        $userBonuses = $user->getAvBonuses()->toArray();
        $publicBonuses = $em->getRepository('CasinoBonusBundle:Bonuses')->findBy( array('isPublic' => true ) );
        //merge arrays. contain duplicates
        $avialableBonuses = array_merge($publicBonuses,$userBonuses);
        //remove duplicates
        $avialableBonuses = array_map("unserialize", array_unique(array_map("serialize", $avialableBonuses)));
        sort( $avialableBonuses );

        $source = new Entity('CasinoBonusBundle:Bonuses');
        $source->setData($avialableBonuses);

        $source->manipulateRow(
            function (Row $row)
            {
                $type = $row->getField('type');
                if ( $type == 'fixed_val'){
                    $row->setField('type', 'fixed');
                } elseif(($type == 'float_val')) {
                    $row->setField('type', 'percentage');
                    $row->setField('price', 'percentage');
                }
                return $row;
            }
        );



        $grid = $this->get('grid');
        $grid->setSource($source);
        $grid->setId('id');
        $grid->setLimits([5, 10, 15]);

        $grid->getColumn('amount')->manipulateRenderCell(
            function($value) {
                return $value . '$';
            }
        );
        $grid->getColumn('price')->manipulateRenderCell(
            function($value, $row) {
                return ($value == 'percentage')? 'percentage' : $value . '$';
            }
        );
        $column = $grid->getColumn('isPublic');
        $column->setTitle('Private bonus?');
        $column->manipulateRenderCell(
            function($value, $row) {
                return ($value == 'true')? 'public' : 'personal';
            }
        );
        $grid->getColumn('wageRate')->manipulateRenderCell(
            function($value, $row) {
                return $value . '%';
            }
        );

        $grid->hideColumns(
            [
                'id', 'reasonMsg','provider'
            ]
        );

        if ($grid->isReadyForRedirect()) {
            return $grid->getGridResponse();
        }
        return $this->render(
            'CasinoBonusBundle:BonusUser:index.html.twig',
            ['grid' => $grid]
        );
    }

    private function promocodeConstraintsErrors(\Casino\BonusBundle\Entity\Promocode $promocode, User $user){
        $formErrors = [];

        //temporal-related constraints
        if ($promocode->getIsTemporary()){
            if ($promocode->getCondStartDate() > new \DateTime('now')){
                $formErrors[] = new FormError('Promocode is not active yet!');
            }

            if ($promocode->getCondEndDate() < new \DateTime('now')){
                $formErrors[] = new FormError('Promocode is no longer active!');
            }
        }

        //not allowed users
        $notUsers = $promocode->getCondNotUsers();
        if (!empty($notUsers)){
            foreach($notUsers as $notUser){
                if ($notUser->getId() == $user->getId()){
                    $formErrors[] = new FormError('You are not allowed to use this code.');
                }
            }
        }

        //only allowed users
        $allowedUsers = $promocode->getCondUsers();
        if (!empty($allowedUsers)){
            $allowedIds = [];
            foreach($allowedUsers as $allowedUser){
                $allowedIds[] = $allowedUser->getId();
            }
            if ( !in_array($user->getId(), $allowedIds)){
                $formErrors[] = new FormError('You are not allowed to use this code.');
            }
        }

        //get array of user groups
        $ugroups = $user->getUgroups();
        $ugroupIds = [];
        foreach ($ugroups as $group){
            $ugroupIds[] = $group->getId();
        }

        //only allowed groups
        $allowedGroups = $promocode->getCondGroups();
        if (!empty($allowedGroups)){
            $allowedIds = [];
            foreach($allowedGroups as $allowedGroup){
                $allowedIds[] = $allowedGroup->getId();
            }

            if (!empty($allowedIds) && count( array_intersect( $ugroupIds, $allowedIds)) == 0){
                $formErrors[] = new FormError('You are not allowed to use this code.');
            }
        }

        //check if user in restricted user group
        $notAllowedGroups = $promocode->getCondGroups();
        if (!empty($notAllowedGroups)){
            $allowedIds = [];
            foreach($notAllowedGroups as $notAllowedGroup){
                $allowedIds[] = $notAllowedGroup->getId();
            }
            if ( count( array_intersect( $ugroupIds, $allowedIds)) > 0){
                $formErrors[] = new FormError('You are not allowed to use this code.');
            }
        }

        return $formErrors;
    }

}
