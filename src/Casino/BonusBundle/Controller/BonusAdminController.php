<?php

namespace Casino\BonusBundle\Controller;

use Casino\BonusBundle\Form\BonusType;
use APY\DataGridBundle\Grid\Source\Entity;
use APY\DataGridBundle\Grid\Action\RowAction;
use Casino\BonusBundle\Entity\Bonuses;
use Casino\DefaultBundle\Controller\BaseAdminController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;

/**
 * Bonus controller.
 *
 */
class BonusAdminController extends BaseAdminController
{

    /**
     * Lists all Bonus entities.
     *
     */
    public function indexAction()
    {
        $source = new Entity('CasinoBonusBundle:Bonuses');

        $grid = $this->get('grid');
        $grid->setSource($source);
        $grid->setId('id');
        $grid->setLimits([5, 10, 15]);

        $grid->hideColumns(
            [
                'reasonMsg', 'provider'
            ]
        );
        $grid->setColumnsOrder(['id', ]);

        $rowAction = new RowAction('Show', 'admin_bonus_bonus_show');
        $rowAction->setRouteParameters(['id']);
        $grid->addRowAction($rowAction);

        $rowAction = new RowAction('Edit', 'admin_bonus_bonus_edit');
        $rowAction->setRouteParameters(['id']);
        $grid->addRowAction($rowAction);

        if ($grid->isReadyForRedirect()) {
            return $grid->getGridResponse();
        }

        return $this->render(
            'CasinoBonusBundle:BonusAdmin:index.html.twig',
            ['grid' => $grid]
        );
    }

    /**
     * Finds and displays a Bonus entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CasinoBonusBundle:Bonuses')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Bonus entity.');
        }

        return $this->render(
            'CasinoBonusBundle:BonusAdmin:show.html.twig',
            [
                'entity' => $entity,
            ]
        );
    }

    /**
     * Finds and displays a Bonus entity.
     *
     */
    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CasinoBonusBundle:Bonuses')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Bonus entity.');
        }
//        $form = $this->createFormBuilder($entity)
//            ->getForm();

        $form = $this->createForm(new BonusType(), $entity, array(
            //  'action' => $this->generateUrl('promocode-admin_update', array('id' => $entity->getId())),
            //'method' => 'POST',
        ))->add('Update','submit');

        if ('POST' == $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $em->flush();

                $request->getSession()->getFlashBag()->add(
                    'notice',
                    'Your changes were saved!'
                );
            } else {
                $request->getSession()->getFlashBag()->add(
                    'warning',
                    'Wrong input data!'
                );

                return $this->redirect(
                    $this->generateUrl(
                        'admin_bonus_bonus_edit',
                        ['id' => $id]
                    )
                );
            }
        }

        return $this->render(
            'CasinoBonusBundle:BonusAdmin:edit.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }


    /**
     * Creates a form to create a Bonus entity.
     *
     * @param Bonuses $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Bonuses $entity)
    {

        $form = $this->createForm(new BonusType(), $entity, array(
          //  'action' => $this->generateUrl('promocode-admin_update', array('id' => $entity->getId())),
            'method' => 'POST',
        ))
            ->add('Create','submit');;
        return $form;
    }

    /**
     * Displays a form to create a new Promocode entity.
     *
     */
    public function newAction(Request $request)
    {
        $entity = new Bonuses();
        $form   = $this->createCreateForm($entity);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_bonus_bonus_show', array('id' => $entity->getId())));
        }

        return $this->render('CasinoBonusBundle:BonusAdmin:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }



    public function giveBonusAction( Request $request, $email = null){

        $em = $this->getDoctrine()->getManager();
        $formData = array();
        $user = null;
        $form = $this->createFormBuilder($formData)
            ->setMethod('post')
            ->add('username', 'text', array('label' => 'Username or email','required' => false, 'data'=>$email))
            ->add('bonus',
                'entity', array(
                    'label' => 'Select bonus',
                    'required' => true,
                    'class' => 'CasinoBonusBundle:Bonuses',
                    'property' => 'inputChoise',
                    'expanded' => false,
                   /* //show only private bonuses
                    'query_builder' => function(EntityRepository $repository) {
                        $qb = $repository->createQueryBuilder('u');
                        return $qb->where($qb->expr()->neq('u.isPublic', '1'));
                    }*/
                ))
            ->add('groups',
                'entity', array(
                    'label' => 'And/or give to group',
                    'empty_value' => '-- None --',
                    'required' => false,
                    'class' => 'CasinoUserBundle:Group',
                    'property' => 'name',
                    'multiple' => true,
                    'expanded' => true
                ))
            ->add('submit', 'submit', array('label' => 'Give bonus'))
            ->getForm();

        if ('POST' == $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $bonus = $form->get('bonus')->getData();
                $username = $form->get('username')->getData();

                if (!is_null($username)){

                    /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
                    $userManager = $this->get('fos_user.user_manager');
                    $user = $userManager->findUserByUsernameOrEmail($username);

                    if (!is_null($user)) {
                        $avBonuses = $user->getAvBonuses();
                        if ($avBonuses->contains($bonus)){
                            $form->get('username')->addError( new FormError('User already has this bonus'));
                        } else {
                            $user->addAvBonus($bonus);
                            $em->persist($user);
                        }
                    } else {
                        $form->get('username')->addError(new FormError('User not found'));
                    }
                }

                $groups = $form->get('groups')->getData();
                if (is_array($groups)){
                    foreach ($groups as $group){
                        foreach ($group as $groupUser ){
                            $avBonuses = $groupUser->getAvBonuses();
                            if ($avBonuses->contains($bonus)){
                                $form->get('groups')->addError( new FormError('User ' . $groupUser->getUsername() . ' already has this bonus'));
                            } else {
                                $groupUser->addAvBonus($bonus);
                                $em->persist($groupUser);
                            }

                        }
                    }
                }
                $em->flush();

                $request->getSession()->getFlashBag()->add(
                    'notice',
                    'Your changes were saved!'
                );
            } else {
                $request->getSession()->getFlashBag()->add(
                    'warning',
                    'Wrong input data!'
                );
            }
        }
        return $this->render(
            'CasinoBonusBundle:BonusAdmin:give.html.twig',
            [
                'give_form' => $form->createView(),
                'user'=>$user
            ]
        );
    }
}
