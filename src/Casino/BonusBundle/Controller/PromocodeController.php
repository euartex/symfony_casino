<?php

namespace Casino\BonusBundle\Controller;

use APY\DataGridBundle\Grid\Action\RowAction;
use APY\DataGridBundle\Grid\Source\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Casino\BonusBundle\Entity\Promocode;
use Casino\BonusBundle\Form\PromocodeType;
use Symfony\Component\PropertyAccess\StringUtil;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Promocode controller.
 *
 */
class PromocodeController extends Controller
{

    /**
     * Lists all Promocode entities.
     *
     */
    public function indexAction()
    {
        $source = new Entity('CasinoBonusBundle:Promocode');

        $grid = $this->get('grid');
        $grid->setSource($source);
        $grid->setId('id');
        $grid->setLimits([5, 10, 15]);

//        $grid->hideColumns(
//            [
//                'reasonMsg', 'provider'
//            ]
//        );
        $grid->setColumnsOrder(['id', ]);

        $rowAction = new RowAction('Show', 'promocode-admin_show');
        $rowAction->setRouteParameters(['id']);
        $grid->addRowAction($rowAction);

        $rowAction = new RowAction('Edit', 'promocode-admin_edit');
        $rowAction->setRouteParameters(['id']);
        $grid->addRowAction($rowAction);

        if ($grid->isReadyForRedirect()) {
            return $grid->getGridResponse();
        }

        return $this->render(
            'CasinoBonusBundle:Promocode:index.html.twig',
            ['grid' => $grid]
        );

    }
    /**
     * Creates a new Promocode entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Promocode();

        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('promocode-admin_show', array('id' => $entity->getId())));
        }

        return $this->render('CasinoBonusBundle:Promocode:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Promocode entity.
     *
     * @param Promocode $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Promocode $entity)
    {
        $form = $this->createForm(new PromocodeType(), $entity, array(
            'action' => $this->generateUrl('promocode-admin_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Promocode entity.
     *
     */
    public function newAction()
    {
        $entity = new Promocode();
        $form   = $this->createCreateForm($entity);

        return $this->render('CasinoBonusBundle:Promocode:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Promocode entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CasinoBonusBundle:Promocode')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Promocode entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CasinoBonusBundle:Promocode:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Promocode entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CasinoBonusBundle:Promocode')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Promocode entity.');
        }

        $editForm = $this->createEditForm($entity);
////        $deleteForm = $this->createDeleteForm($id);
//
        return $this->render('CasinoBonusBundle:Promocode:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
////            'delete_form' => $deleteForm->createView(),
        ));
//
//        return new Response();
    }

    /**
    * Creates a form to edit a Promocode entity.
    *
    * @param Promocode $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Promocode $entity)
    {
        $form = $this->createForm(new PromocodeType(), $entity, array(
            'action' => $this->generateUrl('promocode-admin_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Promocode entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CasinoBonusBundle:Promocode')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Promocode entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('promocode-admin_edit', array('id' => $id)));
        }

        return $this->render('CasinoBonusBundle:Promocode:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Promocode entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CasinoBonusBundle:Promocode')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Promocode entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('promocode-admin'));
    }

    /**
     * Creates a form to delete a Promocode entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('promocode-admin_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    /**
     * Use an promoceode
     *
     */
    public function useAction(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if (!is_object($user)) {
            return $this->redirect(
                $this->generateUrl(
                    'fos_user_security_login'
                )
            );
        }

        $form = $this->createFormBuilder()
            //->setAction($this->generateUrl('promocode-admin_delete', array('id' => $id)))
            ->setMethod('POST')
            ->add('code','text', ['required'=>true, 'label' => 'Enter the code', 'attr' => ['maxlength' => 8]])
            ->add('submit', 'submit', array('label' => 'Get Bonus'))
            ->getForm()
        ;

        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $promocode = $em->getRepository('CasinoBonusBundle:Promocode')->findOneBy(
                ['code'=>$form->get('code')->getData()]);

            //check existence of code
            if (!is_object($promocode)){
                $form->get('code')->addError(new FormError('Wrong code!'));
            } else {

                //check constraints by users, groups, time
                $constErrors = $this->promocodeConstraintsErrors($promocode, $user);
                if (count($constErrors) > 0 ){
                    foreach ($constErrors as $error){
                        $form->get('code')->addError($error);
                    }
                } else {

                    //check is code attached to bonus
                    $bonusByPromocode = $em->getRepository('CasinoBonusBundle:Bonuses')->findOneBy( ['promocode'=>$promocode]);
                    if (is_object($bonusByPromocode)){

                        //check if user used this code before
                        $userPromocodes = $em->getRepository('CasinoTreasuryBundle:TreasuryDepositBonuses')->findOneBy( ['promocode'=>$promocode, 'user' => $user]);
                        if (is_object($userPromocodes)){
                            $form->get('code')->addError(new FormError('Already used!'));
                        } else {

                            //check if user already has access to this bonus
                            $userBonusByCodeId = $bonusByPromocode->getId();
                            $closure = function($key, $element) use ($userBonusByCodeId){
                                return $element->getId() == $userBonusByCodeId;
                            };
                            if ($user->getAvBonuses()->exists($closure)){
                                $form->get('code')->addError(new FormError('You already have this bonus!'));
                            } else {
                                $promocode->setUseTimes( ($promocode->getUseTimes() + 1) );
                                $em->persist($promocode);
                                $user->addAvBonus($bonusByPromocode);
                                $em->persist($user);
                                $em->flush();
                            }
                        }
                    } else {
                        $form->get('code')->addError(new FormError('Code without bonus!'));
                    }
                }
            }
        }

        return $this->render('CasinoBonusBundle:Promocode:use.html.twig', array(
            'form'   => $form->createView(),
        ));


    }

    private function promocodeConstraintsErrors(\Casino\BonusBundle\Entity\Promocode $promocode, $user){
        $formErrors = [];

        //temporal-related constraints
        if ($promocode->getIsTemporary()){
            if ($promocode->getCondStartDate() > new \DateTime('now')){
                $formErrors[] = new FormError('Promocode is not active yet!');
            }

            if ($promocode->getCondEndDate() < new \DateTime('now')){
                $formErrors[] = new FormError('Promocode is no longer active!');
            }
        }

        //not allowed users
        $notUsers = $promocode->getCondNotUsers();
        if (!empty($notUsers)){
            foreach($notUsers as $notUser){
                if ($notUser->getId() == $user->getId()){
                    $formErrors[] = new FormError('You are not allowed to use this code.');
                }
            }
        }

        //only allowed users
        $allowedUsers = $promocode->getCondUsers();
        if (!empty($allowedUsers)){
            $allowedIds = [];
            foreach($allowedUsers as $allowedUser){
                $allowedIds[] = $allowedUser->getId();
            }
            if ( !in_array($user->getId(), $allowedIds)){
                $formErrors[] = new FormError('You are not allowed to use this code.');
            }
        }

        //get array of user groups
        $ugroups = $user->getUgroups();
        $ugroupIds = [];
        foreach ($ugroups as $group){
            $ugroupIds[] = $group->getId();
        }

        //only allowed groups
        $allowedGroups = $promocode->getCondGroups();
        if (!empty($allowedGroups)){
            $allowedIds = [];
            foreach($allowedGroups as $allowedGroup){
                $allowedIds[] = $allowedGroup->getId();
            }

            if (!empty($allowedIds) && count( array_intersect( $ugroupIds, $allowedIds)) == 0){
                $formErrors[] = new FormError('You are not allowed to use this code.');
            }
        }

        //check if user in restricted user group
        $notAllowedGroups = $promocode->getCondGroups();
        if (!empty($notAllowedGroups)){
            $allowedIds = [];
            foreach($notAllowedGroups as $notAllowedGroup){
                $allowedIds[] = $notAllowedGroup->getId();
            }
            if ( count( array_intersect( $ugroupIds, $allowedIds)) > 0){
                $formErrors[] = new FormError('You are not allowed to use this code.');
            }
        }

        //use-time constraints
        if ($promocode->getUseTimes()){
            if ($promocode->getCondStartDate() > new \DateTime('now')){
                $formErrors[] = new FormError('Promocode is not active yet!');
            }

            if ($promocode->getCondEndDate() < new \DateTime('now')){
                $formErrors[] = new FormError('Promocode is no longer active!');
            }
        }

        return $formErrors;
    }

}
