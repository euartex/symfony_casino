<?php

namespace Casino\BonusBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bonuses
 *
 * @ORM\Table(name="bonuses")
 * @ORM\Entity
 */
class Bonuses
{

    const BONUS_TYPE_FIXED_AMOUNT = 'fixed_val';
    const BONUS_TYPE_FLOAT_AMOUNT = 'float_val';

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=false)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="reason_msg", type="text", length=65535, nullable=true)
     */
    private $reasonMsg;

    /**
     * @var string
     *
     * @ORM\Column(name="provider", type="string", length=255, nullable=true)
     */
    private $provider;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * Whether accessible for everyone by default
     *
     * @var boolean
     *
     * @ORM\Column(name="is_public", type="boolean", nullable=false)
     */
    private $isPublic = 0;

    /**
     * @var integer determines bonus total wage
     *
     * @ORM\Column(name="wage_rate", type="integer", nullable=true)
     */
    private $wageRate = 200;//%

    /**
     * @var integer bonus amount for float bonus price constant. determines how big bonus amount will be
     *
     * @ORM\Column(name="buy_rate", type="integer", nullable=true)
     */
    private $buyRate = 100;//%

    /**
     * @var integer game constant. determines how wager will be added upon spin
     *
     * @ORM\Column(name="game_rate", type="integer")
     */
    private $gameRate = 100;//%


    /**
     * @var string
     *
     * @ORM\Column(name="name", type="text", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $amount;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $price = '0.00';

    /**
     * @ORM\OneToOne(targetEntity="Casino\BonusBundle\Entity\Promocode", mappedBy="bonus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="code_id", referencedColumnName="id")
     * })
     */
    protected $promocode;

    /**
     * @var integer makes bonus available after n-th deposit.
     *
     * @ORM\Column(name="nth_deposit", type="integer")
     */
    private $rewardForNthDep = 0;

    /**
     * Set amount
     *
     * @param string $amount
     *
     * @return Bonuses
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string
     */
    public function getAmount()
    {
        return  $this->amount;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Bonuses
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set reasonMsg
     *
     * @param string $reasonMsg
     *
     * @return Bonuses
     */
    public function setReasonMsg($reasonMsg)
    {
        $this->reasonMsg = $reasonMsg;

        return $this;
    }

    /**
     * Get reasonMsg
     *
     * @return string
     */
    public function getReasonMsg()
    {
        return $this->reasonMsg;
    }

    /**
     * Set provider
     *
     * @param string $provider
     *
     * @return Bonuses
     */
    public function setProvider($provider)
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * Get provider
     *
     * @return string
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get wage rate
     *
     * @return integer
     */
    public function getWageRate()
    {
        return $this->wageRate;
    }

    /**
     * Set wage rate
     *
     * @return integer
     */
    public function setWageRate($wageRate)
    {
        $this->wageRate = $wageRate;

        return $this;
    }

    /**
     * Get amount rate
     *
     * @return integer
     */
    public function getBuyRate()
    {
        return $this->buyRate;
    }

    /**
     * Set buy rate
     *
     * @return integer
     */
    public function setBuyRate($buyRate)
    {
        $this->buyRate = $buyRate;
        return $this;
    }

    /**
     * Get game rate
     *
     * @return integer
     */
    public function getGameRate()
    {
        return $this->gameRate;
    }

    /**
     * Set game rate
     *
     * @return integer
     */
    public function setGameRate($gameRate)
    {
        $this->gameRate = $gameRate;
        return $this;
    }

    /**
     * Set bonus name
     *
     * @param string $name
     *
     * @return Bonuses
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }



    /**
     * @param boolean $isPublic
     * @return Bonuses
     */
    public function setIsPublic($isPublic)
    {
        $this->isPublic = $isPublic;

        return $this;
    }

    /**
     * Is bonus obtainable for everyone
     *
     * @return boolean
     */
    public function getIsPublic()
    {
        return ($this->isPublic)? true : false;
    }

    public function getInputChoise(){
        return '"' . $this->getName() . '" (' . $this->getGameRate()  . '% wage), price:' . $this->getPrice() . '$';
    }


    /**
     * Set price
     *
     * @param string $price
     *
     * @return Bonuses
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }


    /**
     * Set promocode
     *
     * @param \Casino\BonusBundle\Entity\Promocode $promocode
     *
     * @return Bonuses
     */
    public function setPromocode(\Casino\BonusBundle\Entity\Promocode $promocode = null)
    {
        $this->promocode = $promocode;

        return $this;
    }

    /**
     * Get promocode
     *
     * @return \Casino\BonusBundle\Entity\Promocode
     */
    public function getPromocode()
    {
        return $this->promocode;
    }

    /**
     * @param int $rewardForNthDep
     * @return Bonuses
     */
    public function setRewardForNthDep($rewardForNthDep)
    {
        $this->rewardForNthDep = $rewardForNthDep;
        return $this;
    }

    /**
     * @return int
     */
    public function getRewardForNthDep()
    {
        return $this->rewardForNthDep;
    }
}
