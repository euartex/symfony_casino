<?php

namespace Casino\BonusBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Promocodes
 *
 * @ORM\Table(name="promocode")
 * @ORM\Entity
 */
class Promocode
{

    //protected $conditions; // = ['starts'=>null, 'ends'=>null, 'uid'=>null, 'group'=>null, 'not_uid'=>null, 'not_group'=>null];

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="bonuses", inversedBy="promocode")
     **/
    private $bonus;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255, nullable=false)
     */
    private $code;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_times", type="integer", nullable=false)
     */
    private $useTimes = 0;

    /**
     * @ORM\ManyToMany(targetEntity="Casino\UserBundle\Entity\User")
     * @ORM\JoinTable(name="promocode_conds_users",
     *      joinColumns={@ORM\JoinColumn(name="id1", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="id2", referencedColumnName="id", unique=true)}
     *    )
     **/
    public $condUsers;

    /**
     * @ORM\ManyToMany(targetEntity="Casino\UserBundle\Entity\User")
     * @ORM\JoinTable(name="promocode_conds_not_users",
     *      joinColumns={@ORM\JoinColumn(name="id1", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="id2", referencedColumnName="id", unique=true)}
     *    )
     **/
    public $condNotUsers;

    /**
     * @ORM\ManyToMany(targetEntity="Casino\UserBundle\Entity\Group")
     * @ORM\JoinTable(name="promocode_conds_groups",
     *      joinColumns={@ORM\JoinColumn(name="id1", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="id2", referencedColumnName="id", unique=true)}
     *    )
     **/
    public $condGroups;

    /**
     * @ORM\ManyToMany(targetEntity="Casino\UserBundle\Entity\Group")
     * @ORM\JoinTable(name="promocode_conds_not_groups",
     *      joinColumns={@ORM\JoinColumn(name="id1", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="id2", referencedColumnName="id", unique=true)}
     *    )
     **/
    public $condNotGroups;

    /** @ORM\Column(name="cond_start_date",type="datetime") */
    private $condStartDate;

    /** @ORM\Column(name="cond_end_date",type="datetime") */
    private $condEndDate;

    /** @ORM\Column(name="is_temporary",type="boolean") */
    private $isTemporary;

//    /** @ORM\Column(name="use_time_consts",type="boolean") */
//    private $useTimeConstraint;

    public function __construct(){
        $this->condUsers =  new \Doctrine\Common\Collections\ArrayCollection();//  setUseCondition(json_decode($this->getConditions()));
        $this->condNotUsers =  new \Doctrine\Common\Collections\ArrayCollection();//  setUseCondition(json_decode($this->getConditions()));
        $this->condGroups =  new \Doctrine\Common\Collections\ArrayCollection();//  setUseCondition(json_decode($this->getConditions()));
        $this->condNotGroups =  new \Doctrine\Common\Collections\ArrayCollection();//  setUseCondition(json_decode($this->getConditions()));
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Promocode
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set useTimes
     *
     * @param integer $useTimes
     *
     * @return Promocode
     */
    public function setUseTimes($useTimes)
    {
        $this->useTimes = $useTimes;

        return $this;
    }

    /**
     * Get useTimes
     *
     * @return integer
     */
    public function getUseTimes()
    {
        return $this->useTimes;
    }

    /**
     * Set condStartDate
     *
     * @param \DateTime $condStartDate
     *
     * @return Promocode
     */
    public function setCondStartDate($condStartDate)
    {
        $this->condStartDate = $condStartDate;

        return $this;
    }

    /**
     * Get condStartDate
     *
     * @return \DateTime
     */
    public function getCondStartDate()
    {
        return $this->condStartDate;
    }

    /**
     * Set condEndDate
     *
     * @param \DateTime $condEndDate
     *
     * @return Promocode
     */
    public function setCondEndDate($condEndDate)
    {
        $this->condEndDate = $condEndDate;

        return $this;
    }

    /**
     * Get condEndDate
     *
     * @return \DateTime
     */
    public function getCondEndDate()
    {
        return $this->condEndDate;
    }

    /**
     * Set bonus
     *
     * @param \Casino\BonusBundle\Entity\bonuses $bonus
     *
     * @return Promocode
     */
    public function setBonus(\Casino\BonusBundle\Entity\bonuses $bonus = null)
    {
        $this->bonus = $bonus;

        return $this;
    }

    /**
     * Get bonus
     *
     * @return \Casino\BonusBundle\Entity\bonuses
     */
    public function getBonus()
    {
        return $this->bonus;
    }

    /**
     * Add condUsers
     *
     * @param \Casino\UserBundle\Entity\User $condUser
     *
     * @return Promocode
     */
    public function addCondUser(\Casino\UserBundle\Entity\User $condUser)
    {
        $this->condUsers[] = $condUser;

        return $this;
    }

    /**
     * Remove condUser
     *
     * @param \Casino\UserBundle\Entity\User $condUser
     */
    public function removeCondUser(\Casino\UserBundle\Entity\User $condUser)
    {
        $this->condUsers->removeElement($condUser);
    }

    /**
     * Get condUsers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCondUsers()
    {
        return $this->condUsers;
    }

    public function setCondUsers($condUsers)
    {
        $this->condUsers = $condUsers;
    }

    public function setCondNotUser($condUsers)
    {
        $this->condNotUser = $condUsers;
    }

    /**
     * Add condNotUser
     *
     * @param \Casino\UserBundle\Entity\User $condNotUser
     *
     * @return Promocode
     */
    public function addCondNotUser(\Casino\UserBundle\Entity\User $condNotUser)
    {
        $this->condNotUsers[] = $condNotUser;

        return $this;
    }

    /**
     * Remove condNotUser
     *
     * @param \Casino\UserBundle\Entity\User $condNotUser
     */
    public function removeCondNotUser(\Casino\UserBundle\Entity\User $condNotUser)
    {
        $this->condNotUsers->removeElement($condNotUser);
    }

    /**
     * Get condNotUsers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCondNotUsers()
    {
        return $this->condNotUsers;
    }

    /**
     * Add condGroup
     *
     * @param \Casino\UserBundle\Entity\Group $condGroup
     *
     * @return Promocode
     */
    public function addCondGroup(\Casino\UserBundle\Entity\Group $condGroup)
    {
        $this->condGroups[] = $condGroup;

        return $this;
    }

    /**
     * Remove condGroup
     *
     * @param \Casino\UserBundle\Entity\Group $condGroup
     */
    public function removeCondGroup(\Casino\UserBundle\Entity\Group $condGroup)
    {
        $this->condGroups->removeElement($condGroup);
    }

    /**
     * Get condGroups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCondGroups()
    {
        return $this->condGroups;
    }

    public function setCondGroups($condGroups)
    {
        $this->condGroups = $condGroups;
    }

    public function setCondNotGroups($condNotGroups)
    {
        $this->condNotGroups = $condNotGroups;
    }


    /**
     * Add condNotGroup
     *
     * @param \Casino\UserBundle\Entity\Group $condNotGroup
     *
     * @return Promocode
     */
    public function addCondNotGroup(\Casino\UserBundle\Entity\Group $condNotGroup)
    {
        $this->condNotGroups[] = $condNotGroup;

        return $this;
    }

    /**
     * Remove condNotGroup
     *
     * @param \Casino\UserBundle\Entity\Group $condNotGroup
     */
    public function removeCondNotGroup(\Casino\UserBundle\Entity\Group $condNotGroup)
    {
        $this->condNotGroups->removeElement($condNotGroup);
    }

    /**
     * Get condNotGroups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCondNotGroups()
    {
        return $this->condNotGroups;
    }

    /**
     * Set isTemporary
     *
     * @param boolean $isTemporary
     *
     * @return Promocode
     */
    public function setIsTemporary($isTemporary)
    {
        $this->isTemporary = $isTemporary;

        return $this;
    }

    /**
     * Get isTemporary
     *
     * @return boolean
     */
    public function getIsTemporary()
    {
        return $this->isTemporary;
    }

    /**
     * Get promocode input name
     *
     * @return string
     */
    public function getInputChoise()
    {
        $date = ($this->isTemporary)? (' '. $this->condStartDate->format('Y-m-d H:i').' - '.$this->condEndDate->format('Y-m-d H:i').' ') : '';
        return '['.$this->code.'] '.$this->bonus->getName().$date;
    }

//    /**
//     * @param boolean $useTimeConstraint
//     * @return Promocode
//     */
//    public function setUseTimeConstraint($useTimeConstraint)
//    {
//        $this->useTimeConstraint = $useTimeConstraint;
//        return $this;
//    }
//
//    /**
//     * @return mixed
//     */
//    public function getUseTimeConstraint()
//    {
//        return $this->useTimeConstraint;
//    }

}
