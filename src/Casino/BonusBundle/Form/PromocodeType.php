<?php

namespace Casino\BonusBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PromocodeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code')
            ->add('useTimes')
            ->add('isTemporary','checkbox',['label'=>'Is promocode temporary?','required'=>false])
            ->add('condStartDate', 'date', array('widget' => 'single_text', 'html5'=>false, 'format'=> 'dd-MM-yyyy',
                'data' => new \DateTime()))
            ->add('condEndDate', 'date', array('widget' => 'single_text', 'html5'=>false, 'format'=> 'dd-MM-yyyy',
                'data' => new \DateTime()))
            ->add('bonus', 'entity', [
                'empty_value' => '- empty -',
                'class' => 'CasinoBonusBundle:Bonuses',
                'property' => 'getInputChoise',
                'required' => false,
            ])
            ->add('condUsers', 'entity', [
                'label' => 'To specific user',
                'empty_value' => '- empty -',
                'class' => 'CasinoUserBundle:User',
                'property' => 'username',
                'multiple' => true,
                'required' => false,
            ])
            ->add('condNotUsers', 'entity', [
                'label' => 'Except specific user',
                'empty_value' => '- empty -',
                'class' => 'CasinoUserBundle:User',
                'property' => 'username',
                'required' => false,
                'multiple' => true,
            ])
            ->add('condGroups', 'entity', [
                'label' => 'To specific User Group',
                'empty_value' => '- empty -',
                'class' => 'CasinoUserBundle:Group',
                'property' => 'name',
                'required' => false,
                'multiple' => true,
            ])
            ->add('condNotGroups', 'entity', [
                'label' => 'Except specific User Group',
                'empty_value' => '- empty -',
                'class' => 'CasinoUserBundle:Group',
                'property' => 'name',
                'required' => false,
                'multiple' => true,
            ])
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Casino\BonusBundle\Entity\Promocode'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'casino_bonusbundle_promocode';
    }
}
