<?php

namespace Casino\BonusBundle\Form;

use Casino\BonusBundle\Entity\Bonuses;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BonusType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('type', 'choice', array(
                'choices'  => array(Bonuses::BONUS_TYPE_FIXED_AMOUNT => 'fixed price', Bonuses::BONUS_TYPE_FLOAT_AMOUNT  => 'percentage of deposit'),
                'empty_data'  => Bonuses::BONUS_TYPE_FLOAT_AMOUNT
            ))
            ->add('price', 'money', array(
                'currency' => 'USD',
                'required' => true,
                'label' =>  'Price of bonus',
            ))
            ->add('amount', 'money', array(
                'currency' => 'USD',
                'required' => false,
                'label' => 'Amount of bonus money on bonus',
            ))
            ->add('buyRate')
            ->add('gameRate')
            ->add('wageRate')
            ->add('isPublic','checkbox', ['required' => false])
            ->add('rewardForNthDep', 'number', array(
                'required' => false,
                'label' =>  'Give bonus after #NUMBER# of deposits',
            ))
            ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Casino\BonusBundle\Entity\Bonuses'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'casino_bonusbundle_bonuses';
    }
}
