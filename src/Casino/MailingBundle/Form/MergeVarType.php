<?php

namespace Casino\MailingBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class MergeVarType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('var','text', ['required'=>true, 'label'=>'SendGrid template var', 'attr'=>['maxlength' => 16]])
            ->add('var_value','text', ['required'=>true, 'label'=>'Template var value', 'attr'=>['maxlength' => 255]])
            ->add('is_active', 'checkbox', ['required' => true])
        ;
    }

    public function getName()
    {
        return 'mergeVar';
    }
}
