<?php

namespace Casino\MailingBundle\Form;

use Casino\MailingBundle\Controller\TemplateVarsController;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TemplateType extends AbstractType
{
    private $templateVarsService;

    public function __construct( $templateVarsService){
        $this->templateVarsService = $templateVarsService;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('templateName','text')
            ->add('templateId','text')
            ->add('emailFrom','email')
            ->add('emailFromName')
            ->add('subject')
            ->add('vars',
                'collection',
                array(
                    'type'=>'choice',
                    'prototype'=>true,
                    'allow_add'=>true,
                    'allow_delete'=>true,
                    'options'=>array(
                        'choices' => $this->templateVarsService,
                        'required'=>true,
                        'label'=>'SendGrid template var',
                        'label_attr' => ['class'=>'mv-label'],
                        'attr'=> array('class' => 'mv-box')
                    ),
                    'label_attr' => ['id'=>'vars-label']
                )
            )
        ;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'casino_mailingbundle_template';
    }
}
