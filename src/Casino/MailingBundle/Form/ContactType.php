<?php

namespace Casino\MailingBundle\Form;

use EWZ\Bundle\RecaptchaBundle\Form\Type\EWZRecaptchaType;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints\IsTrue;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ContactType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
            $builder
            ->add('subject','text', ['required'=>true, 'label' => 'Subject of email', 'attr' => ['maxlength' => 255]])
            ->add('sender_name', 'text', ['required'=>true, 'label' => 'Your name', 'attr' => ['maxlength' => 255]])
            ->add('sender_email', 'email', ['required'=>true, 'label' => 'Your email', 'attr' => ['maxlength' => 255]])
            ->add('content', 'textarea', ['required'=>true, 'label' => 'Please write some feedback', 'label_attr' =>
                array('class' => 'textarea-label')])
            ->add('recaptcha', EWZRecaptchaType::class, array(
                'attr'        => array(
                    'options' => array(
                        'theme' => 'dark',
                        'type'  => 'image',
                        'size'  => 'normal'
                    ),
                    'class'    => 'recaptcha-test'
                ),
                'mapped'      => false,
                'constraints' => array(
                    new IsTrue()
                ),
                'label_attr' =>
                    array('class' => 'recaptcha-label')
            ))
            ->add('submit', 'submit', array('label' => 'Send email', 'attr' => ['class' => 'upload-withdraw_btn
            button_decoration contacts-btn']))
            ->setMethod('POST');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'casino_mailingbundle_mailing';
    }
}
