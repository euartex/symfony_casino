<?php

namespace Casino\MailingBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MailingType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title','text', array(
                'label'=>'Mailing name',
                'required'=>true))
            ->add('slug', 'text', [
                'label' => 'Slug for this mailing',
                'required' => true,
            ])
            ->add('template', 'entity', [
                'class' => 'CasinoMailingBundle:EmailTemplate',
                'property' => 'getTemplateName',
                'required' => true,
            ])
            ->add('groups','entity', [
                'class'    => 'CasinoUserBundle:Group',
                'property' => 'name',
                'multiple' => true,
                'required' => false
            ])
            ->add('isActive', 'checkbox', array(
                'label'    => 'Is this mailing active?',
                'required' => false,
            ));
    }

//    /**
//     * @param OptionsResolverInterface $resolver
//     */
//    public function setDefaultOptions(OptionsResolverInterface $resolver)
//    {
//        $resolver->setDefaults(
//            [
//                'data_class' => 'Casino\MailingBundle\Entity\Mailing'
//            ]
//        );
//    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'casino_mailingbundle_mailing';
    }
}
