<?php

namespace Casino\MailingBundle\Controller;

use APY\DataGridBundle\Grid\Action\RowAction;
use APY\DataGridBundle\Grid\Source\Entity;
use Casino\DefaultBundle\Controller\BaseAdminController;
use Casino\MailingBundle\Entity\Mailing;
use Casino\MailingBundle\Form\MailingType;
use Symfony\Component\HttpFoundation\Request;

class DefaultAdminController extends BaseAdminController
{
    public function indexAction()
    {
        $source = new Entity('CasinoMailingBundle:Mailing');
        $grid = $this->get('grid');
        $grid->setSource($source);
        $grid->setId('id');
        $grid->setLimits([5, 10, 15]);

        /*$grid->hideColumns(
            [ ]
        );*/

        $rowAction = new RowAction('Edit', 'casino_admin_mailing_edit');
        $rowAction->setRouteParameters(['id']);
        $grid->addRowAction($rowAction);

        $rowAction = new RowAction('Template', 'email_template_mailing_update');
        $rowAction->setRouteParameters(['id']);
        $grid->addRowAction($rowAction);


        if ($grid->isReadyForRedirect()) {
            return $grid->getGridResponse();
        }

        return $this->render(
            'CasinoMailingBundle:DefaultAdmin:index.html.twig',
            ['grid' => $grid]
        );

    }

    public function newAction(Request $request)
    {
        $entity = new Mailing();
        $form = $this->createForm(new MailingType(), $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            return $this->redirect($this->generateUrl('casino_admin_mailing'));
        }

        return $this->render(
            'CasinoMailingBundle:DefaultAdmin:new.html.twig',
            [
                'entity' => $entity,
                'form'   => $form->createView(),
            ]
        );
    }

    public function editAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CasinoMailingBundle:Mailing')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EmailTemplate entity.');
        }

        $editForm = $this->createForm(new MailingType(),$entity);
        $editForm->add('submit', 'submit', array('label' => 'Update'));


        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('casino_admin_mailing_edit', array('id' => $id)));
        }

        return $this->render('CasinoMailingBundle:DefaultAdmin:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ));

    }

}
