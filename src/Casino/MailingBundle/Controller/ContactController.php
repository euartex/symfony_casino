<?php

namespace Casino\MailingBundle\Controller;

use Casino\MailingBundle\Form\ContactType;
use Casino\UserBundle\Entity\User;
use Exception;
use Hip\MandrillBundle\Message;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;

class ContactController extends Controller
{
    public function contactAction(Request $request)
    {
        $formSended = false;
        $form = $this->createForm(new ContactType());
        $form->handleRequest($request);

        if ($form->isValid()) {


            $this->sendContactsMail($form);

            $formSended= true;
            unset($form);
            $form = $this->createForm(new ContactType());
        }

        return $this->render('CasinoMailingBundle:ContactUser:contact.html.twig', array(
            'form'   => $form->createView(), 'sended'=>$formSended
        ));
    }
/*
    private function sendContactsMail($form){
        $em = $this->getDoctrine()->getManager();
        $emailTemplate = $em->getRepository('CasinoMailingBundle:Mailing')->findOneBy(['slug' => 'contact-form'])->getTemplate();

        if (!$emailTemplate) {
            throw new Exception('Email template is absent. Please contact administration', 404);
        }

        $service = $this->get('casino.mailing.mandrill.mail.handler');
        $service->addToUser();

        $service->setContactContent($form->get('content')->getData());
        $service->setContactSubject($form->get('subject')->getData());
        $service->setContactSenderEmail($form->get('sender_email')->getData());
        $service->setContactSenderName($form->get('sender_name')->getData());
        $service->handleMail($emailTemplate);

    }*/

    private function sendContactsMail(Form $form){
        $em = $this->getDoctrine()->getManager();
        $emailTemplate = $em->getRepository('CasinoMailingBundle:Mailing')->findOneBy(['slug' => 'en-contacts'])->getTemplate();

        if (!$emailTemplate) {
            throw new Exception('Email template is absent. Please contact administration', 404);
        }

//        $admin = $this->get('fos_user.user_manager')->findUserByUsernameOrEmail('lotedo@outlook.com');
        $dev = $this->get('fos_user.user_manager')->findUserByUsernameOrEmail('lotedo@outlook.com');

        $service = $this->get('casino.mailing.mailer');
//        if (is_object($admin)) {
//            $service->addToUser($admin);
//        }
        if (is_object($dev)) {
            /** @var User $dev */
            $service->addToUser($dev);
        }

        $params = [
            '{{ContactSubject}}' => $form->get('subject')->getData(),
            '{{ContactSenderEmail}}' => $form->get('sender_email')->getData(),
            '{{ContactSenderName}}' => $form->get('sender_name')->getData(),
            '{{ContactContent}}' => $form->get('content')->getData(),
        ];
        $customParams = [
            'from' => $form->get('sender_email')->getData(),
            'fromName' => $form->get('sender_name')->getData(),
            'subject' => $form->get('subject')->getData(),
        ];
        $service->send($emailTemplate, $params, $customParams);
    }
}
