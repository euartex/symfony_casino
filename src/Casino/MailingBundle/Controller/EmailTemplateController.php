<?php

namespace Casino\MailingBundle\Controller;

use APY\DataGridBundle\Grid\Action\RowAction;
use APY\DataGridBundle\Grid\Source\Entity;
use Casino\MailingBundle\Entity\EmailTemplate;
use Casino\MailingBundle\Form\TemplateType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/**
 * EmailTemplate controller.
 *
 */
class EmailTemplateController extends Controller
{

    /**
     * Lists all EmailTemplate entities.
     *
     */
    public function indexAction()
    {
        $source = new Entity('CasinoMailingBundle:EmailTemplate');
        $grid = $this->get('grid');
        $grid->setSource($source);
        $grid->setId('id');
        $grid->setLimits([5, 10, 15]);

        $grid->hideColumns(
            ['vars' ]
        );
        $grid->setColumnsOrder(['title']);

        $rowAction = new RowAction('Edit', 'email-template_edit');
        $rowAction->setRouteParameters(['id']);
        $grid->addRowAction($rowAction);

        if ($grid->isReadyForRedirect()) {
            return $grid->getGridResponse();
        }

        return $this->render(
            'CasinoMailingBundle:EmailTemplate:index.html.twig',
            ['grid' => $grid]
        );
    }
    /**
     * Creates a new EmailTemplate entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new EmailTemplate();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setVars(array_values($entity->getVars()));
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('email-template'));
        }

        return $this->render('CasinoMailingBundle:EmailTemplate:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a EmailTemplate entity.
     *
     * @param EmailTemplate $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(EmailTemplate $entity)
    {
        $avialableMergeVars = $this->get('casino.mailing.templatevars.resolver')->fetchVarsList();
        $form = $this->createForm(new TemplateType($avialableMergeVars), $entity, array(
            'action' => $this->generateUrl('email-template_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new EmailTemplate entity.
     *
     */
    public function newAction()
    {
        $entity = new EmailTemplate();
        $form   = $this->createCreateForm($entity);

        return $this->render('CasinoMailingBundle:EmailTemplate:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a EmailTemplate entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CasinoMailingBundle:EmailTemplate')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EmailTemplate entity.');
        }

        $editForm = $this->createFormBuilder()
            ->setAction($this->generateUrl('email-template_edit', array('id' => $id)))
            ->add('Edit', 'submit', array('label' => 'Edit'))
            ->getForm();

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CasinoMailingBundle:EmailTemplate:show.html.twig', array(
            'entity'      => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing EmailTemplate entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CasinoMailingBundle:EmailTemplate')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EmailTemplate entity.');
        }

        $avialableMergeVars = $this->get('casino.mailing.templatevars.resolver')->fetchVarsList();
        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CasinoMailingBundle:EmailTemplate:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'actions_table' => $avialableMergeVars
        ));
    }

    /**
    * Creates a form to edit a EmailTemplate entity.
    *
    * @param EmailTemplate $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(EmailTemplate $entity)
    {
        $avialableMergeVars = $this->get('casino.mailing.templatevars.resolver')->fetchVarsList();
        $form = $this->createForm(new TemplateType($avialableMergeVars), $entity, array(
            'action' => $this->generateUrl('email-template_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing EmailTemplate entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CasinoMailingBundle:EmailTemplate')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EmailTemplate entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);
        $avialableMergeVars = $this->get('casino.mailing.templatevars.resolver')->fetchVarsList();

        if ($editForm->isValid()) {
            $entity->setVars(array_values($entity->getVars()));
            $em->persist($entity);
            $em->flush();

           // return $this->redirect($this->generateUrl('email-template_edit', array('id' => $id)));
        }

        return $this->render('CasinoMailingBundle:EmailTemplate:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'actions_table' => $avialableMergeVars
        ));
    }

    /**
     * Edits an existing EmailTemplate entity.
     *
     */
    public function updateMailingAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CasinoMailingBundle:Mailing')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EmailMailing entity.');
        }
        $entity = $entity->getTemplate();
        if (!$entity) {
            throw $this->createNotFoundException('This mailing does not have any associated templates.');
        }

        $deleteForm = $this->createDeleteForm($entity->getId());
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);
        $avialableMergeVars = $this->get('casino.mailing.templatevars.resolver')->fetchVarsList();

        if ($editForm->isValid()) {
            $entity->setVars(array_values($entity->getVars()));
            $em->persist($entity);
            $em->flush();

            // return $this->redirect($this->generateUrl('email-template_edit', array('id' => $id)));
        }

        return $this->render('CasinoMailingBundle:EmailTemplate:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'actions_table' => $avialableMergeVars
        ));
    }

    /**
     * Deletes a EmailTemplate entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CasinoMailingBundle:EmailTemplate')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find EmailTemplate entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('email-template'));
    }

    /**
     * Creates a form to delete a EmailTemplate entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('email-template_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
