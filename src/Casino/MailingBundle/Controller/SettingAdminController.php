<?php

namespace Casino\MailingBundle\Controller;

use Casino\DefaultBundle\Controller\BaseAdminController;
use Casino\MailingBundle\Form\TemplateType;
use Symfony\Component\HttpFoundation\Request;

class SettingAdminController extends BaseAdminController
{
    public function indexAction()
    {
        return $this->render('CasinoMailingBundle:SettingAdmin:index.html.twig',[
            'mailings' => ['registration']
        ]);
    }

    public function updateAction(Request $request, $name)
    {
        $avialableMergeVars = $this->get('casino.mailing.templatevars.resolver')->fetchVarsList();
        $form = $this->createForm(new TemplateType($avialableMergeVars));
        $form->setData(unserialize($this->get('craue_config')->get('mailing.' . $name)));
        $form->handleRequest($request);

        if ($form->isValid()) {

            $this->get('craue_config')->set(
                'mailing.' . $name,
                serialize($form->getData())
            );

            $request->getSession()->getFlashBag()->add(
                'notice',
                'Your changes were saved!'
            );
            return $this->redirect($this->generateUrl(
                    'casino_mailing_admin_setting_update',
                    ['name'=>$name]
                ));
        }

        return $this->render('CasinoMailingBundle:SettingAdmin:update.html.twig',[
            'form'=>$form->createView(),
            'name'=>$name,
        ]);
    }
}
