<?php

namespace Casino\MailingBundle\Services;

use Casino\MailingBundle\Entity\EmailTemplate;
use Casino\UserBundle\Entity as User;
use Hip\MandrillBundle\Dispatcher;
use Hip\MandrillBundle\Message;

/**
 * @package Casino\MailingBundle\Controller
 */
class MandrillVarsMailer {

    private $templateVarsService;
    private $mandrillDispatcher;

    /**
     * @var array of User`s to send them email
     */
    private $toUsers = [];

    /**
     * @var string override for email tempale`s default to simplify re
     */
    private $fromEmail = null;

    public function __construct(TemplateVars $templateVarsService, Dispatcher $mandrillDispatcher){
        $this->templateVarsService = $templateVarsService;
        $this->mandrillDispatcher = $mandrillDispatcher;
    }

    private function getTemplateVarsService(){
        return $this->templateVarsService;
    }

    public function setToUsers(User\User $user){
        $this->toUsers[] = $user;
    }

    public function getToUsers(){
        return $this->toUsers;
    }

    public function addToUser(User\User $user){
        $this->toUsers[] = $user;
    }

    /** dirty hacks
     * @todo: refactor it
     */
    public function setRestoreAccountLink($resettingUrl){
        $this->getTemplateVarsService()->setRestoreAccountLink($resettingUrl);
    }

    public function setRoute($route){
        $this->getTemplateVarsService()->setRoute($route);
    }

    public function setBonusAmount($bonusAmount){
        $this->getTemplateVarsService()->setBonusAmount($bonusAmount);
    }

    /**
     * @todo: refactor this setters
     */
    /* contacts setters */
    public function setContactSubject($contactSubject){
        $this->getTemplateVarsService()->setContactSubject($contactSubject);
    }
    public function setContactSenderName($senderName){
        $this->getTemplateVarsService()->setContactSenderName($senderName);
    }
    public function setContactSenderEmail($senderName){
        $this->getTemplateVarsService()->setContactSenderEmail($senderName);
    }
    public function setContactContent($contactContent){
        $this->getTemplateVarsService()->setContactContent($contactContent);
    }
    /* end of contacts setters */

    /**
     * @param $fromEmail string
     */
    public function setFromEmail($fromEmail){
        $this->fromEmail = $fromEmail;
    }

    function handleMail(EmailTemplate $emailTemplate){

        $emailFrom = (is_null($this->fromEmail))? $emailTemplate->getEmailFrom() : $this->fromEmail;
        $emailFromName = $emailTemplate->getEmailFromName();
        $subject = $emailTemplate->getSubject();
        $templateName = $emailTemplate->getTemplateName();

        /** array to store Mandrill mergevars in email=>[data] format*/
        $dynamicEmailVars = [];
        foreach ($this->getToUsers() as $user){
            $this->getTemplateVarsService()->setUser($user);
            $currEmail = $user->getEmail();

            //vars from template dynamic fields
            $mergeVars = [];
            $templateVars = $emailTemplate->getVars();
            foreach ($templateVars as $var){
                $resolvedVar = $this->templateVarsService->resolveVar($var);
                //check for null
                $mergeVars[$var] = isset($resolvedVar)? $resolvedVar : '';
            }
            $dynamicEmailVars[$currEmail] = $mergeVars;
        }

        $message = new Message();
        $message
            ->setFromEmail($emailFrom)
            ->setFromName($emailFromName)
            ->setSubject($subject)
            ->setSubaccount('lotedo')
            ->setMerge(true)
            ->setMergeLanguage('handlebars');

        foreach ($dynamicEmailVars as $email=>$emailData){
            $message->addTo($email);
            $message->addMergeVars($email, $emailData);
        }

        return $this->mandrillDispatcher->send($message, $templateName);
    }

}