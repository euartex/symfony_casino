<?php

namespace Casino\MailingBundle\Services;

use Casino\MailingBundle\Entity\EmailTemplate;
use Casino\UserBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MailerService
{
    /** @var ContainerInterface */
    private $container;

    /** @var \SendGrid */
    private $mailer;

    /**
     * @var User[]
     */
    private $toUsers = [];

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->mailer = new \SendGrid($this->container->getParameter('sendgrid.api_key'));
    }

    /**
     * @param User $user
     */
    public function addToUser($user)
    {
        $this->toUsers[] = $user;
    }

    /**
     * @param EmailTemplate $emailTemplate
     * @param array $params
     * @param array $customParams
     */
    public function send($emailTemplate, $params, $customParams = null)
    {
        $email = new \SendGrid\Email();
        $email
            ->setTemplateId($emailTemplate->getTemplateId())
            ->setText(' ')
            ->setHtml(' ')
            ->addFilter('templates', 'enabled', 1)
            ->addFilter('templates', 'template_id', $emailTemplate->getTemplateId());
        if ($customParams) {
            $email
                ->setFrom($customParams['from'])
                ->setFromName($customParams['fromName'])
                ->setSubject($customParams['subject']);
        } else {
            $email
                ->setFrom($emailTemplate->getEmailFrom())
                ->setFromName($emailTemplate->getEmailFromName())
                ->setSubject($emailTemplate->getSubject());
        }
        $templateVarsService = $this->container->get('casino.mailing.templatevars.resolver');
        $substitutions = [];
        foreach ($this->toUsers as $user) {
            $email->addSmtpapiTo($user->getEmail());
            $templateVarsService->setUser($user);

            //vars from template dynamic fields
            $mergeVars = [];
            $templateVars = $emailTemplate->getVars();
            foreach ($templateVars as $var) {
                $resolvedVar = $templateVarsService->resolveVar($var);
                $mergeVars[sprintf('{{%s}}', $var)] = empty($resolvedVar) ? '' : $resolvedVar;
            }
            $mergeVars = array_merge($mergeVars, $params);
            foreach ($mergeVars as $key => $value) {
                $substitutions[$key][] = $value;
            }
        }
        $email->setSubstitutions($substitutions);
        $this->mailer->send($email);
    }

    /**
     * @param array $emails
     * @param string $subject
     * @param string $text
     * @param EmailTemplate $emailTemplate
     * @throws \SendGrid\Exception
     */
    public function sendRaw(array $emails, $subject, $text, $emailTemplate = null)
    {
        if (null === $emailTemplate) {
            $emailTemplate = $this->container
                ->get('doctrine')
                ->getRepository('CasinoMailingBundle:EmailTemplate')
                ->findOneBy(['templateName' => 'system']);
        }
        $message = new \SendGrid\Email();
        $message
            ->setTos($emails)
            ->setText($text)
            ->setHtml(str_replace("\n", '<br>', $text))
            ->setFrom($emailTemplate->getEmailFrom())
            ->setFromName($emailTemplate->getEmailFromName())
            ->setSubject($subject ?: $emailTemplate->getSubject());
        $this->mailer->send($message);
    }
}
