<?php

namespace Casino\MailingBundle\Services;

use Casino\UserBundle\Entity as User;
use Doctrine\ORM\EntityManager;

/**
 * Class TemplateVarsController. Service to get user/mailing related data by calling resolved method name.
 * To add functionality just add @public method with name started with "get"
 *
 * @package Casino\MailingBundle\Services
 */
class TemplateVars {

    private $methodsList = [];
    private $user;
    private $restoreAccountLink;
    private $contactSubject;
    private $senderName;
    private $contactSenderEmail;
    private $contactContent;
    private $route;
    private $bonusAmount;


    public function __construct( EntityManager $entityManager){
        $this->em = $entityManager;
        $this->initMethodsList();
    }

    /**
     * @return array of accessible method names
     */
    protected function initMethodsList(){
        if (empty($this->methodsList)){
            $reflection = new \ReflectionClass(get_class($this));
            foreach ($reflection->getMethods( \ReflectionMethod::IS_PUBLIC) as $m) {
                if (substr($m->name, 0,  strlen('get')) === 'get'){
                    $this->methodsList[] = $m->name;
                }
            }
        }
        return $this->methodsList;
    }

    public function setUser(User\User $user){
        $this->user = $user;
    }

    /** get user. Notice 'get' @prefix AND @private modifier. So its out a list with acceptable Var names */
    public function fetchUser(){
        return $this->user;
    }

    /**
     * @param $paramName string method to be called
     * @return string resolved data
     */
    public function resolveVar($paramName)
    {
        //user not found
        if (!is_object($this->user)) {
            dump('user not found');
            return '';
        }

        $paramName = 'get'.$paramName;
        if (in_array($paramName, $this->methodsList)){
           return $this->$paramName();
        }
        return '';
    }

    public function fetchVarsList()
    {
        $varsList = $this->methodsList;

        $newList = [];
        foreach ($varsList as $val){
            $val = substr($val, 3);
            $newList[$val] = $val;
        }

        return $newList;
    }


    /** start of User entity methods */
//    public function getName(){
//        return $this->user->getName();
//    }

    public function getUserName(){
        return $this->user->getUserName();
    }

    public function getNickname(){
        return $this->user->getNickname();
    }

    public function getEmail(){
        return $this->user->getEmail();
    }

    public function getBalance(){
        return $this->user->getBalance();
    }

    public function getBonusBalance(){
        return $this->user->getBonusBalance();
    }

    public function getTel(){
        return $this->user->getTel();
    }

    public function getCreateDate(){
        return $this->user->getCreateDate();
    }

    public function getLastActivity(){
        return $this->user->getLastActivity();
    }

    /**
     * Array of user groups
     * @return array
     */
    public function getUgroups(){
        return $this->user->getUgroups();
    }

    public function getFirstName(){
        return $this->user->getFirstName();
    }

    public function getLastName(){
        return $this->user->getLastName();
    }

    public function getBirthDay(){
        return $this->user->getAge();
    }

    public function getCurrentBonus(){
        return $this->user->getCurrentBonus()->getInputChoise();
    }

    public function getAvBonuses(){
        return $this->user->getAvBonuses();//getInputChoise
    }

    /**
     * Prepares data for Deposits table.
     * @return array table data
     */
    public function getDepositsTable(){
        $qb = $this->em->createQueryBuilder()
            ->select("td.balanceAfter,td.amount,td.status,td.createdAt")
            ->from("CasinoTreasuryBundle:Deposit","td")
            ->where("td.user = :userId")
            ->setParameter("userId", $this->user->getId())
            ->orderBy('td.createdAt','DESC')
            ->setMaxResults(4);
        $res = $qb->getQuery()->getResult();
        foreach ($res as &$row) {
            $row['createdAt'] = $row['createdAt']->format('Y-m-d H:i:s');
        }
        return (is_array($res))? $res : [];
    }

/*    public function getGameLogs(){
        return $this->user->getGameLogs();
    }

    public function getUserAddresses(){
        return $this->user->getUserAddresses();
    }

    public function getUserAffiliates(){
        return $this->user->getUserAffiliates();
    }*/

    public function getAffiliatedBy(){
        return $this->user->getAffiliatedBy();
    }
    /** end of User entity methods */

    public function setRestoreAccountLink($link){
        $this->restoreAccountLink = $link;
    }

    public function getRestoreAccountLink(){
        return $this->restoreAccountLink;
    }

    /** contact form */
    public function setContactSubject($contactSubject){
        $this->contactSubject = $contactSubject;
    }
    public function getContactSubject(){
        return $this->contactSubject;
    }

    public function setContactSenderName($senderName){
        $this->senderName= $senderName;
    }
    public function getContactSenderName(){
        return $this->senderName;
    }

    public function setContactSenderEmail($senderName){
        $this->contactSenderEmail= $senderName;
    }
    public function getContactSenderEmail(){
        return $this->contactSenderEmail;
    }

    public function setContactContent($contactContent){
        $this->contactContent= $contactContent;
    }
    public function getContactContent(){
        return $this->contactContent;
    }
    /** end contact form */

    public function setBonusAmount($bonusAmount){
        $this->bonusAmount = $bonusAmount;
    }
    public function getBonusAmount(){
        return $this->bonusAmount;
    }

    public function setRoute($route){
        $this->route= $route;
    }
    public function getRoute(){
        return $this->route;
    }

}