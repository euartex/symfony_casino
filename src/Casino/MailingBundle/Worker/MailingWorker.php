<?php
namespace Casino\MailingBundle\Worker;

use GearmanJob;
use Laelaps\GearmanBundle\Annotation as Gearman;
use Laelaps\GearmanBundle\Worker;
use Symfony\Component\Console\Output\OutputInterface;
use Casino\MailingBundle\Entity\Mailing;

class MailingWorker extends Worker
{
    /**
    * @Gearman\PointOfEntry(name="mailing_job")
    * @param GearmanJob $job
    * @param Symfony\Component\Console\Output\OutputInterface $output
    * @return boolean returning false means job failure
    */
    public function mailingJob(GearmanJob $job, OutputInterface $output)
    {
        $em = $this->getDoctrine()->getManager();
        try {
            $data = unserialize($job->workload());
            $mailing = $em->getRepository('CasinoMailingBundle:Mailing')->find($data['id']);
            $this->checkStatus($mailing);

            $this->beginMailing($em, $mailing);

            $users = $this->getUsers($em, $mailing);
            $mailing->setTotal(count($users));
            $this->saveMailingState($em, $mailing);
            foreach ($users as $user) {
                if($mailing->getStatus()==Mailing::STATUS_ABORTED){
                    throw new \Exception('Mailing aborted');
                }

                $mailer = $this->get('mailer');
                $message = $mailer->createMessage()
                    ->setSubject($mailing->getTitle())
                    ->setFrom('casino@creawill.com')
                    ->setTo($user->getEmail())
                    ->setBody(
                        str_replace(
                            ['{nickname}'],
                            [$user->getNickname()],
                            $mailing->getContent()
                        ),'text/html');
                $mailer->send($message);
                $output->writeln($user->getEmail());
                $mailing->setProcess($mailing->getProcess()+1);
                $this->saveMailingState($em, $mailing);
            }

            $this->endMailing($em, $mailing);

            $output->writeln('ok');
        }catch (\Exception $e){
            $output->writeln('<error>'.$e->getMessage().'</error>');
        }
    }

    protected function getUsers($em, $mailing)
    {
        $groups = [0];
        foreach ($mailing->getGroups() as $group) {
            $groups[] = $group->getId();
        }

        return $em->createQuery('select u
            from CasinoUserBundle:User u
            JOIN u.ugroups g
            WHERE g.id in (:ids)
        ')->setParameter('ids', $groups)->getResult();
    }

    protected function checkStatus($mailing)
    {
        if($mailing->getStatus()!==Mailing::STATUS_NEW){
            throw new \Exception($mailing->getId().'] mailing is not new');
        }
    }

    protected function beginMailing($em, $mailing)
    {
        $mailing->setStatus(Mailing::STATUS_IN_PROGRESS);
        $this->saveMailingState($em, $mailing);
    }

    protected function endMailing($em, $mailing)
    {
        $mailing->setStatus(Mailing::STATUS_DONE);
        $mailing->setCompleatedAt(new \DateTime("now"));
        $this->saveMailingState($em, $mailing);
    }

    protected function saveMailingState($em, $mailing)
    {
        $em->persist($mailing);
        $em->flush();
    }
}