<?php

namespace Casino\MailingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="mailing")
 */
class Mailing
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @ORM\Column(type="string", length=255) */
    protected $title;

    /**
     * @Assert\Regex("/^[a-z\-0-9]+$/i")
     * @ORM\Column(type="string", length=128) */
    protected $slug;

    /**
     * @ORM\ManyToMany(targetEntity="Casino\UserBundle\Entity\Group")
     * @ORM\JoinTable(name="mailing_groups")
     **/
    protected $groups;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false)
     */
    private $isActive = false;

    /**
     * @ORM\ManyToOne(targetEntity="Casino\MailingBundle\Entity\EmailTemplate")
     * @ORM\JoinColumn(name="template_id", referencedColumnName="id")
     **/
    private $template;

    public function __construct()
    {
        $this->groups = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return Mailing
     */
    public function setIsActive( $isActive)
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }


    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     *
     * @return Mailing
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * @param mixed $groups
     *
     * @return Mailing
     */
    public function setGroups($groups)
    {
        $this->groups = $groups;
        return $this;
    }


    /**
     * Add group
     *
     * @param \Casino\UserBundle\Entity\Group $group
     *
     * @return Mailing
     */
    public function addGroup(\Casino\UserBundle\Entity\Group $group)
    {
        $this->groups[] = $group;

        return $this;
    }

    /**
     * Remove group
     *
     * @param \Casino\UserBundle\Entity\Group $group
     */
    public function removeGroup(\Casino\UserBundle\Entity\Group $group)
    {
        $this->groups->removeElement($group);
    }

    /**
     * Set template
     *
     * @param \Casino\MailingBundle\Entity\EmailTemplate $template
     *
     * @return Mailing
     */
    public function setTemplate(\Casino\MailingBundle\Entity\EmailTemplate $template = null)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * Get template
     *
     * @return \Casino\MailingBundle\Entity\EmailTemplate
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Mailing
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }
}
