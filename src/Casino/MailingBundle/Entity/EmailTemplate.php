<?php

namespace Casino\MailingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Email template
 *
 * @ORM\Table(name="email_templates")
 * @ORM\Entity
 */
class EmailTemplate
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email_from", type="string", length=255, nullable=false)
     */
    private $emailFrom;

    /**
     * @var string
     *
     * @ORM\Column(name="email_from_name", type="string", length=255, nullable=false)
     */
    private $emailFromName;

    /**
     * @var string
     *
     * @ORM\Column(name="email_subject", type="string", length=255, nullable=false)
     */
    private $subject;

    /**
     * @var string
     *
     * @ORM\Column(name="vars", type="json_array", nullable=true)
     */
    private $vars;

    /**
     * @var string
     *
     * @ORM\Column(name="template_name", type="string", length=255, nullable=false)
     */
    private $templateName;

    /**
     * @var string
     *
     * @ORM\Column(name="template_id", type="string", length=255, nullable=false)
     */
    private $templateId;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set emailFrom
     *
     * @param string $emailFrom
     *
     * @return EmailTemplate
     */
    public function setEmailFrom($emailFrom)
    {
        $this->emailFrom = $emailFrom;

        return $this;
    }

    /**
     * Get emailFrom
     *
     * @return string
     */
    public function getEmailFrom()
    {
        return $this->emailFrom;
    }

    /**
     * Set emailFromName
     *
     * @param string $emailFromName
     *
     * @return EmailTemplate
     */
    public function setEmailFromName($emailFromName)
    {
        $this->emailFromName = $emailFromName;

        return $this;
    }

    /**
     * Get emailFromName
     *
     * @return string
     */
    public function getEmailFromName()
    {
        return $this->emailFromName;
    }

    /**
     * Set subject
     *
     * @param string $subject
     *
     * @return EmailTemplate
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

  /**
     * Set template name
     *
     * @param string $templateName
     *
     * @return EmailTemplate
     */
    public function setTemplateName($templateName)
    {
        $this->templateName = $templateName;

        return $this;
    }

    /**
     * Get templateName
     *
     * @return string
     */
    public function getTemplateName()
    {
        return $this->templateName;
    }

    /**
     * @return string
     */
    public function getTemplateId()
    {
        return $this->templateId;
    }

    /**
     * @param string $templateId
     * @return EmailTemplate
     */
    public function setTemplateId($templateId)
    {
        $this->templateId = $templateId;
        return $this;
    }

    /**
     * Set vars
     *
     * @param array $vars
     *
     * @return EmailTemplate
     */
    public function setVars($vars)
    {
        $this->vars = json_encode(array_unique($vars));

        return $this;
    }

    /**
     * Get vars
     *
     * @return array
     */
    public function getVars()
    {
        return json_decode($this->vars, true);
    }
}
