<?php

namespace Casino\MediaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{

    const SLIDES_UPLOAD_DIR = 'assets/slides/';
    const WEBFOLDER = '../../../../web/';

    public function indexAction()
    {
        return $this->render('CasinoMediaBundle:Default:index.html.twig');
    }

    public function uploadImageAction($path)
    {

    }

    public function sliderAction(Request $request)
    {
        /** @var array $slidesData */
        $slidesData = unserialize($this->get('craue_config')->get('main.slider'));

        /** @var Form $form */
        $form = $this->container->get('form.factory')
            ->createNamedBuilder('cruciform', 'form', array(), array())
            ->setMethod('POST')
            ->add('file','file')
            ->add('submit', 'submit', array(
                'label' => 'Add an slide',
                'attr' => array('class' => 'submit button_decoration wide-btn'),
            ))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isValid()){

           // dump( $form->isValid());

            /** @var UploadedFile $slide */
            $slide = $form->get('file')->getData();
            $slideName = $slide->getClientOriginalName();


            //move uploaded file to location
            $slide->move(
                 __DIR__ . '/' . self::WEBFOLDER . self::SLIDES_UPLOAD_DIR,
                $slideName
            );

            $msg = 'Your changes were saved!';
            if (!in_array($slideName, $slidesData)){
                $slidesData[]= $slideName;
            } else {
                $msg .= ' Image was overwritten.';
            }

            $this->get('craue_config')->set('main.slider', serialize($slidesData) );

            $request->getSession()->getFlashBag()->add( 'warning', $msg );

//            return $this->redirect($this->generateUrl(
//                'casino_media_slider'
//            ));
        }

        return $this->render('CasinoMediaBundle:DefaultAdmin:slider.html.twig', ['form' => $form->createView(), 'slides'=>$slidesData]);
    }

    public function sliderDeleteAction( Request $request)
    {
        //position in array
        $id = intval($request->get('id'));
        //name of file
        $name = strval($request->get('name'));

        /** @var array $slidesData */
        $slidesData = unserialize($this->get('craue_config')->get('main.slider'));

        if ( isset($slidesData[$id]) && ($slidesData[$id] == $name) ){
            if (file_exists('../web/' . self::SLIDES_UPLOAD_DIR . $name)){
                unlink('../web/' . self::SLIDES_UPLOAD_DIR . $name);
            }

            unset($slidesData[$id]);
            $this->get('craue_config')->set('main.slider', serialize($slidesData) );
            $request->getSession()->getFlashBag()->add( 'warning', 'Slide has been deleted' );
        }

        return $this->redirect($this->generateUrl(
            'casino_media_slider'
        ));
    }
}
