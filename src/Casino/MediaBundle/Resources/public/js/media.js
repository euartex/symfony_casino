/**
 * Created by melnik on 12.11.14.
 */

function handleMediaSimplePhoto(evt, img) {
    var files = evt.target.files; // FileList object

    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {

        // Only process image files.
        if (!f.type.match('image.*')) {
            continue;
        }

        var reader = new FileReader();

        // Closure to capture the file information.
        reader.onload = (function(theFile) {
            return function(e) {
               $(img).attr('src',e.target.result);

              /*  var data_ob = {
                    pic_name: theFile.name,
                    pic_type: theFile.type,
                    pic_data: e.target.result.split("base64,")[1]
                };
                $.post('/media/upload-image/'+path, data_ob, funcion(res){

                });*/
            };
        })(f);

        // Read in the image file as a data URL.
        reader.readAsDataURL(f);
        break;
    }
}
