<?php

namespace Casino\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Casino\UserBundle\Entity\UserAffiliate;
use Casino\UserBundle\Form\UserAffiliateType;

/**
 * UserAffiliate controller.
 *
 */
class UserAffiliateController extends Controller
{

    /**
     * Lists all UserAffiliate entities.
     *
     */
    public function indexAction()
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if (!is_object($user)) {
            return $this->redirect(
                $this->generateUrl(
                    'casino_default_homepage_login'
                )
            );
        }

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CasinoUserBundle:UserAffiliate')->findBy(['user'=>$user]);

        return $this->render('CasinoUserBundle:UserAffiliate:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new UserAffiliate entity.
     *
     */
    public function createAction(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if (!is_object($user)) {
            return $this->redirect(
                $this->generateUrl(
                    'casino_default_homepage_login'
                )
            );
        }
        $entity = new UserAffiliate();
        $entity->setUser($user);
        $entity->setHash( mb_substr( hash('sha256', $user->getId().'_'.$entity->getPromoName()),0,8) );
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('useraffiliate_show', array('id' => $entity->getId())));
        }

        return $this->render('CasinoUserBundle:UserAffiliate:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a UserAffiliate entity.
     *
     * @param UserAffiliate $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(UserAffiliate $entity)
    {
        $form = $this->createForm(new UserAffiliateType(), $entity, array(
            'action' => $this->generateUrl('useraffiliate_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create', 'attr'=>array('class' => 'button_decoration edit-left-btn')));

        return $form;
    }

    /**
     * Displays a form to create a new UserAffiliate entity.
     *
     */
    public function newAction()
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if (!is_object($user)) {
            return $this->redirect(
                $this->generateUrl(
                    'casino_default_homepage_login'
                )
            );
        }

        $entity = new UserAffiliate();
        $form   = $this->createCreateForm($entity);

        return $this->render('CasinoUserBundle:UserAffiliate:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a UserAffiliate entity.
     *
     */
    public function showAction($id)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if (!is_object($user)) {
            return $this->redirect(
                $this->generateUrl(
                    'casino_default_homepage_login'
                )
            );
        }

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CasinoUserBundle:UserAffiliate')->findOneBy(['id'=>$id, 'user'=>$user]);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find UserAffiliate entity.');
        }

        $affiliatedUsers = $em->getRepository('CasinoUserBundle:User');
        $qb = $affiliatedUsers->createQueryBuilder('a');
        $qb->select('COUNT(a)');
        $qb->where('a.affiliatedBy = :aff');
        $qb->setParameter('aff', 1);
        $affiliatedCount = $qb->getQuery()->getSingleScalarResult();

        return $this->render('CasinoUserBundle:UserAffiliate:show.html.twig', array(
            'entity'      => $entity,
            'affiliated' => $affiliatedCount
        ));
    }

    /**
     * Displays a form to edit an existing UserAffiliate entity.
     *
     */
    public function editAction($id)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if (!is_object($user)) {
            return $this->redirect(
                $this->generateUrl(
                    'casino_default_homepage_login'
                )
            );
        }

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CasinoUserBundle:UserAffiliate')->findOneBy(['id'=>$id, 'user'=>$user ]);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find UserAffiliate entity.');
        }

        $editForm = $this->createEditForm($entity);

        return $this->render('CasinoUserBundle:UserAffiliate:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a UserAffiliate entity.
    *
    * @param UserAffiliate $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(UserAffiliate $entity)
    {
        $form = $this->createForm(new UserAffiliateType(), $entity, array(
            'action' => $this->generateUrl('useraffiliate_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update', 'attr' => [ 'class' => 'button_decoration
        edit-left-btn'] ));

        return $form;
    }
    /**
     * Edits an existing UserAffiliate entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if (!is_object($user)) {
            return $this->redirect(
                $this->generateUrl(
                    'casino_default_homepage_login'
                )
            );
        }

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CasinoUserBundle:UserAffiliate')->findOneBy(['id'=>$id, 'user'=> $user]);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find UserAffiliate entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('useraffiliate_edit', array('id' => $id)));
        }

        return $this->render('CasinoUserBundle:UserAffiliate:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ));
    }

}
