<?php

namespace Casino\UserBundle\Controller;

use Casino\GameBundle\Entity\GameLog;
use Casino\TreasuryBundle\Entity\Deposit;
use Casino\TreasuryBundle\Entity\TreasuryDepositBonuses;
use Casino\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ApiController extends Controller
{
    /**
     * @param $token
     * @return string
     */
    public function profileAction($token)
    {
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->findUserBy(['apiToken' => $token]);
        $gameLog = $this->getGameByToken($token, true);
        if (!is_object($gameLog)){
            throw new Exception('Game not found',404);
        }

        $balance = ($gameLog->getType())? $user->getBalance() : $user->getBonusBalance();
        $response = new JsonResponse();
        if (is_object($user)) {

            $response->setData(
                [
                    'status'  => 'success',
                    'profile' => [
                        'id'      => $user->getId(),
                        'login'   => $user->getUsername(),
                        'name'    => $user->getNickname(),
                        'balance' => (float)$balance,
                    ]
                ]
            );
        } else {
            $response->setData(
                [
                    'status' => 'error',
                ]
            );
        }

        return $response;
    }

    /**
     * @param $token
     * @return string
     */
    public function treasuryAction($token, Request $request)
    {
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var User $user */
        $user = $userManager->findUserBy(['apiToken' => $token]);

        $response = new JsonResponse();
        if ($user && $request->get('secure') == md5($request->get('money').'m8PaB=Uc&-STekep')) {
            $em = $this->getDoctrine()->getManager();
            /** @var GameLog $gameLog */
            $gameLog = $this->getGameByToken( $token );

            if(is_object($gameLog)) {

                $newGame = $this->processDuplicateGame($gameLog, $request);

                $isWagerCompleted = false;
                $isRealMoneyGame = $newGame->getType();
                if ($isRealMoneyGame) {
                    $newMoneyValue = $user->getBalance() + (float)$request->get('money');
                    $user->setBalance($newMoneyValue);
                    $newGame->setBalanceAfter($newMoneyValue);
                } else {

                    /** @var TreasuryDepositBonuses $userCurrentBonus */
                    $userCurrentBonus = $user->getCurrentBonus();
                    if (!is_object($userCurrentBonus)) {
                        throw new Exception('You dont have any active bonuses',404);
                    }

                    if (!$userCurrentBonus->getIsActive() || ($userCurrentBonus->getAmount() <=0)){
                        $userCurrentBonus->setIsActive(false);
                        $userCurrentBonus->setExpireReason(TreasuryDepositBonuses::BY_TIME);

                        $user->setBonusBalance(0);
                        $user->setCurrentBonus(null);
                        $em->persist($user);
                        $em->persist($userCurrentBonus);
                        $em->flush();
                        throw new Exception('Bonus expired', 423);
                    }

                    $userCurrentBonus->setChangedAt(new \DateTime());
                    $userCurrentBonus->setBalanceBefore($userCurrentBonus->getAmount());
                    $userCurrentBonus->setAmount((float) $userCurrentBonus->getAmount() + (float)$request->get('money'));
                    $userCurrentBonus->setWage((float) $userCurrentBonus->getWage() + (float)($request->get('bet') * $userCurrentBonus->getBonus()->getGameRate()));


                    $currentBonusBalance = (float) $newGame->getBalanceBefore() + (float)$request->get('money');
                    $newGame->setBalanceAfter( $currentBonusBalance );
                    $user->setBonusBalance( $currentBonusBalance );

                    // if wager is completed
                    if ($userCurrentBonus->getWage() >= $userCurrentBonus->getTotalWage()){
                        $isWagerCompleted = true;

                        $userCurrentBonus->setExpireReason(TreasuryDepositBonuses::BY_WAGER);
                        $userCurrentBonus->setIsCashedOut(true);
                        $userCurrentBonus->setIsActive(false);
                        $user->setBonusBalance(0);
                        $user->setBalance($user->getBalance() + (float)$userCurrentBonus->getWage());
                        $user->setCurrentBonus(null);

                        $newDeposit = $this->createDeposit($user, $userCurrentBonus->getAmount(), $userCurrentBonus);
                        $em->persist($newDeposit);

                        $user->addDeposit($newDeposit);
                    }
                    $em->persist($userCurrentBonus);
                }
                $userManager->updateUser($user);
                $em->persist($newGame);
                $em->flush();

                // duplicated if because mail should be sended after flush()
                if ($isWagerCompleted){
                    $this->sendWagerMail($user, $userCurrentBonus->getAmount());
                }
            }

            $response->setData(
                [
                    'status'  => 'success',
                    'profile' => [
                        'id'      => $user->getId(),
                        'login'   => $user->getUsername(),
                        'money'    => (float)$request->get('money'),
                        'balance' => (float)$newGame->getBalanceAfter(),
                    ]
                ]
            );
        } else {
            $response->setData(['status' => 'error']);
        }

        return $response;
    }

    /**
     * @param string $token md5 hashsum
     * @param bool $silent to wheser to throw exeption or not
     * @return object of GameLog
     */
    protected function getGameByToken($token, $silent = false)
    {
        $em = $this->getDoctrine()->getManager();
        $gameLog = $em->getRepository('CasinoGameBundle:GameLog')->findOneBy([ 'token' => $token ], ['changedAt' => 'DESC']);
        if ( !is_object($gameLog) && $silent == false ) {
            throw new Exception('GameLog not found', 404);
        }
        return $gameLog;
    }

    /**
     * @param $gameLog GameLog
     * @param $request Request
     * @return GameLog
     */
    private function processDuplicateGame($gameLog, $request)
    {
        $newGame = null;
        $now = new \DateTime();
        $gameLog->setBalanceDelta($request->get('money'));
        $gameLog->setChangedAt($now);
        if ($gameLog->getStatus() === 'processed')
        {//create new row, do not modify existing.
            $newGame = clone ($gameLog);
            $this->getDoctrine()->getManager()->detach($gameLog);
            $newGame->setBalanceBefore($gameLog->getBalanceAfter()); //preserving old value
            $newGame->setCreatedAt($now);
            return $newGame;
        } else { // we dont have games with this token yet
            $gameLog->setStatus(GameLog::STATUS_PROCESSED);
            return $gameLog;
        }
    }

    /**
     * @param User $user
     * @param float $amount
     */
    private function sendWagerMail($user, $amount) {
        $em = $this->getDoctrine()->getManager();
        $emailTemplate = $em->getRepository('CasinoMailingBundle:Mailing')->findOneBy(['slug' => 'en-wager-completed'])->getTemplate();

        if (!$emailTemplate) {
            throw new Exception('Email template is absent. Please contact administration', 404);
        }

        $mailer = $this->get('casino.mailing.mailer');
        $mailer->addToUser($user);
        $params = [
            '{{Route}}' => $this->generateUrl('casino_users_get_bonuses',[],UrlGeneratorInterface::ABSOLUTE_URL),
            '{{BonusAmount}}' => $amount,
        ];
        $mailer->send($emailTemplate, $params);

    }


    private function createDeposit($user, $amount, TreasuryDepositBonuses $bonus){
        $userDeposit = new Deposit();
        $userDeposit->setBalanceBefore($user->getBalance());
        $userDeposit->setStatus(Deposit::STATUS_SUCCESS);
        $userDeposit->setTo(Deposit::TO_REAL);
        $userDeposit->setFrom(Deposit::FROM_WAGER);
        $userDeposit->setAmount($amount);
        $userDeposit->setBalanceAfter($user->getBalance() + $amount);
        $userDeposit->setDetailsJson(json_encode( array('from'=> 'wager', 'dest'=>'user' ,'bonus_id' => $bonus->getId() )));
        $userDeposit->setUser($user);
        return $userDeposit;
    }
}
