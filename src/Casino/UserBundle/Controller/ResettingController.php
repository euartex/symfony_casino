<?php

namespace Casino\UserBundle\Controller;

use Casino\UserBundle\Entity\User;
use Exception;
use FOS\UserBundle\Controller\ResettingController as BaseController;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\User\UserInterface;

use Hip\MandrillBundle\Message;
use Hip\MandrillBundle\Dispatcher;

class ResettingController extends BaseController
{
    /**
     * Request reset user password: submit form and send email
     */
    public function sendEmailAction(Request $request)
    {
        $username = $request->request->get('username');

        /** @var $user User */
        $user = $this->get('fos_user.user_manager')->findUserByUsernameOrEmail($username);

        if (null === $user) {
            return $this->redirectIfAjaxResponse(array(
                'status' => 'error',
                'data'=>'Invalid username',
            ),$request);
        }

        if ($user->isPasswordRequestNonExpired($this->container->getParameter('fos_user.resetting.token_ttl'))) {
            return $this->redirectIfAjaxResponse(
                array(
                    'status' => 'error',
                    'data'=>'Resetting request for this email is not completed yet.',
                    ),
                $request);
        }

        if (null === $user->getConfirmationToken()) {
            /** @var $tokenGenerator \FOS\UserBundle\Util\TokenGeneratorInterface */
            $tokenGenerator = $this->get('fos_user.util.token_generator');
            $user->setConfirmationToken($tokenGenerator->generateToken());
        }

        $em = $this->getDoctrine()->getManager();
        $emailTemplate = $em->getRepository('CasinoMailingBundle:Mailing')->findOneBy(['slug' => 'en-resetpassword'])->getTemplate();

        if (!$emailTemplate) {
            throw new Exception('Email template is absent. Please contact administration', 404);
        }

        $resettingUrl = $this->get('router')->generate('fos_user_resetting_reset', array('token' => $user->getConfirmationToken()), true);

        $mailer = $this->get('casino.mailing.mailer');
        $params = [
            '{{RestoreAccountLink}}' => $resettingUrl,
        ];
        $mailer->addToUser($user);
        $mailer->send($emailTemplate, $params);

        $user->setPasswordRequestedAt(new \DateTime());
        $this->get('fos_user.user_manager')->updateUser($user);

        return $this->redirectIfAjaxResponse(
            array(
                'status' => 'ok',
                'data'=>'Please check your email to reset password. ',
            ),
            $request);
    }

    private function redirectIfAjaxResponse($data,Request $request) {
        if(!$request->isXmlHttpRequest()){
            return $this->render('CasinoUserBundle:Resetting:checkEmail.html.twig', array(
                'data' => $data['data'],
            ));
        } else {
            $response = new JsonResponse();
            $response->setData($data);
            return $response;
        }
    }

    /**
     * Reset user password
     */
    public function resetAction(Request $request, $token)
    {

        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.resetting.form.factory');
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $user = $userManager->findUserByConfirmationToken($token);


        if (null === $user) {
            return $this->render('FOSUserBundle:Resetting:token_error.html.twig');
        }


        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::RESETTING_RESET_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $formFactory->createForm();
        $form->setData($user);

        $form->handleRequest($request);

        //isValid checks for submitted == true, we are redirected here
        if (!count($form->getErrors(true)) && $request->getMethod() == 'POST' ) {

            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::RESETTING_RESET_SUCCESS, $event);

            $userManager->updateUser($user);

            if (null === $response = $event->getResponse()) {
                $url = $this->generateUrl('fos_user_profile_show');
                $response = new RedirectResponse($url);
            }

            $dispatcher->dispatch(FOSUserEvents::RESETTING_RESET_COMPLETED, new FilterUserResponseEvent($user, $request, $response));
            return $response;
        }

        return $this->render('FOSUserBundle:Resetting:reset.html.twig', array(
            'token' => $token,
            'form' => $form->createView(),
        ));
    }

    /**
     * Tell the user to check his email provider
     */
    public function checkEmailAction(Request $request)
    {
        $email = $request->query->get('email');

        if (empty($email)) {
            // the user does not come from the sendEmail action
            return new RedirectResponse($this->generateUrl('fos_user_resetting_request'));
        }

        return $this->redirectIfAjaxResponse(
            array(
                'status' => 'ok',
                'data'=>'Please check your email to reset password.',
            ),
            $request);
    }
}
