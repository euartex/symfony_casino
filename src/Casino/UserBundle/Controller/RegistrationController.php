<?php

namespace Casino\UserBundle\Controller;

use Casino\UserBundle\Entity\User;
use Exception;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Util\TokenGenerator;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Controller\RegistrationController as BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RegistrationController extends BaseController
{
    public function registerAction(Request $request)
    {
        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $user = $userManager->createUser();



        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $formFactory->createForm();
        $form->setData($user);

        $form->handleRequest($request);

        if ($form->isValid()) {
            if ($request->get('btag')){
                $em = $this->getDoctrine()->getManager();
                $affiliate = $em->getRepository('CasinoUserBundle:UserAffiliate')->findOneBy( array( 'hash' => $request->get('btag'), 'isActive'=> true ));
                if (is_object($affiliate)) {
                         $user->setAffiliatedBy( $affiliate );
                }
            }


//            $event = new FormEvent($form, $request);
//            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);
            $user->setEnabled(false);
            if (null === $user->getConfirmationToken()) {
                $tokenGenerator = new TokenGenerator();
                $user->setConfirmationToken($tokenGenerator->generateToken());
            }

            $this->sendConfirmationMail($user);

            $session = new Session();
            $session->set('fos_user_send_confirmation_email/email', $user->getEmail());

            $userManager->updateUser($user);

//            if (null === $response = $event->getResponse()) {
//                $url = $this->generateUrl('fos_user_registration_confirmed');
//                $response = new RedirectResponse($url);
//            }
            $redirectEmailRoute = $this->generateUrl('fos_user_registration_check_email');
            $response = new RedirectResponse($redirectEmailRoute);

            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_COMPLETED, new FilterUserResponseEvent($user, $request, $response));

            return $response;
        }

        return $this->render('FOSUserBundle:Registration:register.html.twig', array(
            'form' => $form->createView(), 'btag' => $request->cookies->get('btag')
        ));
    }

    /**
     * Tell the user to check his email provider
     */
    public function checkEmailAction()
    {
        $email = $this->get('session')->get('fos_user_send_confirmation_email/email');
        $this->get('session')->remove('fos_user_send_confirmation_email/email');
        $user = $this->get('fos_user.user_manager')->findUserByEmail($email);

        if (null === $user) {
            return $this->redirect(
                $this->generateUrl(
                    'casino_default_homepage'
                )
            );
        }

        return $this->render('CasinoUserBundle:Registration:checkEmail.html.twig', array(
            'user' => $user,
        ));
    }
//
//    /**
//     * Receive the confirmation token from user email provider, login the user
//     */
//    public function confirmAction(Request $request, $token)
//    {
//        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
//        $userManager = $this->get('fos_user.user_manager');
//
//        $user = $userManager->findUserByConfirmationToken($token);
//
//        if (null === $user) {
//            throw new NotFoundHttpException(sprintf('The user with confirmation token "%s" does not exist', $token));
//        }
//
//        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
//        $dispatcher = $this->get('event_dispatcher');
//
//        $user->setConfirmationToken(null);
//        $user->setEnabled(true);
//
//        $event = new GetResponseUserEvent($user, $request);
//        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_CONFIRM, $event);
//
//        $userManager->updateUser($user);
//
//        if (null === $response = $event->getResponse()) {
//            $url = $this->generateUrl('fos_user_registration_confirmed');
//            $response = new RedirectResponse($url);
//        }
//
//        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_CONFIRMED, new FilterUserResponseEvent($user, $request, $response));
//
//        return $response;
//    }
//
//    /**
//     * Tell the user his account is now confirmed
//     */
//    public function confirmedAction()
//    {
//        $user = $this->getUser();
//        if (!is_object($user) || !$user instanceof UserInterface) {
//            throw new AccessDeniedException('This user does not have access to this section.');
//        }
//
//        return $this->render('FOSUserBundle:Registration:confirmed.html.twig', array(
//            'user' => $user,
//        ));
//    }
//
//    private function sendConfirmationEmailMessage($user){
//        $em = $this->getDoctrine()->getManager();
//        $emailTemplate = $em->getRepository('CasinoMailingBundle:Mailing')->findOneBy(['slug' => 'en-wager-completed'])->getTemplate();
//
//        if (!$emailTemplate) {
//            throw new Exception('Email template is absent. Please contact administration', 404);
//        }
//
//        $activationURL = $this->get('router')->generate('fos_user_registration_confirm', array('token' => $user->getConfirmationToken()), true);
//
//        $service = $this->get('casino.mailing.mandrill.mail.handler');
//        $service->addToUser($user);
//        $service->setRoute($activationURL);
//        $service->handleMail($emailTemplate);
//
//    }

    public function checkAction()
    {
        dump('hallo');
        throw new \RuntimeException('You must configure the check path to be handled by the firewall using form_login in your security firewall configuration.');
    }

    /**
     * @param User $user
     * @throws Exception
     */
    private function sendConfirmationMail($user) {
        $em = $this->getDoctrine()->getManager();
        $emailTemplate = $em->getRepository('CasinoMailingBundle:Mailing')
                            ->findOneBy(['slug' => 'en-activation-mail'])
                            ->getTemplate();

        if (!$emailTemplate) {
            throw new Exception('Email template is absent. Please contact administration', 404);
        }
        $mailer = $this->get('casino.mailing.mailer');
        $route = $this->generateUrl('fos_user_registration_confirm', array('token' => $user->getConfirmationToken()), true);
        $params = [
            '{{Route}}' => $route,
        ];
        $mailer->addToUser($user);
        $mailer->send($emailTemplate, $params);
        // TODO: rm
//        $service = $this->get('casino.mailing.mandrill.mail.handler');
//        $service->addToUser($user);
//        $route = $this->generateUrl('fos_user_registration_confirm', array('token' => $user->getConfirmationToken()), true);
//        $service->setRoute($route);
//        $service->handleMail($emailTemplate);
    }
}
