<?php

namespace Casino\UserBundle\Controller;

use APY\DataGridBundle\Grid\Column\Column;
use Casino\DefaultBundle\Controller\BaseAdminController;
use Casino\UserBundle\Entity\Group;
use Casino\UserBundle\Entity\User;
use Doctrine\ORM\QueryBuilder;
use APY\DataGridBundle\Grid\Source\Entity;
use APY\DataGridBundle\Grid\Action\RowAction;
use APY\DataGridBundle\Grid\Column\NumberColumn;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Acl\Exception\Exception;

class GroupAdminController extends BaseAdminController
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $source = new Entity('CasinoUserBundle:Group');
        $grid = $this->get('grid');
        $grid->setSource($source);
        $grid->setId('id');
        $grid->setLimits([5, 10, 15]);
        
        $grid->hideColumns(['id']);
        
        $MyColumn2 = new NumberColumn(
            ['title' => 'Count users', 'filterable' => false , 'sortable' => false]
        );
        
        $MyColumn2->manipulateRenderCell(
            function ($value, $row, $router) use ($em) {
                $group = $em->getRepository('CasinoUserBundle:Group')
                    ->findOneById($row->getField("id"));
                return $group->getUsers()->count();
            }
        );
        
        $grid->addColumn($MyColumn2);

        $userColumns = [
            'id',
            'name'
        ];
        
        $grid->setColumnsOrder($userColumns);

        $rowAction = new RowAction('Change group name', 'casino_admin_group_edit');
        $rowAction->setRouteParameters(['id']);
        $grid->addRowAction($rowAction);

        $rowAction = new RowAction('Edit members', 'casino_admin_group_users');
        $rowAction->setRouteParameters(['id']);
        $grid->addRowAction($rowAction);
        
        if ($grid->isReadyForRedirect()) {
            return $grid->getGridResponse();
        }

        return $this->render(
            'CasinoUserBundle:GroupAdmin:index.html.twig',
            ['grid' => $grid]
        );
    }

    public function editAction(Request $request, $id=null)
    {
        $em = $this->getDoctrine()->getManager();
        if ($id) {
            $group = $em->getRepository('CasinoUserBundle:Group')
                ->findOneById($id);
        } else {
            $group = new \Casino\UserBundle\Entity\Group();
        }


        $form = $this->createFormBuilder($group)
            ->add('name', 'text')
            ->getForm();
        
        $form->handleRequest($request);
        if (count($form->getErrors())) {
            $em->persist($group);
            $em->flush();

            $request->getSession()->getFlashBag()->add(
                'notice',
                'Your changes were saved!'
            );
            
            return $this->redirect(
                $this->generateUrl(
                    'casino_admin_group_edit',
                    ['id' => $group->getId()]
                )
            );
        }

        return $this->render(
            'CasinoUserBundle:GroupAdmin:edit.html.twig',
            ['form' => $form->createView(), 'obj' => $group]
        );
    }

    public function usersAction(Request $request, $id)
    {
        $id = intval($id);
        $em = $this->getDoctrine()->getManager();
        /** @var Group $group */
        $group = $em->getRepository('CasinoUserBundle:Group')->findOneById($id);
        if (is_null($group)){
            throw new Exception('Group not found', 404);
        }

        $source = new Entity('CasinoUserBundle:User');
        $tableAlias = $source->getTableAlias();
        $source->manipulateQuery(
            function (QueryBuilder $query) use ($tableAlias, $id) {
                //$query->addSelect('g.name');
                $query->leftJoin( $tableAlias.'.ugroups', "g");
                $query->andWhere( 'g.id = '.$id);
            }
        );

        $grid = $this->get('grid');
        $grid->setSource($source);
        $grid->setLimits([15, 20, 30]);

        //hide all cols except username
        $gridCols = [];
        /** @var Column $c */
        foreach ($grid->getColumns() as $c){
            $gridCols[] = $c->getId();
        }
        $grid->hideColumns($gridCols);
        $grid->showColumns('username');

        $removeFromGroupAction = new RowAction('Remove from group', 'casino_admin_group_banish',true);
        $removeFromGroupAction->setRouteParameters(['id','group' => $group->getId()]);
        $grid->addRowAction($removeFromGroupAction);

        $grid->setId('id');
        $form = $this->createFormBuilder()
            ->add('user','text',
                ['label'=>'Username or email'])
            ->add('submit','submit', ['label'=>'Add user'])
            ->getForm();

        $form->handleRequest($request);
        if ( $form->isValid() ){
            $user = $form->get('user')->getData();
            /** @var $user User */
            $user = $this->get('fos_user.user_manager')->findUserByUsernameOrEmail($user);
            if ($user instanceof User) {
                if (!$user->hasUGroup($group->getName())){
                    $group->addUser($user);
                    $user->addUgroup($group);
                    $em->persist($user);
                }
                $em->persist($group);
                $em->flush();
            } else {
                $form->get('user')->addError(new FormError('User not found!'));
            }
        }

        if ($grid->isReadyForRedirect()) {
            return $grid->getGridResponse();
        }

        return $this->render(
            'CasinoUserBundle:GroupAdmin:groupUsers.html.twig',
            ['grid' => $grid, 'group'=>$group->getName(), 'form'=>$form->createView()]
        );
    }

    public function banishUserAction(Request $request, $id, $group)
    {
        $id = intval($id);
        $group = intval($group);
        $em = $this->getDoctrine()->getManager();
        /** @var Group $group */
        $group = $em->getRepository('CasinoUserBundle:Group')->findOneById($group);
        if( !($group instanceof Group)){
            throw new \Exception('Group not found.' ,404);
        }
        $user = $em->getRepository('CasinoUserBundle:User')->findOneById($id);
        if( !($user instanceof User)){
            throw new \Exception('User not found.' ,404);
        }

        $user->removeUgroup($group);
        //$group->removeUser($user);
        $em->persist($group);
        $em->flush();

        $request->getSession()->getFlashBag()->add(
            'notice',
            'User '.$user->getUsername().' has been remover from this group'
        );
        return $this->redirect(
            $this->generateUrl(
                'casino_admin_group_users',['id'=>$group->getId()]
            )
        );
    }
   
}
