<?php

namespace Casino\UserBundle\Controller;

use APY\DataGridBundle\Grid\Action\RowAction;
use APY\DataGridBundle\Grid\Column\TextColumn;
use APY\DataGridBundle\Grid\Source\Entity;
use Casino\DefaultBundle\Controller\BaseAdminController;
use Casino\UserBundle\Entity\User;
use Casino\UserBundle\Entity\UserDoc;
use Casino\UserBundle\Form\UserDocType;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\Request;

class DocsAdminController extends BaseAdminController
{

    public function indexAction($userid = null)
    {
        $user = null;
        if (!is_null($userid)) {
            $userid = intval($userid);
            /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
            $userManager = $this->get('fos_user.user_manager');
            /** @var User $user */
            $user = $userManager->findUserBy(['id' => $userid]);
            if (!is_object($user)) {
                throw new \Exception('User not found!');
            }
        }

        $source = new Entity('CasinoUserBundle:UserDoc');

        $tableAlias = $source->getTableAlias();
        $source->manipulateQuery(
            function (QueryBuilder $query) use ($tableAlias, $userid) {
                $query->leftJoin( 'Casino\UserBundle\Entity\User', "user", "WITH", $tableAlias . ".user = user.id");
                $query->orderBy($tableAlias . '.updatedAt','DESC');
                if ( !is_null($userid) ){
                    $query->andWhere('user.id ='.$userid);
                } else {
                    $query->addSelect( 'user.username');
                }
            }
        );

        $grid = $this->get('grid');

        if ( is_null($userid) ) {
            $userName = new TextColumn(
                [
                    'id'=>'username',
                    'title'=>'User name',
                    'field' => 'user.username',
                    // 'isManualField' => true,
                    'isAggregate' => false, // Defaults to false, set true if using aggregate func. like SUM()
                    'source' => true,
                    'filterable' => true,
                    'sortable' => true
                ]);
            $grid->addColumn($userName);
        }
        
        $grid->setSource($source);
        $grid->setId('id');
        $grid->setLimits([5, 10, 15]);

        $grid->getColumn('updatedAt')->setTitle('Status changed at');
        $grid->getColumn('doc')->setTitle('Document');
        $grid->getColumn('validation')->setTitle('Validation status');
        $grid->getColumn('validation')->manipulateRenderCell(
            function($value, $row) {
                return ((in_array($value,array_keys(UserDoc::$statuses))))?UserDoc::$statuses[$value]:UserDoc::$statuses[UserDoc::USER_DOCUMENT_STATUS_NEW];
            }
        );

        $approveAction = new RowAction('Mark as approved', 'docs_admin_check');
        $approveAction->setRouteParameters(['id','status' =>  UserDoc::$statuses[UserDoc::USER_DOCUMENT_STATUS_APPROVED]]);
        $grid->addRowAction($approveAction);

        $declineAction = new RowAction('Mark as declined', 'docs_admin_check');
        $declineAction->setRouteParameters(['id','status' => UserDoc::$statuses[UserDoc::USER_DOCUMENT_STATUS_DECLINED]]);
        $grid->addRowAction($declineAction);

        $rowAction = new RowAction('Delete', 'docs_admin_delete');
        $rowAction->setRouteParameters(['id']);
        $grid->addRowAction($rowAction);

        if ($grid->isReadyForRedirect()) {
            return $grid->getGridResponse();
        }

        return $this->render(
            'CasinoUserBundle:DocsAdmin:index.html.twig',
            ['grid' => $grid,
            'user' => $user]
        );

    }

    public function createAction(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if (!is_object($user)) {
            return $this->redirect(
                $this->generateUrl(
                    'fos_user_security_login'
                )
            );
        }

        $entity = new UserDoc();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setUser($user);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('docs_admin_index'));
        }

        return $this->render('CasinoUserBundle:DocsAdmin:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a EmailTemplate entity.
     *
     * @param UserDoc $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(UserDoc $entity)
    {
        $form = $this->createForm(new UserDocType(), $entity, array(
            'action' => $this->generateUrl('docs_admin_create'),
            'method' => 'POST',
        ));


        $form->add('submit', 'submit', array('label' => 'Upload'));

        return $form;
    }

    /**
     * Displays a form to create a new EmailTemplate entity.
     *
     */
    public function newAction()
    {
        $entity = new UserDoc();
        $form = $this->createCreateForm($entity);

        return $this->render('CasinoUserBundle:DocsAdmin:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a EmailTemplate entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CasinoUserBundle:UserDoc')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find UserDoc entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CasinoUserBundle:DocsAdmin:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to delete a EmailTemplate entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('docs_admin_delete', array('id' => $id)))
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm();
    }

    /**
     * Deletes a EmailTemplate entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if (count($form->getErrors(true)) == 0) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CasinoUserBundle:UserDoc')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find UserDoc entity.');
            } else {
                $em->remove($entity);
                $em->flush();
            }
        }

        return $this->redirect($this->generateUrl('docs_admin_index'));
    }

    /**
     * Deletes a EmailTemplate entity.
     *
     */
    public function checkAction(Request $request, $id, $status)
    {
        $id = intval($id);
        $status = strval($status);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CasinoUserBundle:UserDoc')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find user document. Check database integrity.');
        }

        $statuses = array_flip(array_values(UserDoc::$statuses));
        if (in_array($status, array_keys($statuses) )){

            $entity->setIsValidation($statuses[$status]);
            if ($statuses[$status] == UserDoc::USER_DOCUMENT_STATUS_APPROVED){
                $entity->getUser()->setIsDocsValidated(true);
            }
            $em->persist($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('docs_admin_index'));
    }

    /**
     * Creates a form to delete a EmailTemplate entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createStatusForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('docs_admin_check', array('id' => $id)))
            ->add('validation', 'choice', ['choices' => UserDoc::$statuses])
            ->add('submit', 'submit', array('label' => 'Validate'))
            ->getForm();
    }
}
