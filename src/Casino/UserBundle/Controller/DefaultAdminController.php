<?php

namespace Casino\UserBundle\Controller;

use APY\DataGridBundle\Grid\Action\RowAction;
use APY\DataGridBundle\Grid\Column\BooleanColumn;
use APY\DataGridBundle\Grid\Column\Column;
use APY\DataGridBundle\Grid\Column\JoinColumn;
use APY\DataGridBundle\Grid\Column\TextColumn;
use APY\DataGridBundle\Grid\Row;
use APY\DataGridBundle\Grid\Source\Entity;
use Casino\DefaultBundle\Controller\BaseAdminController;
use Casino\TreasuryBundle\Entity\Deposit;
use Casino\UserBundle\Entity\Group;
use Casino\UserBundle\Entity\User;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Acl\Exception\Exception;

class DefaultAdminController extends BaseAdminController
{
    public function indexAction(Request $request)
    {
        $source = new Entity('CasinoUserBundle:User');
        $grid = $this->get('grid');
        $grid->setSource($source);
        $grid->setId('id');
        $grid->setLimits([5, 10, 15]);

        $grid->hideColumns(
            [
                'usernameCanonical',
                'emailCanonical',
                'salt',
                'password',
                'confirmationToken',
                'passwordRequestedAt',
                'expired',
                'expiresAt',
                'credentialsExpired',
                'credentialsExpireAt',
                'create_date',
                'lastLogin',
                'enabled',
                'locked',
                'photo',
                'nickname',
                'tel',
                'gender',
                'social_data',
                'apiToken',
                'network_uid',
                'roles',
                'birth_day',
                'last_name',
                'first_name',
                'avatar',
                'address',
                'address2',
                'state',
                'city',
                'zip',
                'country',
            ]
        );

        $MyColumn2 = new BooleanColumn(
            ['id' => 'locked', 'sortable' => true, 'title' => 'Locked']
        );
        $MyColumn2->manipulateRenderCell(
            function ($value, $row, $router) {
                return $row->getField('locked') ? 'false' : 'true';
            }
        );
        $grid->addColumn($MyColumn2);

        $column = new JoinColumn(array('id' => 'full_name', 'title' => 'Full name', 'columns' => array('last_name', 'first_name')));

        $grid->addColumn($column);

        $userColumns = [
            'id',
            'username',
            'full_name',
            'email',
            'balance',
            'last_activity',
            'locale',
            'locked'
        ];
        $grid->setColumnsOrder($userColumns);

        $grid->getColumn('id')->setFilterable(true)->setOperatorsVisible(false);
        $grid->getColumn('username')->setFilterable(true)->setOperatorsVisible(true);
        $grid->getColumn('full_name')->setFilterable(true)->setFilterType('input')->setOperatorsVisible(true);
        $grid->getColumn('balance')->setFilterable(true)->setOperatorsVisible(true);
        $grid->getColumn('last_activity')->setFilterable(true)->setOperators(
            array(
                Column::OPERATOR_LTE,
                Column::OPERATOR_GT,
            )
        );
        $grid->getColumn('isDocsValidated')->setTitle('Docs approved?');
        $grid->getColumn('isDocsValidated')->manipulateRenderCell( function($value) {
            return ((boolean)$value)? 'yes': 'no';
        });

        $rowAction = new RowAction('Edit', 'casino_admin_users_edit');
        $rowAction->setRouteParameters(['username']);
        $grid->addRowAction($rowAction);

        $rowAction = new RowAction('Create deposit', 'casino_admin_users_deposit');
        $rowAction->setRouteParameters(['username']);
        $grid->addRowAction($rowAction);
        $rowAction = new RowAction('Give bonus', 'admin_bonus_give_mail');
        $rowAction->setRouteParameters(['email']);
        $grid->addRowAction($rowAction);

        if ($grid->isReadyForRedirect()) {
            return $grid->getGridResponse();
        }

        return $this->render(
            'CasinoUserBundle:DefaultAdmin:index.html.twig',
            ['grid' => $grid]
        );
    }

    public function editAction($username, Request $request)
    { 
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var User $user */
        $user = $userManager->findUserByUsername($username);
        if (!is_object($user)){
            throw new Exception(404,'User "'.$username.'"" not found!');
        }
        $em = $this->getDoctrine()->getManager();

        $form = $this->createFormBuilder($user)
            ->add('id','text', array(
                    'disabled' => true,
                    'read_only' => true,
                    //'attr' => array('class' => 'form-md-line-input')
                )
            )
            ->add('username', 'text', array(
                    'disabled' => true,
                    'read_only' => true,
                    //'attr' => array('class' => 'form-md-line-input')
                )
            )
            ->add('email','email', array(
                    'disabled' => true,
                    'read_only' => true,
                    //'attr' => array('class' => 'form-md-line-input')
                )
            )
            ->add('first_name', 'text', array(
                    //'attr' => array('class' => 'form-md-line-input')
                ))
            ->add('last_name', 'text', array(
                    //'attr' => array('class' => 'form-md-line-input')
                ))
            ->add('nickname','text', array(
                    //'attr' => array('class' => 'form-md-line-input')
                ))
            ->add('tel', 'integer', array(
                    //'attr' => array('class' => 'form-md-line-input'),
                    'label'  => 'editform.tel'
                ))
            ->add('gender', 'choice', array(
                    //'attr' => array('class' => 'form-md-line-input'),
                    'choices'  => array('-1' => 'Not chosen', '1' => 'Male', '0' => 'Female'),
                    'empty_data'  => '-1'
                ))
            ->add('locked', 'choice', array(
                    'choices'  => array('1' => 'Yes', '0' => 'No'),
                    'empty_data'  => '0'
                )
            )
            ->add('birth_day', 'birthday', array(
                    //'attr' => array('class' => 'form-md-line-input')
                ))
//            ->add('photo', 'text', array(
//
//                    //'attr' => array('class' => 'form-md-line-input')
//                ))
            ->add('ugroups', 'entity', [
                'class'=>'Casino\UserBundle\Entity\Group',
                'property'=>'name',
                'multiple'=>true,
                'expanded'=>true,
                'label' => 'User groups'

            ])
            ->add('save', 'submit', array(
                'attr' => array('class' => 'btn blue margin-t-20'),
                'label'  => 'editform.save'
            ))
            ->getForm();


        if ('POST' == $request->getMethod()) {
            $form->handleRequest($request);
            if ($form->isValid()) {

                if ($request->get('user_groups')) {
                    $groups = $em->getRepository('CasinoUserBundle:Group')
                        ->findBy(["id" => $request->get('user_groups')]);
                    $user->setUgroups($groups);
                }

                $userManager->updateUser($user);
              
                $request->getSession()->getFlashBag()->add(
                    'notice',
                    'Your changes were saved!'
                );
                return $this->redirect(
                    $this->generateUrl(
                        'casino_admin_users_edit',
                        ['username' => $username]
                    )
                );
            }
        }
        return $this->render(
            'CasinoUserBundle:DefaultAdmin:edit.html.twig',
            [
                'user' => $user,
                'form' => $form->createView()
            ]
        );
    }

    public function depositAction($username, $type = null , Request $request)
    {
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');

        /** @var User $user */
        $user = $userManager->findUserByUsername($username);
        $em = $this->getDoctrine()->getManager();

        //form choice field
        $typeOpts = ['label'=>'Wallet', 'choices'=> ['real'=>'$ wallet','bonus'=>'Bonus wallet']];
        if (in_array($type, ['real', 'bonus']))
            $typeOpts['data'] = $type;

        $form = $this->createFormBuilder()
            ->add('money', 'number', ['precision' => 2, 'label'=> 'Add funds' ] )
            ->add('type', 'choice', $typeOpts)
            ->add('add', 'submit',[])
            ->getForm();

        if ('POST' == $request->getMethod()) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $data = $form->getData();
                if ((float)$data['money'] <= 0) {
                    return $this->redirect(
                        $this->generateUrl(
                            'casino_admin_users_deposit',
                            ['username' => $username]
                        )
                    );
                }

                $isRealMoney = $form->get('type')->getData() == 'real';
                $deposit = new Deposit();
                $deposit->setUser($user);
                $deposit->setAmount((float)$data['money']);
                $deposit->setTo( ($isRealMoney)? Deposit::TO_REAL : Deposit::TO_BONUS);
                $deposit->setFrom(Deposit::FROM_ADMIN);
                $deposit->setStatus(Deposit::STATUS_SUCCESS);
                if ($isRealMoney) {
                    $balance = $user->getBalance();
                    $user->setBalance($user->getBalance() + (float)$data['money']);
                } else {
                    $balance = $user->getBonusBalance();
                    $user->setBonusBalance($balance + (float)$data['money']);
                }
                $deposit->setBalanceBefore($balance);
                $deposit->setBalanceAfter($balance+(float)$data['money']);
                $deposit->setDetailsJson(json_encode( array('from'=> 'admin', 'dest'=>'user')));
                $em->persist($deposit);
                $em->flush();

                $userManager->updateUser($user);

                $request->getSession()->getFlashBag()->add(
                    'notice',
                    'Your changes were saved!'
                );

                return $this->redirect(
                    $this->generateUrl(
                        'casino_admin_users_deposit',
                        ['username' => $username]
                    )
                );
            }
        }

        $source = new Entity('CasinoTreasuryBundle:Deposit');
        $tableAlias = $source->getTableAlias();

        $source->manipulateQuery(
            function ($query) use ($tableAlias, $user) {
                $query->where($tableAlias . '.user = ' . $user->getId());
            }
        );

        $grid = $this->get('grid');
        $grid->setSource($source);
        $grid->setId('id');
        $grid->setDefaultOrder('id', 'desc');
        $grid->setLimits([5, 10, 15]);

        if ($grid->isReadyForRedirect()) {
            return $grid->getGridResponse();
        }

        return $this->render(
            'CasinoUserBundle:DefaultAdmin:deposit.html.twig',
            [
                'user' => $user,
                'grid' => $grid,
                'form' => $form->createView()
            ]
        );
    }

    public function validateUsernameAction($username, Request $request) {
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->findUserByUsername($username);
        $userData = !is_null($user) ?
            ['status' => 'OK', 'email' => $user->getEmail(), 'last_activity' => $user->getLastActivity()] :
            ['status' => 'User not found'];
        return new JsonResponse(json_encode($userData));
    }

    public function sessionsAction(Request $request, $userid = null)
    {
        $user = null;
        if (!is_null($userid)) {
            $userid = intval($userid);
            /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
            $userManager = $this->get('fos_user.user_manager');
            /** @var User $user */
            $user = $userManager->findUserBy(['id' => $userid]);
            if (!is_object($user)) {
                throw new \Exception('User not found!');
            }
        }

        $source = new Entity('CasinoUserBundle:UserSession');
        $grid = $this->get('grid');

        $tableAlias = $source->getTableAlias();
        $source->manipulateQuery(
            function (QueryBuilder $query) use ($tableAlias, $userid) {
                $query->addSelect( 'user.username');
                $query->leftJoin( 'Casino\UserBundle\Entity\User', "user", "WITH", $tableAlias . ".user = user.id");
                $query->orderBy($tableAlias.'.loginDate','DESC');
                if ( !is_null($userid) ){
                    $query->andWhere('user.id ='.$userid);
                }
            }
        );
        $userName = new TextColumn(
            [
                'id'=>'username',
                'title'=>'User name',
                'field' => 'user.username',
                'isAggregate' => false,
                'source' => true,
                'filterable' => true,
                'sortable' => true,
            ]);

        $timeSpent = new TextColumn(
            [
                'id'=>'time_spent',
                'title'=>'Time spent',
                //'field' => 'user.username',
                'filterable' => false,
                'sortable' => false,
            ]);
        $timeSpent->manipulateRenderCell(
            function ($value, $row, $router) {
                /** @var \DateTime $startTime */
                $startTime = $row->getField('loginDate');
                /** @var \Datetime $endTime */
                $endTime = $row->getField('last_activity');
                $timeSpent = $startTime->diff($endTime);
                $d = ($timeSpent->h > 0)? $timeSpent->d.'d, ' : '';
                $h = ($timeSpent->h > 0)? $timeSpent->h.'h, ' : '';
                $i = ($timeSpent->i > 0)? $timeSpent->i.'m, ' : '';
                $s = ($timeSpent->s > 0)? $timeSpent->s.'s' : '';
                $totalSpent = $d.$h.$i.$s;
                return $totalSpent;
            }
        );

        if ( is_null($userid) ) {
            $userName = new TextColumn(
                [
                    'id'=>'username',
                    'title'=>'User name',
                    'field' => 'user.username',
                    // 'isManualField' => true,
                    'isAggregate' => false, // Defaults to false, set true if using aggregate func. like SUM()
                    'source' => true,
                    'filterable' => true,
                    'sortable' => true
                ]);
            $grid->addColumn($userName);
        }

        $grid->addColumn($timeSpent);

        $grid->setSource($source);
        $grid->setId('id');
        $grid->setLimits([15, 20, 50]);

        $grid->hideColumns(
            [
                'id',
                'last_activity',
            ]
        );

        $rowAction = new RowAction('Edit', 'casino_admin_users_edit');
        $rowAction->setRouteParameters(['username']);
        $rowAction->manipulateRender(function(RowAction $action,Row $row){
            if ($row->getEntity()->getUser() == null){
                return null;
            }
            return $action;
        });
        $grid->addRowAction($rowAction);
        if ($grid->isReadyForRedirect()) {
            return $grid->getGridResponse();
        }

        return $this->render(
            'CasinoUserBundle:DefaultAdmin:index.html.twig',
            ['grid' => $grid, 'user'=>$user]
        );
    }

    public function affilsAction($userid = null)
    {
        $user = null;
        if (!is_null($userid)) {
            $userid = intval($userid);
            /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
            $userManager = $this->get('fos_user.user_manager');
            /** @var User $user */
            $user = $userManager->findUserBy(['id' => $userid]);
            if (!is_object($user)) {
                throw new \Exception('User not found!');
            }
        }

        $source = new Entity('CasinoUserBundle:UserAffiliate');

        $tableAlias = $source->getTableAlias();
        $source->manipulateQuery(
            function (QueryBuilder $query) use ($tableAlias, $userid) {
                $query->leftJoin( 'Casino\UserBundle\Entity\User', "user", "WITH", $tableAlias . ".user = user.id");
                if ( !is_null($userid) ){
                    $query->andWhere('user.id ='.$userid);
                } else {
                    $query->addSelect( 'user.username');
                }
            }
        );

        $grid = $this->get('grid');
        $grid->setId('id');
        $grid->setSource($source);

        $grid->setLimits([5, 10, 15]);

        if ( is_null($userid) ) {
            $userName = new TextColumn(
                [
                    'id'=>'username',
                    'title'=>'User name',
                    'field' => 'user.username',
                    // 'isManualField' => true,
                    'isAggregate' => false, // Defaults to false, set true if using aggregate func. like SUM()
                    'source' => true,
                    'filterable' => true,
                    'sortable' => true
                ]);
            $grid->addColumn($userName);
        }

        if ($grid->isReadyForRedirect()) {
            return $grid->getGridResponse();
        }

        return $this->render(
            'CasinoTreasuryBundle:DepositAdmin:index.html.twig',
            ['grid' => $grid,'title'=>'Affiliate links', 'user'=>$user]
        );

    }
}
