<?php

namespace Casino\UserBundle\Controller;

use Casino\DefaultBundle\Controller\BaseAdminController;
use APY\DataGridBundle\Grid\Source\Entity;
use APY\DataGridBundle\Grid\Action\RowAction;
use APY\DataGridBundle\Grid\Column\BooleanColumn;
use Symfony\Component\HttpFoundation\Request;

class AdministratorsAdminController extends BaseAdminController
{
    public function indexAction()
    {
        $securityContext = $this->container->get('security.authorization_checker');
        if (!($securityContext->isGranted('ROLE_ADMIN') || $securityContext->isGranted('ROLE_SUPER_ADMIN') )) {
            return $this->redirect(
                $this->generateUrl(
                    'admin_login'
                )
            );
        }

        $source = new Entity('CasinoUserBundle:User');
        $grid = $this->get('grid');
        $grid->setSource($source);
        $grid->setId('id');
        $grid->setLimits([5, 10, 15]);

        $grid->hideColumns(
            [
                'usernameCanonical',
                'emailCanonical',
                'salt',
                'password',
                'confirmationToken',
                'passwordRequestedAt',
                'expired',
                'expiresAt',
                'credentialsExpired',
                'credentialsExpireAt',
                'create_date',
                'lastLogin',
                'enabled',
                'locked',
                'photo',
                'nickname',
                'tel',
                'gender',
                'social_data',
                'apiToken',
                'network_uid',
                'balance',
                'network',
                'avatar',
                'address',
                'address2',
                'state',
                'city',
                'zip',
                'country',
                'isDocsValidated',
            ]
        );

        $MyColumn2 = new BooleanColumn(
            ['id' => 'locked', 'sortable' => true, 'title' => 'Locked']
        );
        $MyColumn2->manipulateRenderCell(
            function ($value, $row, $router) {
                return $row->getField('locked') ? 'false' : 'true';
            }
        );
        $grid->addColumn($MyColumn2);

        $userColumns = [
            'id',
            'username',
            'email',
            'last_activity',
            'locale',
            'locked'
        ];
        $grid->setColumnsOrder($userColumns);

        $rowAction = new RowAction('Edit', 'casino_admin_administrators_edit');
        $rowAction->setRouteParameters(['username']);
        $grid->addRowAction($rowAction);

        /* $exporter = new ExcelExport('Excel Export');
         $exporter->setFileName('users_'.date('Y-m-d_H-i-s'));
         $grid->addExport($exporter);

         $exporter = new DSVExport('DSV Export');
         $exporter->setDelimiter(',');
         $exporter->setFileName('users_'.date('Y-m-d_H-i-s'));
         $exporter->setFileExtension('csv');
         $exporter->setMimeType('text/comma-separated-values');

         $grid->addExport($exporter);*/

        if ($grid->isReadyForRedirect()) {
            return $grid->getGridResponse();
        }

        return $this->render(
            'CasinoUserBundle:AdministratorsAdmin:index.html.twig',
            ['grid' => $grid]
        );
    }

    public function editAction($username, Request $request)
    {
        $securityContext = $this->container->get('security.authorization_checker');
        if (!($securityContext->isGranted('ROLE_ADMIN') || $securityContext->isGranted('ROLE_SUPER_ADMIN'))) {
            return $this->redirect(
                $this->generateUrl(
                    'admin_login'
                )
            );
        }

        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->findUserByUsername($username);
        $em = $this->getDoctrine()->getManager();

        $form = $this->createFormBuilder($user)
            ->add('email', 'text')
            ->add('locked', 'text')
            ->getForm();

        if ('POST' == $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                if ($request->get('user_password')) {
                    $user->setPlainPassword($request->get('user_password'));
                }
                if ($request->get('user_role')) {

                    if ($request->get('user_role') == 'ROLE_CONTENT_MANAGER') {
                        $user->setRoles(['ROLE_CONTENT_MANAGER']);
                    }

                    if ($request->get('user_role') == 'ROLE_ADMIN') {
                        $user->setRoles(['ROLE_ADMIN']);
                    }

                    if ($securityContext->isGranted('ROLE_SUPER_ADMIN') && $request->get('user_role') == 'ROLE_SUPER_ADMIN') {
                        $user->setRoles(['ROLE_SUPER_ADMIN']);
                    }
                }

                $userManager->updateUser($user);

                $request->getSession()->getFlashBag()->add(
                    'notice',
                    'Your changes were saved!'
                );

                return $this->redirect(
                    $this->generateUrl(
                        'casino_admin_administrators_edit',
                        ['username' => $username]
                    )
                );
            }
        }

        return $this->render(
            'CasinoUserBundle:AdministratorsAdmin:edit.html.twig',
            [
                'user' => $user,
                'isSuperAdmin' => $securityContext->isGranted('ROLE_SUPER_ADMIN'),
            ]
        );
    }

    public function createAction(Request $request)
    {
        $securityContext = $this->container->get('security.authorization_checker');
        if (!($securityContext->isGranted('ROLE_ADMIN') || $securityContext->isGranted('ROLE_SUPER_ADMIN'))) {
            return $this->redirect(
                $this->generateUrl(
                    'admin_login'
                )
            );
        }

        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        $user = new \Casino\UserBundle\Entity\User();
        $em = $this->getDoctrine()->getManager();

        $form = $this->createFormBuilder($user)
            ->add('username', 'text')
            ->add('email', 'text')
            ->getForm();

        if ('POST' == $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $user_test_email = $userManager->findUserBy(['email' => $user->getEmail()]);
                $user_test_username = $userManager->findUserBy(['username' => $user->getUsername()]);
                if ($user_test_email || $user_test_username) {
                    $request->getSession()->getFlashBag()->add(
                        'danger',
                        'Email or Username is already in use'
                    );

                    return $this->render('CasinoUserBundle:AdministratorsAdmin:create.html.twig');
                }

                if ($request->get('user_password')) {
                    $user->setPlainPassword($request->get('user_password'));
                }
                if ($request->get('user_role')) {

                    if ($request->get('user_role') == 'ROLE_CONTENT_MANAGER') {
                        $user->setRoles(['ROLE_CONTENT_MANAGER']);
                    }

                    if ($request->get('user_role') == 'ROLE_ADMIN') {
                        $user->setRoles(['ROLE_ADMIN']);
                    }

                    if ($securityContext->isGranted('ROLE_SUPER_ADMIN') && $request->get('user_role') == 'ROLE_SUPER_ADMIN') {
                        $user->setRoles(['ROLE_SUPER_ADMIN']);
                    }
                }
                $user->setEnabled(true);
                $userManager->updateUser($user);

                $request->getSession()->getFlashBag()->add(
                    'notice',
                    'Your changes were saved!'
                );

                return $this->redirect(
                    $this->generateUrl(
                        'casino_admin_administrators_edit',
                        ['username' => $user->getUsername()]
                    )
                );
            }
        }

        return $this->render('CasinoUserBundle:AdministratorsAdmin:create.html.twig');
    }
}
