<?php

namespace Casino\UserBundle\Controller;
//setlocale(LC_ALL, 'ru_RU.UTF-8');

use APY\DataGridBundle\Grid\Action\RowAction;
use APY\DataGridBundle\Grid\Source\Entity;
use Casino\UserBundle\Entity\User;
use Casino\UserBundle\Entity\UserDoc;
use Casino\UserBundle\Form\UserDocType;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class DocsController extends Controller
{


    public function indexAction(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if (!is_object($user)) {
            return $this->redirect(
                $this->generateUrl(
                    'fos_user_security_login'
                )
            );
        }

        $uid = $user->getId();
        $source = new Entity('CasinoUserBundle:UserDoc');
        $tableAlias = $source->getTableAlias();
        $source->manipulateQuery(
            function ($query) use ($tableAlias, $uid) {
                $query->andWhere( $tableAlias . '.user  = ' . $uid);
            }
        );

     //   $tableAlias = $source->getTableAlias();
        $grid = $this->get('grid');
        //dump($source);
        $grid->setSource($source);
        $grid->setId('id');
        $grid->setLimits([5, 10, 15]);
        $grid->hideColumns('id');

        $grid->getColumn('updatedAt')->setTitle('Status changed at');
        $grid->getColumn('doc')->setTitle('Document');
        $grid->getColumn('validation')->setTitle('Validation status');
        $grid->getColumn('validation')->manipulateRenderCell(
            function($value, $row) {
                return (in_array($value, array_keys(UserDoc::$statuses)))? UserDoc::$statuses[$value] : UserDoc::$statuses[UserDoc::USER_DOCUMENT_STATUS_NEW];
            }
        );

        $newDocEntity = new UserDoc();
        $form = $this->createCreateForm($newDocEntity);

        $rowAction = new RowAction('Delete', 'docs_delete');
        $rowAction->setRouteParameters(['id']);
        $rowAction->manipulateRender(
            function ($action, $row)
            {
                if ($row->getField('validation')) {
                    return false;
                }
            }
        );
        $grid->addRowAction($rowAction);

        if ($grid->isReadyForRedirect()) {
            return $grid->getGridResponse();
        }

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            /** @var QueryBuilder $qb */
            $qb = $em->createQueryBuilder()
                ->select('count(userDoc.id)')
                ->from('CasinoUserBundle:UserDoc','userDoc')
                ->where('userDoc.user = :user')
                ->andWhere('userDoc.doc = :doc')
                ->setParameter('user', $user->getId())
                ->setParameter('doc', $newDocEntity->getUserFile()->getClientOriginalName())
            ;
            $count = $qb->getQuery()->getSingleScalarResult();
            if ($count >=1 ){
                $form->get('doc')->addError(new FormError('You has already uploaded file with this name.( "'.$newDocEntity->getUserFile()->getClientOriginalName().'" )'));
            } else {
                $newDocEntity->setUser($user);
                $em->persist($newDocEntity);
                $em->flush();
                return $this->redirect($this->generateUrl('docs_index'));
            }
        }

        return $this->render(
            'CasinoUserBundle:Docs:index.html.twig',
            [
                'grid' => $grid,
                'form'   => $form->createView()
            ]
        );
    }

    /**
     * Creates a form to create a EmailTemplate entity.
     *
     * @param UserDoc $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(UserDoc $entity)
    {
        $form = $this->createForm(new UserDocType(), $entity, array(
            'action' => $this->generateUrl('docs_index'),
            'method' => 'POST',
        ));

        $form->add('upload', 'submit', array('label' => 'Choose file', 'attr' => ['class' => 'someclass']));
        $form->add('submit', 'submit', array('label' => 'Upload document'));

        return $form;
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, $id)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if (!is_object($user)) {
            return $this->redirect(
                $this->generateUrl(
                    'fos_user_security_login'
                )
            );
        }

        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if (count($form->getErrors(true)) == 0) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CasinoUserBundle:UserDoc')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find UserDoc entity.');
            } else {
                if ($entity->getUser()->getId() !== $user->getId()){
                    throw $this->createAccessDeniedException('Access denied. Its now your document.');
                }

                $em->remove($entity);
                $em->flush();
            }
        }

        return $this->redirect($this->generateUrl('docs_index'));
    }

    /**
     * Creates a form to delete a EmailTemplate entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('docs_delete', array('id' => $id)))
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm();
    }


    /**
     * @param Request $request
     * @param $hash
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function getAction(Request $request, $hash)
    {
        $hash = urldecode(strval($hash));
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if (!is_object($user)) {
            return $this->redirect(
                $this->generateUrl(
                    'fos_user_security_login'
                )
            );
        }

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CasinoUserBundle:UserDoc')->findOneBy(['doc'=>$hash]);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find UserDoc entity.');
        }

        $isAdminPerson = (true === $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) || (true === $this->get('security.authorization_checker')->isGranted('ROLE_CONTENT_MANAGER'));
        $fileBenogsToYou = $entity->getUser()->getId() === $user->getId();
        if ( !($fileBenogsToYou || $isAdminPerson)) {
            throw $this->createNotFoundException('Unable to find file.');
        }

        //$basePath = $this->container->getParameter('kernel.root_dir').'/../private/uploads/docs';
        $basePath = $this->get('kernel')->getRootDir().'/../private/uploads/docs/'.$entity->getUser()->getUsernameCanonical();
        $filePath = $basePath.'/'.$hash;

        $fs = new FileSystem();
        if (!$fs->exists($filePath)) {
            throw $this->createNotFoundException();
        }

        $response = new Response();
        $response->headers->set('Content-Type', mime_content_type($filePath));
        $d = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $hash,
            mb_convert_encoding($hash, "ASCII")
        );

        $response->headers->set('Content-Disposition', $d, false);
        $response->setPrivate();
      //  dump($response->headers->all());
        $response->sendHeaders();
        $response->setContent(readfile($filePath));
        return $response;
    }
}
