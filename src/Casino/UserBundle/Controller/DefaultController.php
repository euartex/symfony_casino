<?php

namespace Casino\UserBundle\Controller;

use APY\DataGridBundle\Grid\Source\Entity;
use Casino\UserBundle\Entity\User;
use Casino\UserBundle\Entity\UserSession;
use Casino\UserBundle\Form\Type\FileType;
use Doctrine\ORM\EntityManager;
use GeoIp2\Database\Reader;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Intl\Intl;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends Controller
{
    /*
     * @todo: Move from R.E.C.T.A.L. approach to CALLBACK approach
     */
    public function balanceAction()
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if (!is_object($user)) {
            throw new Exception('user not found');
        }
        $response = new JsonResponse();
        $response->setData(
            [
                'status'  => 'ok',
                'balance' => $user->getBalance(),
                'bonus' => $user->getBonusBalance()
            ]
        );

        return $response;
    }

    public function socialAction(Request $request)
    {
        $s = file_get_contents(
            'http://ulogin.ru/token.php?token=' . $request->get('token') . '&host=' . $_SERVER['HTTP_HOST']
        );
        $userData = json_decode($s, true);
        if (isset($userData['error'])) {
            throw new Exception($userData['error']);
            die();
        }

        $userManager = $this->get('fos_user.user_manager');
        //name = uid + network
        $username = $userData['uid'] . $userData['network'];
        //name = nick + network
        if (isset($userData['nickname']) && $userData['nickname']) {
            $username = $userData['nickname'] . $userData['network'];
        }
        //name = name + lastname
        if (isset($userData['first_name'])&& isset($userData['last_name'])){
            $username = $userData['first_name'].' '. $userData['last_name'];
        }

        if (isset($userData['email']) && $userData['email']) {
            $user = $userManager->findUserBy(['email' => $userData['email']]);
        } else {
            $user = $userManager->findUserBy(['username' => $username]);
        }

        if (null === $user) {
            /** @var User $user */
            $user = $userManager->createUser();
            $user->setUsername($username);
            if (isset($userData['email']) && $userData['email']) {
                $user->setEmail($userData['email']);
            }
            if (isset($userData['sex'])) {
                $user->setGender($userData['sex'] == '2' ? 1 : 0);
            }
            if (isset($userData['nickname']) && $userData['nickname']) {
                $user->setNickname($userData['nickname']);
            }
            if (isset($userData['first_name'])&& isset($userData['last_name'])){
                $user->setFirstName($userData['first_name']);
                $user->setLastName($userData['last_name']);
            }
            $user->setPassword(md5($username));
            $user->setEnabled(true);
            $user->setSocialData($userData);
            $user->setNetwork($userData['network']);
            $user->setNetworkUid($userData['uid']);
        } elseif (!$user->isAccountNonLocked() || !$user->isEnabled() || !$user->isAccountNonExpired() ) {
            $this->creasteSessionEntry($request);
            return $this->redirect(
                $this->get('router')->generate('casino_default_homepage_login')
            );
        }
        $userSession = $this->creasteSessionEntry($request, $user);
        $user->setCurrentSession($userSession);

        $user->setSocialData($userData);
        $user->setNetwork($userData['network']);
        $user->setNetworkUid($userData['uid']);
        if (!$user->getPhoto()) {
            if (isset($userData['photo_big']) && $userData['photo_big']) {
                $name = md5(microtime(true)) . "_" . rand(0, 10000) . '.jpg';
                copy($userData['photo_big'], WEB_DIRECTORY . '/uploads/users/' . $name);
                $user->setPhoto('/uploads/users/' . $name);
            }
        }
        $userManager->updateUser($user);

        $token = new UsernamePasswordToken(
            $user,
            $user->getPassword(),
            'main',
            $user->getRoles()
        );

        $this->get('security.context')->setToken($token);

        return $this->redirect(
            $this->generateUrl(
                'casino_default_homepage'
            )
        );
    }

    public function profileAction(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if (!is_object($user)) {
            return $this->redirect(
                $this->generateUrl(
                    'casino_default_homepage_login'
                )
            );
        }

        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');

        $form = $this->createFormBuilder($user)
            //->add('_token', 'hidden')
            ->add('nickname','text', array(
                    //'disabled' => true,
                    //'read_only' => true
                )
            )
            ->add('email','email', array(
                    'disabled' => true,
                    'read_only' => true
                )
            )
            ->add('first_name', 'text',
                [ 'attr' => [ 'placeholder' => 'First name (letters only)']]
            )
            ->add('last_name', 'text', ['attr' => [ 'placeholder' => 'Last name (letters only)'], 'label' => 'Last name', 'invalid_message'=> 'Letters only'])
            ->add('tel', 'integer', array( 'label'  => 'Phone', 'invalid_message'=>'Digits only (0-9)', 'attr' => [ 'placeholder' => ''] ))
            ->add('gender', 'choice', array(
                'choices'  => array('1' => 'Male', '0' => 'Female'),
                'placeholder' => 'Not chosen',
                'empty_data'  => '-1'
            ))
            ->add('birthday', 'date', array('widget' => 'single_text', 'html5'=>false, 'format'=> 'dd-MM-yyyy'))
            ->add('address', 'text', array( 'label'  => 'Address', 'attr' => [ 'placeholder' => 'Country, State, City, Street, apt'] ))
            ->add('address2', 'text', array( 'label'  => 'Address2', 'required' => false, 'attr' => [ 'placeholder' => 'Optional address'] ))
            ->add('state', 'text', array( 'label'  => 'State', 'required' => false, 'attr' => [ 'placeholder' => 'State'] ))
            ->add('city', 'text', array( 'label'  => 'City', 'required' => false, 'attr' => [ 'placeholder' => 'City'] ))
            ->add('zip', 'text', array( 'label'  => 'Zip', 'required' => false, 'attr' => [ 'placeholder' => 'Zip / Postal Code'] ))
            ->add(
                'country',
                'choice',
                array(
                    'label'  => 'Country',
                    'required' => false,
                    'attr' => [
                        'placeholder' => 'Country',
                        'class' => 'profile-country-select',
                    ],
                    'choices' => array_merge([ '' => 'profile.select_country' ], Intl::getRegionBundle()->getCountryNames()),
                )
            )
            ->add('avatar', 'hidden', array('required'=>false ) )
            ->add('userFile', 'vich_image', array(
                    'label' => null,
                    'label_attr' => ['class' => 'hidden'],
                    'required'      => false,
                    'allow_delete'  => false, // not mandatory, default is true
                    'download_link' => false, // not mandatory, default is true
                'attr'=> ['class' => 'row-wrap input-wrap hidden-file'],
            ))
            ->add('update', 'submit', array( 'label'  => 'Save settings', 'attr' => [ 'class' => 'profile__item
            button_decoration'] ))
            ->getForm();

        if ('POST' == $request->getMethod()) {
            $form->handleRequest($request);
            if ($form->isValid()) {

                $uploadedFile = $request->files->get('form')['userFile']['file'];
                if ($uploadedFile instanceof UploadedFile) {
                    $user->setAvatar('');
                }

                $userManager->updateUser($user);

                $request->getSession()->getFlashBag()->add(
                    'notice',
                    'Your changes were saved!'
                );
            }
        }

        return $this->render(
            'CasinoUserBundle:Default:profile.html.twig',
            [
                'user' => $user,
                'form' => $form->createView()
            ]
        );
    }

    private function creasteSessionEntry(Request $request, User $user = null)
    {
        /** @var EntityManager $em */
        $em = $this->get('doctrine')->getManager();
        $now = new \DateTime();
        $userSession = new UserSession();
        if ($user !== null){
            $userSession->setUser($user);
        } else {
            $userSession->setIsLoginSuccessful(false);
        }
        $userSession->setIsLoggedBySocial(true);
        $userSession->setLastActivity($now);
        $userSession->setLoginDate($now);
        $userSession->setUserAgent($request->headers->get('User-Agent'));
        $userSession->setIp($request->getClientIp());

        $baseDir = $this->get('kernel')->getRootDir() . '/../';
        $reader = new Reader($baseDir . $this->container->getParameter('geoip_db_path'));
        $location = $reader->city($request->getClientIp());
        $country = ($location->country->isoCode)? $location->country->name . ' [' . $location->country->isoCode . ']' : '';
        $city = $location->city->name;
        $userSession->setCity($city);
        $userSession->setCountry($country);
        $em->persist($userSession);
        $em->flush($userSession);
        return $userSession;
    }

    /**
     * Retrieve avatar in a way that it doesn't get cached by nginx
     * @param string $file
     * @return Response
     */
    public function avatarAction($file)
    {
        $path = sprintf(
            '%s/%s/%s',
            $this->container->get('kernel')->getRootDir(),
            $this->container->getParameter('avatar_path'),
            $file
        );
        $response = new Response();
        $response->setContent(file_get_contents($path));
        $response->setMaxAge(0);
        $response->headers->addCacheControlDirective('no-store');
        $response->headers->add(['Content-Type' => mime_content_type($path)]);

        return $response;
    }
}
