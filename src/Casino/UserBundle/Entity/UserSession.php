<?php

namespace Casino\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="user_session")
 */
class UserSession
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @ORM\Column(type="string", length=15) */
    protected $ip = null;

    /** @ORM\Column(type="string", length=255) */
    protected $userAgent;

    /** @ORM\Column(type="string", length=255, nullable=true) */
    protected $country = null;

    /** @ORM\Column(type="string", length=255, nullable=true) */
    protected $city = null;

    /** @ORM\Column(type="boolean") */
    protected $isLoginSuccessful = true;

    /** @ORM\Column(type="boolean") */
    protected $isLoggedBySocial = false;

    /** @ORM\Column(name="login_date", type="datetime") */
    protected $loginDate;

    /** @ORM\Column(name="last_activity", type="datetime") */
    protected $last_activity;

    /**
     * @ORM\ManyToOne(targetEntity="Casino\UserBundle\Entity\User", cascade={"persist"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    public function __construct()
    {
        $this->last_activity = new \DateTime("now");
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ip
     *
     * @param string $ip
     *
     * @return UserSession
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set userAgent
     *
     * @param string $userAgent
     *
     * @return UserSession
     */
    public function setUserAgent($userAgent)
    {
        $this->userAgent = $userAgent;

        return $this;
    }

    /**
     * Get userAgent
     *
     * @return string
     */
    public function getUserAgent()
    {
        return $this->userAgent;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return UserSession
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return UserSession
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set loginDate
     *
     * @param \DateTime $loginDate
     *
     * @return UserSession
     */
    public function setLoginDate($loginDate)
    {
        $this->loginDate = $loginDate;

        return $this;
    }

    /**
     * Get loginDate
     *
     * @return \DateTime
     */
    public function getLoginDate()
    {
        return $this->loginDate;
    }

    /**
     * Set lastActivity
     *
     * @param \DateTime $lastActivity
     *
     * @return UserSession
     */
    public function setLastActivity($lastActivity)
    {
        $this->last_activity = $lastActivity;

        return $this;
    }

    /**
     * Get lastActivity
     *
     * @return \DateTime
     */
    public function getLastActivity()
    {
        return $this->last_activity;
    }

    /**
     * Set user
     *
     * @param \Casino\UserBundle\Entity\User $user
     *
     * @return UserSession
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Casino\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set isLoginSuccessful
     *
     * @param boolean $isLoginSuccessful
     *
     * @return UserSession
     */
    public function setIsLoginSuccessful($isLoginSuccessful)
    {
        $this->isLoginSuccessful = $isLoginSuccessful;

        return $this;
    }

    /**
     * Get isLoginSuccessful
     *
     * @return boolean
     */
    public function getIsLoginSuccessful()
    {
        return $this->isLoginSuccessful;
    }

    /**
     * Set isLoggedBySocial
     *
     * @param boolean $isLoggedBySocial
     *
     * @return UserSession
     */
    public function setIsLoggedBySocial($isLoggedBySocial)
    {
        $this->isLoggedBySocial = $isLoggedBySocial;

        return $this;
    }

    /**
     * Get isLoggedBySocial
     *
     * @return boolean
     */
    public function getIsLoggedBySocial()
    {
        return $this->isLoggedBySocial;
    }
}
