<?php

namespace Casino\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity
 * @ORM\Table(name="user_docs")
 * @Vich\Uploadable
 */
class UserDoc
{
    const USER_DOCUMENT_STATUS_NEW = 0;
    const USER_DOCUMENT_STATUS_APPROVED = 1;
    const USER_DOCUMENT_STATUS_DECLINED = 2;

    public static $statuses = [
        self::USER_DOCUMENT_STATUS_NEW => 'New',
        self::USER_DOCUMENT_STATUS_APPROVED => 'Approved',
        self::USER_DOCUMENT_STATUS_DECLINED => 'Declined',
    ];

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="user_doc", fileNameProperty="doc")
     *
     * @var File $userFile
     * @Assert\File(
     *     maxSize="4M",
     * )
     */
    private $userFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $doc;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $validation = self::USER_DOCUMENT_STATUS_NEW;

    /**
     * @var \Casino\UserBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Casino\UserBundle\Entity\User", inversedBy="userDocs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $user;


    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $file
     */
    public function setUserFile(File $file = null)
    {
        $this->userFile = $file;

        if ($file) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getUserFile()
    {
        return $this->userFile;
    }

    /**
     * @param string $doc
     */
    public function setDoc($doc)
    {
        $this->doc = $doc;
    }

    /**
     * @return string
     */
    public function getDoc()
    {
        return $this->doc;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @param User $user
     * @return UserDoc
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param int $validation
     * @return UserDoc
     */
    public function setIsValidation($validation)
    {
        $this->validation = $validation;
        return $this;
    }

    /**
     * @return int
     */
    public function getValidation()
    {
        return $this->validation;
    }

}
