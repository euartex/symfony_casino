<?php

namespace Casino\UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 * @Vich\Uploadable
 */
class User extends BaseUser
{
    const GENDER_MALE = 1;
    const GENDER_FEMALE = 2;
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @ORM\Column(name="photo", type="string", length=255, nullable=true) */
    protected $photo;

    /** @ORM\Column(name="nickname", type="string", length=255, nullable=true) */
    protected $nickname;

    /** @ORM\Column(name="tel", type="integer", length=15, nullable=true)
     */
    protected $tel;

    /** @ORM\Column(type="integer", length=1) */
    protected $gender = -1;

    /** @ORM\Column(type="decimal", scale=2) */
    protected $balance = 0;

    /** @ORM\Column(name="create_date",type="datetime") */
    protected $create_date;

    /** @ORM\Column(name="last_activity",type="datetime") */
    protected $last_activity;

    /** @ORM\Column(name="network", type="string", length=255, nullable=true) */
    protected $network;

    /** @ORM\Column(name="network_uid", type="string", length=255, nullable=true) */
    protected $network_uid;

    /** @ORM\Column(name="social_data", type="array", nullable=true) */
    protected $social_data;

    /**
     * @ORM\OneToMany(targetEntity="Casino\TreasuryBundle\Entity\Deposit", mappedBy="user")
     */
    protected $deposits;

    /**
     * @ORM\OneToMany(targetEntity="Casino\GameBundle\Entity\GameLog", mappedBy="user")
     */
    protected $gameLogs;

    /**
     * @ORM\OneToOne(targetEntity="Casino\TreasuryBundle\Entity\TreasuryDepositBonuses")
     * @ORM\JoinColumn(name="current_bonus", referencedColumnName="id", nullable=true)
     **/
    protected $currentBonus;

    /**
     * @ORM\OneToOne(targetEntity="Casino\UserBundle\Entity\UserSession")
     * @ORM\JoinColumn(name="current_session", referencedColumnName="id", nullable=true)
     **/
    protected $currentSession;

    /** @ORM\Column(type="decimal", scale=2) */
    protected $bonusBalance = 0;

    /** @ORM\Column(name="api_token", type="string", length=32, nullable=true) */
    protected $apiToken;

    /**
     * @ORM\ManyToMany(targetEntity="Casino\UserBundle\Entity\Group", inversedBy="users")
     * @ORM\JoinTable(name="users_groups")
     **/
    protected $ugroups;

    /**
     * @ORM\Column(name="first_name", type="string", length=255, nullable=true)
     * @Assert\Regex(
     *     pattern     = "/^[a-z]+$/i",
     *     htmlPattern = "^[a-zA-Z]+$",
     *     message     = "Name can only contain alphabetic letters"
     * )
     */
    protected $first_name;

    /**
     * @ORM\Column(name="last_name", type="string", length=255, nullable=true)
     * @Assert\Regex(
     *     pattern     = "/^[a-z]+$/i",
     *     htmlPattern = "^[a-zA-Z]+$",
     *     message     = "Last name can only contain alphabetic letters"
     * )
     **/
    protected $last_name;

    /**
     * @ORM\Column(name="birth_day", type="date", nullable=true)
     */
    protected $birth_day;

    /**
     * Avialable bonuses for current user
     *
     * @ORM\ManyToMany(targetEntity="Casino\BonusBundle\Entity\Bonuses")
     * @ORM\JoinTable(name="users_bonuses",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="bonus_id", referencedColumnName="id")}
     *      )
     **/
    private $avBonuses;

//    /**
//     * @var string
//     *
//     * @ORM\Column(name="docs", type="json_array", nullable=true)
//     */
//    private $docs;

    /**
     * @ORM\OneToMany(targetEntity="Casino\UserBundle\Entity\UserDoc", mappedBy="user")
     */
    protected $userDocs;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="user_avatar", fileNameProperty="avatar")
     *
     * @var File $userFile
     * @Assert\Image(
     *     minHeight = 100,
     *     minWidth = 100,
     *     allowLandscape = true,
     *     allowPortrait = true,
     *     maxSize="400k",
     * )
     */
    private $userFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $avatar;

    /**
     * @ORM\Column(name="address", type="string", length=255, nullable=false)
     **/
    protected $address = '';

    /**
     * @ORM\Column(name="address2", type="string", length=255, nullable=true)
     **/
    protected $address2 = '';

    /**
     * @ORM\Column(name="state", type="string", length=100, nullable=true)
     **/
    protected $state = '';

    /**
     * @ORM\Column(name="city", type="string", length=100, nullable=true)
     **/
    protected $city = '';

    /**
     * @ORM\Column(name="zip", type="string", length=10, nullable=true)
     **/
    protected $zip = '';

    /**
     * @ORM\Column(name="country", type="string", length=2, nullable=true)
     **/
    protected $country = '';

    /**
     * @ORM\Column(name="docs_validated", type="boolean")
     **/
    protected $isDocsValidated = false;

    /**
     * @ORM\OneToMany(targetEntity="Casino\UserBundle\Entity\UserAffiliate", mappedBy="user")
     */
    protected $userAffiliates;

    /**
     * @ORM\ManyToOne(targetEntity="Casino\UserBundle\Entity\UserAffiliate")
     * @ORM\JoinColumn(name="affiliated_by", referencedColumnName="id")
     */
    protected $affiliatedBy;

    public function __construct()
    {
        parent::__construct();
        $this->ugroups = new \Doctrine\Common\Collections\ArrayCollection();
        $this->avBonuses = new \Doctrine\Common\Collections\ArrayCollection();

        $this->create_date = new \DateTime("now");
        $this->last_activity = new \DateTime("now");
    }

    public function setUsername($username)
    {
        if (!$this->nickname) {
            $this->nickname = $username;
        }
        return parent::setUsername($username);
    }

    public function getCreateDate()
    {
        return $this->create_date;
    }

    public function getLastActivity()
    {
        return $this->last_activity;
    }

    public function setLastActivity($value)
    {
        $this->last_activity = $value;
        return $this;
    }

    public function getPhoto()
    {
        return $this->photo;
    }

    public function setPhoto($value)
    {
        $this->photo = $value;
        return $this;
    }

    public function getNickname()
    {
        return $this->nickname;
    }

    public function setNickname($value)
    {
        $this->nickname = $value;
        return $this;
    }

    public function getTel()
    {
        return $this->tel;
    }

    public function setTel($value)
    {
        $this->tel = $value;
        return $this;
    }

    public function getGender()
    {
        return $this->gender;
    }

    public function setGender($value)
    {
        $this->gender = $value;
        return $this;
    }

    public function setNetwork($val)
    {
        $this->network = $val;
    }

    public function setNetworkUid($val)
    {
        $this->network_uid = $val;
    }

    public function setSocialData($value)
    {
        $this->social_data = $value;
        return $this;
    }

    public function getSocialData()
    {
        return $this->social_data;
    }

    public function getBalance(){
        return $this->balance;
    }

    public function setBalance($balance)
    {
        $this->balance = $balance;

        return $this;
    }

    public function getBonusBalance(){
        return $this->bonusBalance;
    }

    public function setBonusBalance($bonusBalance)
    {
        $this->bonusBalance = $bonusBalance;

        return $this;
    }

    public function setApiToken($token)
    {
        $this->apiToken = $token;

        return $this;
    }

    public function getApiToken()
    {
        return $this->apiToken;
    }

    public function getUgroups()
    {
        return $this->ugroups;
    }

    public function setUgroups($ugroups)
    {
        $this->ugroups = $ugroups;
    }

    public function getFirstName()
    {
        return $this->first_name;
    }

    public function setFirstName($value)
    {
        $this->first_name = $value;
        return $this;
    }

    public function getLastName()
    {
        return $this->last_name;
    }

    public function setLastName($value)
    {
        $this->last_name = $value;
        return $this;
    }

    public function getBirthDay()
    {
        return $this->birth_day;
    }

    public function setBirthDay($value)
    {
        $this->birth_day = $value;
        return $this;
    }

    public function getAge() {
        $birth = $this->getBirthDay();
        list($years, $months, $days) = explode("-", $birth->format('Y-m-d'));
        $year_diff = date("Y") - $years;
        $month_diff = date("m") - $months;
        $day_diff = date("d") - $days;
        if ($day_diff < 0 || $month_diff < 0)
            $year_diff--;
        return $year_diff;
    }

    /**
     * Set current users bonus
     *
     * @param \Casino\TreasuryBundle\Entity\TreasuryDepositBonuses $currentBonus
     * @return User
     */
    public function setCurrentBonus(\Casino\TreasuryBundle\Entity\TreasuryDepositBonuses $currentBonus = null)
    {
        $this->currentBonus = $currentBonus;

        return $this;
    }

    /**
     * Get current users bonus
     *
     * @return \Casino\TreasuryBundle\Entity\TreasuryDepositBonuses
     */
    public function getCurrentBonus()
    {
        return $this->currentBonus;
    }

    public function getAvBonuses()
    {
        return $this->avBonuses;
    }

    public function setAvBonuses($avBonuses)
    {
        $this->avBonuses = $avBonuses;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     *
     * @return User
     */
    public function setCreateDate($createDate)
    {
        $this->create_date = $createDate;

        return $this;
    }

    /**
     * Get network
     *
     * @return string
     */
    public function getNetwork()
    {
        return $this->network;
    }

    /**
     * Get networkUid
     *
     * @return string
     */
    public function getNetworkUid()
    {
        return $this->network_uid;
    }

    /**
     * Add deposit
     *
     * @param \Casino\TreasuryBundle\Entity\Deposit $deposit
     *
     * @return User
     */
    public function addDeposit(\Casino\TreasuryBundle\Entity\Deposit $deposit)
    {
        $this->deposits[] = $deposit;

        return $this;
    }

    /**
     * Remove deposit
     *
     * @param \Casino\TreasuryBundle\Entity\Deposit $deposit
     */
    public function removeDeposit(\Casino\TreasuryBundle\Entity\Deposit $deposit)
    {
        $this->deposits->removeElement($deposit);
    }

    /**
     * Get deposits
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDeposits()
    {
        return $this->deposits;
    }

    /**
     * Add gameLog
     *
     * @param \Casino\GameBundle\Entity\GameLog $gameLog
     *
     * @return User
     */
    public function addGameLog(\Casino\GameBundle\Entity\GameLog $gameLog)
    {
        $this->gameLogs[] = $gameLog;

        return $this;
    }

    /**
     * Remove gameLog
     *
     * @param \Casino\GameBundle\Entity\GameLog $gameLog
     */
    public function removeGameLog(\Casino\GameBundle\Entity\GameLog $gameLog)
    {
        $this->gameLogs->removeElement($gameLog);
    }

    /**
     * Get gameLogs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGameLogs()
    {
        return $this->gameLogs;
    }

    /**
     * Add ugroup
     *
     * @param \Casino\UserBundle\Entity\Group $ugroup
     *
     * @return User
     */
    public function addUgroup(\Casino\UserBundle\Entity\Group $ugroup)
    {
        $this->ugroups[] = $ugroup;

        return $this;
    }

    /**
     * Remove ugroup
     *
     * @param \Casino\UserBundle\Entity\Group $ugroup
     */
    public function removeUgroup(\Casino\UserBundle\Entity\Group $ugroup)
    {
        $this->ugroups->removeElement($ugroup);
    }

    /**
     * Add avBonus
     *
     * @param \Casino\BonusBundle\Entity\Bonuses $avBonus
     *
     * @return User
     */
    public function addAvBonus(\Casino\BonusBundle\Entity\Bonuses $avBonus)
    {
        $this->avBonuses[] = $avBonus;

        return $this;
    }

    /**
     * Remove avBonus
     *
     * @param \Casino\BonusBundle\Entity\Bonuses $avBonus
     */
    public function removeAvBonus(\Casino\BonusBundle\Entity\Bonuses $avBonus)
    {
        $this->avBonuses->removeElement($avBonus);
    }

    /**
     * Add userAffiliate
     *
     * @param \Casino\UserBundle\Entity\UserAffiliate $userAffiliate
     *
     * @return User
     */
    public function addUserAffiliate(\Casino\UserBundle\Entity\UserAffiliate $userAffiliate)
    {
        $this->userAffiliates[] = $userAffiliate;

        return $this;
    }

    /**
     * Remove userAffiliate
     *
     * @param \Casino\UserBundle\Entity\UserAffiliate $userAffiliate
     */
    public function removeUserAffiliate(\Casino\UserBundle\Entity\UserAffiliate $userAffiliate)
    {
        $this->userAffiliates->removeElement($userAffiliate);
    }

    /**
     * Get userAffiliates
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserAffiliates()
    {
        return $this->userAffiliates;
    }

    /**
     * Set affiliatedBy
     *
     * @param \Casino\UserBundle\Entity\UserAffiliate $affiliatedBy
     *
     * @return User
     */
    public function setAffiliatedBy(\Casino\UserBundle\Entity\UserAffiliate $affiliatedBy = null)
    {
        $this->affiliatedBy = $affiliatedBy;

        return $this;
    }

    /**
     * Get affiliatedBy
     *
     * @return \Casino\UserBundle\Entity\UserAffiliate
     */
    public function getAffiliatedBy()
    {
        return $this->affiliatedBy;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return User
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set address2
     *
     * @param string $address
     *
     * @return User
     */
    public function setAddress2($address)
    {
        $this->address2 = $address;

        return $this;
    }

    /**
     * Get address2
     *
     * @return string
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     * @return User
     */
    public function setState($state)
    {
        $this->state = $state;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     * @return User
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param mixed $zip
     * @return User
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     * @return User
     */
    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $file
     */
    public function setUserFile(File $file = null)
    {
        $this->userFile = $file;

        if ($file) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getUserFile()
    {
        return $this->userFile;
    }

    /**
     * @param string $avatar
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
    }

    /**
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param mixed $isDocsValidated
     * @return User
     */
    public function setIsDocsValidated($isDocsValidated)
    {
        $this->isDocsValidated = $isDocsValidated;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsDocsValidated()
    {
        return $this->isDocsValidated;
    }
//
//    /**
//     * Set docs
//     *
//     * @param array $docs
//     * @return User
//     */
//    public function setDocs($docs)
//    {
//        $this->docs = json_encode(array_unique($docs));
//        return $this;
//    }
//
//    /**
//     * Get vars
//     * @return array
//     */
//    public function getDocs()
//    {
//        return json_decode($this->docs, true);
//    }



    /**
     * Add doc
     *
     * @param \Casino\UserBundle\Entity\UserDoc $doc
     *
     * @return User
     */
    public function addDoc(\Casino\UserBundle\Entity\UserDoc $doc)
    {
        $this->userDocs[] = $doc;

        return $this;
    }

    /**
     * Remove doc
     *
     * @param \Casino\UserBundle\Entity\UserDoc $doc
     */
    public function removeUserDocs(\Casino\UserBundle\Entity\UserDoc $doc)
    {
        $this->userDocs->removeElement($doc);
    }

    /**
     * Get docs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserDocs()
    {
        return $this->userDocs;
    }

    /**
     * Add userDoc
     *
     * @param \Casino\UserBundle\Entity\UserDoc $userDoc
     *
     * @return User
     */
    public function addUserDoc(\Casino\UserBundle\Entity\UserDoc $userDoc)
    {
        $this->userDocs[] = $userDoc;

        return $this;
    }

    /**
     * Remove userDoc
     *
     * @param \Casino\UserBundle\Entity\UserDoc $userDoc
     */
    public function removeUserDoc(\Casino\UserBundle\Entity\UserDoc $userDoc)
    {
        $this->userDocs->removeElement($userDoc);
    }

    /**
     * Set currentSession
     *
     * @param \Casino\UserBundle\Entity\UserSession $currentSession
     *
     * @return User
     */
    public function setCurrentSession(\Casino\UserBundle\Entity\UserSession $currentSession = null)
    {
        $this->currentSession = $currentSession;

        return $this;
    }

    /**
     * Get currentSession
     *
     * @return \Casino\UserBundle\Entity\UserSession
     */
    public function getCurrentSession()
    {
        return $this->currentSession;
    }

    /**
     * @param string $name
     *
     * @return boolean
     */
    public function hasUGroup($name)
    {
        return in_array($name, $this->getUGroupNames());
    }

    public function getUGroupNames()
    {
        $names = array();
        foreach ($this->getUgroups() as $uGroup) {
            $names[] = $uGroup->getName();
        }

        return $names;
    }
}
