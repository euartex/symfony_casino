<?php

namespace Casino\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="user_group")
 */
class Group
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @ORM\Column(type="string", length=255) */
    protected $name;

    /**
     * @ORM\ManyToMany(targetEntity="Casino\UserBundle\Entity\User", mappedBy="ugroups")
     **/
    protected $users;

    public function __construct()
    {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }
    
    public function getUsers()
    {
        return $this->users;
    }
    
    public function setUsers($users)
    {
        $this->users = $users;
    }

    /**
     * Add user
     *
     * @param \Casino\UserBundle\Entity\User $user
     *
     * @return Group
     */
    public function addUser(\Casino\UserBundle\Entity\User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \Casino\UserBundle\Entity\User $user
     */
    public function removeUser(\Casino\UserBundle\Entity\User $user)
    {
        $this->users->removeElement($user);
    }
}
