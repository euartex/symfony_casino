<?php

namespace Casino\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * User addresses
 *
 * @ORM\Table(name="user_affiliate")
 * @ORM\Entity
 */
class UserAffiliate
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    /**
     * @var \Casino\UserBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Casino\UserBundle\Entity\User", inversedBy="userAffiliates")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="promo_name", type="string", length=255, nullable=false)
     */
    private $promoName;

    /**
     * @var string
     *
     * @ORM\Column(name="hash", type="string", length=255, nullable=false)
     */
    private $hash;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false)
     */
    private $isActive = true;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set promoName
     *
     * @param string $promoName
     *
     * @return UserAffiliate
     */
    public function setPromoName($promoName)
    {
        $this->promoName = $promoName;

        return $this;
    }

    /**
     * Get promoName
     *
     * @return string
     */
    public function getPromoName()
    {
        return $this->promoName;
    }

    /**
     * Set hash
     *
     * @param string $hash
     *
     * @return UserAffiliate
     */
    public function setHash($hash)
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * Get hash
     *
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * Set user
     *
     * @param \Casino\UserBundle\Entity\User $user
     *
     * @return UserAffiliate
     */
    public function setUser(\Casino\UserBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Casino\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return UserAffiliate
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }
}
