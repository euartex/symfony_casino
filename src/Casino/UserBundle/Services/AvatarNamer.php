<?php
/**
 * Created by PhpStorm.
 * User: Share
 * Date: 025 25.08.2015
 * Time: 15:53
 */

namespace Casino\UserBundle\Services;


use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\DirectoryNamerInterface;
use Vich\UploaderBundle\Naming\NamerInterface;

class AvatarNamer implements NamerInterface, DirectoryNamerInterface
{
    private $namerSalt = 'egpMjrX9IUo#Dw$ByA6j&oM0';

    /**
     * @var TokenStorage
     */
    protected $tokenStorage;
    private $documentRoot;

    /**
     * @param \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage $tokenStorage
     * @param $rootDir string App document root  (app folder)
     */
    public function __construct(TokenStorage $tokenStorage, $rootDir)
    {
        $this->tokenStorage = $tokenStorage;
        $this->documentRoot = $rootDir;
    }

    public function getUser()
    {
        return $this->tokenStorage->getToken()->getUser();
    }


    /**
     * Creates a name for the file being uploaded. Uses username + this class salt to generate name
     *
     * @param object $object The object the upload is attached to.
     * @param PropertyMapping $mapping The mapping to use to manipulate the given object.
     *
     * @return string The file name.
     */
    public function name($object, PropertyMapping $mapping)
    {
        $file = $object->getUserFile();
        $extension = $file->guessExtension();
        $userName = $this->getUser()->getUserName();

        return md5($userName . $this->namerSalt) . '.' . $extension;
    }

    public function directoryName($object, PropertyMapping $mapping)
    {
        $dir = $this->documentRoot . '/../private/uploads/docs/' . $this->getUser()->getUsernameCanonical();
        return $dir;
    }
}
