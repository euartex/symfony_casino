<?php

namespace Casino\UserBundle\Listener;

use Vich\UploaderBundle\Event\Event;

class UploadedFileListener
{
    public function onPostUpload(Event $event)
    {
        $file = $event->getObject()->getUserFile()->getPathName();
        if (in_array(strtolower(substr($file, -4)), ['.png', '.jpg', 'jpeg', '.gif'])) {
            $image = new \Imagick($file);
            $dimensions = $image->getImageGeometry();
            if ($dimensions['width'] > $dimensions['height']) {
                $size = $dimensions['height'];
                $xOffset = floor(($dimensions['width'] - $dimensions['height']) / 2);
                $image->cropImage($size, $size, $xOffset, 0);
            } else {
                $size = $dimensions['width'];
                $image->cropImage($size, $size, 0, 0);
            }
            $image->writeImage($file);
        }
    }
}
