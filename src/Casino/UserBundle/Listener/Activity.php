<?php
namespace Casino\UserBundle\Listener;

use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use DateTime;
use Casino\UserBundle\Entity\User;

class Activity
{
    protected $context;
    protected $em;

    public function __construct(SecurityContext $context, \Doctrine\ORM\EntityManager $em)
    {
        $this->context = $context;
        $this->em = $em;
    }

    /**
     * On each request we want to update the user's last activity datetime
     *
     * @param \Symfony\Component\HttpKernel\Event\FilterControllerEvent $event
     * @return void
     */
    public function onCoreController(FilterControllerEvent $event)
    {
        if(!$this->context->getToken()){
            return;
        }
        $user = $this->context->getToken()->getUser();
        if($user instanceof User)
        {
            //here we can update the user as necessary
            $user->setLastActivity(new DateTime());

            $session = $user->getCurrentSession();
            if (!is_null($session)){
                $user->getCurrentSession()->setLastActivity($user->getLastActivity());
                $this->em->persist($session);
                $this->em->flush($session);
            }
            $this->em->persist($user);
            $this->em->flush($user);
        }
    }
}