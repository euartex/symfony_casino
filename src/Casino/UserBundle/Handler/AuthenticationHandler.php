<?php

namespace Casino\UserBundle\Handler;

use Casino\UserBundle\Entity\User;
use Casino\UserBundle\Entity\UserSession;
use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Model\UserManager;
use GeoIp2\Database\Reader;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Validator\Constraints\DateTime;

class AuthenticationHandler implements AuthenticationSuccessHandlerInterface, AuthenticationFailureHandlerInterface
{
    protected $router;
    protected $security;
    protected $userManager;
    protected $service_container;

    public function __construct(RouterInterface $router, SecurityContext $security,UserManager  $userManager, $service_container)
    {
        $this->router = $router;
        $this->security = $security;
        $this->userManager = $userManager;
        $this->service_container = $service_container;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        /** @var EntityManager $em */
        $em = $this->service_container->get('doctrine')->getManager();
        $userSession = $this->creasteSessionEntry($request, $token);

        /** @var User $user */
        $user = $token->getUser();
        $user->setCurrentSession($userSession);
        $this->userManager->updateUser($user);
        $em->flush();
        if ($request->isXmlHttpRequest()) {
            $result = array('success' => true, 'targetUrl' => '/en' );
            $response = new Response(json_encode($result));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        } else {
            // Create a flash message with the authentication error message
            //$request->getSession()->getFlashBag()->set('error', $exception->getMessage());

            /** @todo: redirect user to target page when auth expires */
            /*$referer = $request->headers->get('referer');
            //$referer = $request->getSession()->get('_security.'.$this->providerKey.'.target_path', null);
            if ($referer == NULL) {
                $url = $this->router->generate('fos_user_security_login');
            } else {
                $url = $referer;
            }*/
            $url = $this->router->generate('casino_default_homepage_default');

            return new RedirectResponse($url);
        }

        //return new RedirectResponse($this->router->generate('anag_new'));
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {

        $this->creasteSessionEntry($request);
        if ($request->isXmlHttpRequest()) {
            $result = array('success' => false, 'message' => $exception->getMessage());
            $response = new Response(json_encode($result));
            $response->headers->set('Content-Type', 'application/json');

            return $response;
        }

        return new Response();
    }

    private function creasteSessionEntry(Request $request, TokenInterface $token = null)
    {
        /** @var EntityManager $em */
        $em = $this->service_container->get('doctrine')->getManager();
        $now = new \DateTime();
        $userSession = new UserSession();
        if ($token !== null){
            $userSession->setUser($token->getUser());
        } else {
            $userSession->setIsLoginSuccessful(false);
        }
        $userSession->setLastActivity($now);
        $userSession->setLoginDate($now);
        $userSession->setUserAgent($request->headers->get('User-Agent'));
        $userSession->setIp($request->getClientIp());

        $baseDir = $this->service_container->get('kernel')->getRootDir() . '/../';
        if ($request->getClientIp() != '127.0.0.1') {
            $reader = new Reader($baseDir . $this->service_container->getParameter('geoip_db_path'));
            $location = $reader->city($request->getClientIp());
            $country = ($location->country->isoCode) ? $location->country->name . ' [' . $location->country->isoCode . ']' : '';
            $city = $location->city->name;
            $userSession->setCity($city);
            $userSession->setCountry($country);
        }
        $em->persist($userSession);
        $em->flush($userSession);
        return $userSession;
    }
}
