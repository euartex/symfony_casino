<?php

namespace Casino\UserBundle\Twig;

class UserExtension extends \Twig_Extension
{
    const DEFAULT_TEMPLATE = 'CasinoUserBundle::blocks.html.twig';

    /**
     * @var \Twig_Environment
     */
    protected $environment;

    /**
     * @var \Twig_TemplateInterface[]
     */
    protected $templates = array();

    /**
     * @var string
     */
    protected $theme;

    /**
    * @var \Symfony\Component\Routing\Router
    */
    protected $router;

    /**
     * @var array
     */
    protected $names;

    /**
     * @var array
     */
    protected $params = array();
    
    /**
     * 
     * @var array
     */
    protected $pagerFantaDefs;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    public function __construct(
        \Doctrine\ORM\EntityManager $em
    ) {
        $this->em = $em;
    }
    
    public function initRuntime(\Twig_Environment $environment)
    {
        $this->environment = $environment;


    }

    /**
     * Returns a list of functions to add to the existing list.
     *
     * @return array An array of functions
     */
    public function getFunctions()
    {
        return array(
            'admin_users_dashboard'              => new \Twig_Function_Method($this, 'getUsersDashboard', array('is_safe' => array('html'))),
        );
    }



    /**
     * @return string
     */
    public function getUsersDashboard()
    {
        $users = $this->em->getRepository('CasinoUserBundle:User')
            ->findBy(
                ['roles' => [serialize([])]],
                ['last_activity' => 'desc'],
                5
            );
        return $this->renderBlock('admin_user_dashboard', ['users'=>$users]);
    }

    /**
     * Render block
     *
     * @param $name string
     * @param $parameters string
     * @return string
     */
    protected function renderBlock($name, $parameters)
    {
        foreach ($this->getTemplates() as $template) {
            if ($template->hasBlock($name)) {
                return $template->renderBlock($name, array_merge($this->environment->getGlobals(), $parameters, $this->params));
            }
        }

        throw new \InvalidArgumentException(sprintf('Block "%s" doesn\'t exist in grid template "%s".', $name, $this->theme));
    }

    /**
     * Has block
     *
     * @param $name string
     * @return boolean
     */
    protected function hasBlock($name)
    {
        foreach ($this->getTemplates() as $template) {
            if ($template->hasBlock($name)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Template Loader
     *
     * @return \Twig_TemplateInterface[]
     * @throws \Exception
     */
    protected function getTemplates()
    {
        if (empty($this->templates)) {
            if ($this->theme instanceof \Twig_Template) {
                $this->templates[] = $this->theme;
                $this->templates[] = $this->environment->loadTemplate(static::DEFAULT_TEMPLATE);
            } elseif (is_string($this->theme)) {
                $this->templates = $this->getTemplatesFromString($this->theme);
            } elseif ($this->theme === null) {
                $this->templates[] = $this->environment->loadTemplate(static::DEFAULT_TEMPLATE);
            } else {
                throw new \Exception('Unable to load template');
            }
        }

        return $this->templates;
    }

    protected function getTemplatesFromString($theme)
    {
        $this->templates = array();

        $template = $this->environment->loadTemplate($theme);
        while ($template != null) {
            $this->templates[] = $template;
            $template = $template->getParent(array());
        }

        return $this->templates;
    }

    public function getName()
    {
        return 'user_twig_extension';
    }
}
