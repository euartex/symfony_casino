<?php

namespace Casino\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class CasinoUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
