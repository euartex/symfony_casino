<?php

namespace Casino\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserDocType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('doc','hidden')
            ->add('userFile','vich_file', ['allow_delete'=>true])
          //  ->add('updatedAt')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Casino\UserBundle\Entity\UserDoc'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'casino_userbundle_userdoc';
    }
}
