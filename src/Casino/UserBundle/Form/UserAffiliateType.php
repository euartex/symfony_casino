<?php

namespace Casino\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserAffiliateType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('promoName','text')
            ->add('isActive', 'checkbox', array(
                'required' => false,
                'label' => 'Is active',
                'attr' => [ 'class' => 'field radio'],
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Casino\UserBundle\Entity\UserAffiliate'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'casino_userbundle_useraffiliate';
    }
}
