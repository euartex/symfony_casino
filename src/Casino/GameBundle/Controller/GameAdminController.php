<?php

namespace Casino\GameBundle\Controller;

use APY\DataGridBundle\Grid\Action\RowAction;
use APY\DataGridBundle\Grid\Column\TextColumn;
use APY\DataGridBundle\Grid\Row;
use APY\DataGridBundle\Grid\Source\Entity;
use Casino\DefaultBundle\Controller\BaseAdminController;
use Casino\GameBundle\Entity\Game;
use Casino\GameBundle\Entity\GameLogAggregation;
use Casino\GameBundle\Form\GameType;
use Casino\UserBundle\Entity\User;
use Doctrine\ORM\QueryBuilder;
use Exception;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;

/**
 * Game controller.
 *
 */
class GameAdminController extends BaseAdminController
{

    /**
     * Lists all Game entities.
     *
     */
    public function indexAction()
    {
        $source = new Entity('CasinoGameBundle:Game');

        $grid = $this->get('grid');
        $grid->setSource($source);
        $grid->setId('id');
        $grid->setLimits([5, 10, 15]);

        $grid->hideColumns(
            [
                'seoTitleEn',
                'seoTitleEs',
                'seoDescriptionEn',
                'seoDescriptionEs',
                'seoKeywordsEn',
                'seoKeywordsEs',
                'nameEs',
                'object',
                'script',
                'icon',
                'iconBig'
            ]
        );
        $grid->setColumnsOrder(['id', 'nameEn', 'slug',]);

        $rowAction = new RowAction('Edit', 'admin_game_edit');
        $rowAction->setRouteParameters(['id']);
        $grid->addRowAction($rowAction);

        $rowAction = new RowAction('Delete', 'admin_game_delete', true);
        $rowAction->setConfirmMessage('Do you REALLY want to delete this game? Its irreversible action!');
        $rowAction->setRouteParameters(['id']);
        $grid->addRowAction($rowAction);

        if ($grid->isReadyForRedirect()) {
            return $grid->getGridResponse();
        }

        return $this->render(
            'CasinoGameBundle:GameAdmin:index.html.twig',
            [
                'grid' => $grid,
            ]
        );
    }

    /**
     * Creates a new Game entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Game();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($entity->getWageRate() <= 0) {
            $form->get('wageRate')->addError(new FormError('Wage rate must be greater than 0. 50 - means game will be half wagefull, 200 - double wage'));
        }
        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_game_edit', ['id' => $entity->getId()]));
        }

        return $this->render(
            'CasinoGameBundle:GameAdmin:new.html.twig',
            [
                'entity' => $entity,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * Creates a form to create a Game entity.
     *
     * @param Game $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Game $entity)
    {
        $form = $this->createForm(
            new GameType(),
            $entity,
            [
                'action' => $this->generateUrl('admin_game_create'),
                'method' => 'POST',
            ]
        );

        $form->add('submit', 'submit', ['label' => 'Create']);

        return $form;
    }

    /**
     * Displays a form to create a new Game entity.
     *
     */
    public function newAction()
    {
        $entity = new Game();
        $form = $this->createCreateForm($entity);

        return $this->render(
            'CasinoGameBundle:GameAdmin:new.html.twig',
            [
                'entity' => $entity,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * Finds and displays a Game entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CasinoGameBundle:Game')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Game entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render(
            'CasinoGameBundle:GameAdmin:show.html.twig',
            [
                'entity' => $entity,
                'delete_form' => $deleteForm->createView(),
            ]
        );
    }

    /**
     * Creates a form to delete a Game entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_game_delete', ['id' => $id]))
            ->setMethod('DELETE')
            ->add('submit', 'submit', ['label' => 'Delete'])
            ->getForm();
    }

    /**
     * Displays a form to edit an existing Game entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CasinoGameBundle:Game')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Game entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);
        $games = $em->getRepository('CasinoGameBundle:Game')->findAll();
        $reservedPosition = [];
        foreach ($games as $game) {
            if ($game->getId() == $entity->getId()) {
                continue;
            }
            $reservedPosition[$game->getMainPosition()] = 1;
        }

        return $this->render(
            'CasinoGameBundle:GameAdmin:edit.html.twig',
            [
                'entity' => $entity,
                'form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
                'reservedPosition' => $reservedPosition,
            ]
        );
    }

    /**
     * Creates a form to edit a Game entity.
     *
     * @param Game $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Game $entity)
    {
        $form = $this->createForm(
            new GameType(),
            $entity,
            [
                'action' => $this->generateUrl('admin_game_update', ['id' => $entity->getId()]),
                'method' => 'PUT',
            ]
        );

        $form->add('submit', 'submit', ['label' => 'Update']);

        return $form;
    }

    /**
     * Edits an existing Game entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CasinoGameBundle:Game')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Game entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($entity->getWageRate() <= 0) {
            $editForm->get('wageRate')->addError(new FormError('Wage rate must be greater than 0. 50 - means game will be half wagefull, 200 - double wage'));
        }
        if ($editForm->isValid()) {
            $em->flush();
            $request->getSession()->getFlashBag()->add(
                'notice',
                'Your changes were saved!'
            );

            return $this->redirect($this->generateUrl('admin_game_edit', ['id' => $id]));
        }

        return $this->render(
            'CasinoGameBundle:GameAdmin:edit.html.twig',
            [
                'entity' => $entity,
                'form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ]
        );
    }

    /**
     * Deletes a Game entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CasinoGameBundle:Game')->find(intval($id));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Game entity.');
        } else {
            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_game'));
    }

    public function gamelogsAction($userid = null)
    {
        $user = null;
        if (!is_null($userid)) {
            $userid = intval($userid);
            /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
            $userManager = $this->get('fos_user.user_manager');
            /** @var User $user */
            $user = $userManager->findUserBy(['id' => $userid]);
            if (!is_object($user)) {
                throw new \Exception('User not found!');
            }
        }

        $source = new Entity('CasinoGameBundle:GameLog');
        $tableAlias = $source->getTableAlias();
        $source->manipulateQuery(
            function (QueryBuilder $query) use ($tableAlias, $userid) {
                $query->addSelect('g.nameEn');
                $query->leftJoin('Casino\GameBundle\Entity\Game', "g", "WITH", $tableAlias . ".game = g.id");
                //if user id is present - filter with that user
                if (!is_null($userid)) {
                    $query->leftJoin('Casino\UserBundle\Entity\User', "user", "WITH", $tableAlias . ".user = user.id");
                    $query->andWhere('user.id =' . $userid);
                } else {
                    $query->addSelect('user.username');
                    $query->leftJoin('Casino\UserBundle\Entity\User', "user", "WITH", $tableAlias . ".user = user.id");
                }
            }
        );

        $grid = $this->get('grid');
        $gameName = new TextColumn(
            [
                'id' => 'nameEn',
                'title' => 'Game name',
                'field' => 'g.nameEn',
                'isManualField' => true,
                'isAggregate' => false, // Defaults to false, set true if using aggregate func. like SUM()
                'source' => true,
                'filterable' => true,
                'sortable' => true
            ]);

        if (is_null($userid)) {
            $userName = new TextColumn(
                [
                    'id' => 'username',
                    'title' => 'User name',
                    'field' => 'user.username',
                    // 'isManualField' => true,
                    'isAggregate' => false, // Defaults to false, set true if using aggregate func. like SUM()
                    'source' => true,
                    'filterable' => true,
                    'sortable' => true
                ]);
            $grid->addColumn($userName);
        }

        $grid->addColumn($gameName);

        $grid->setSource($source);
        $grid->setId('id');
        $grid->setLimits([30, 50, 100]);

        $grid->hideColumns(
            ['id', 'depObjId']
        );

        $typeCol = $grid->getColumn('type');
        $typeCol->setValues([0 => 'real', 1 => 'bonus']);

        $grid->setColumnsOrder(['username', 'nameEn']);
        /*
        $rowAction = new RowAction('Edit', 'admin_game_edit');
        $rowAction->setRouteParameters(['id']);
        $grid->addRowAction($rowAction);*/

        if ($grid->isReadyForRedirect()) {
            return $grid->getGridResponse();
        }

        return $this->render(
            'CasinoGameBundle:GameAdmin:indexGamelogs.html.twig',
            [
                'grid' => $grid,
                'user' => $user
            ]
        );
    }

    /**
     * @param string $createdAt Filter by createdAt timestamp
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws Exception
     */
    public function winningStatisticsAction($createdAt = null)
    {
        $source = new Entity('CasinoGameBundle:GameLogAggregation');
        $tableAlias = $source->getTableAlias();

        $grid = $this->get('grid');

        if (!$createdAt) {
            $source->manipulateQuery(
                function (QueryBuilder $query) use ($tableAlias) {
                    $query->addSelect(sprintf('SUM(%s.balance) as balance', $tableAlias));
                    $query->addGroupBy(sprintf('%s.createdAt', $tableAlias));
                }
            );
        } else {
            $source->manipulateQuery(
                function (QueryBuilder $query) use ($tableAlias, $createdAt) {
                    $query->addSelect('g.nameEn AS game');
                    $query->leftJoin('Casino\GameBundle\Entity\Game', 'g', 'WITH', $tableAlias . '.game = g.id');
                    $query->andWhere(sprintf('%s.createdAt = :timestamp', $tableAlias))
                        ->setParameter('timestamp', new \DateTime($createdAt));
                }
            );
            $gameColumn = new TextColumn([
                'id' => 'game_win_stat',
                'title' => 'Game',
                'field' => 'game.nameEn',
                'isAggregate' => false, // Defaults to false, set true if using aggregate func. like SUM()
                'source' => true,
                'filterable' => true,
                'sortable' => true
            ]);
            $grid->addColumn($gameColumn);
        }

        $grid->setSource($source);
        $grid->setId('id');
        $grid->setLimits([30, 50, 100]);
        $grid->setDefaultOrder('createdAt', 'desc');

        $grid->hideColumns(
            ['id']
        );

        $dbSettingManager = $this->container->get('casino.db_setting_manager');
        $statSettings = unserialize($dbSettingManager->get(GameLogAggregation::DB_KEY_SETTINGS));
        $grid->getColumn('balance')->manipulateRenderCell(
            function ($value, Row $row) use ($statSettings) {
                // we multiply a value by -1 to indicate that user's win is a casino's loss
                $value *= -1;
                if ($value > 0) {
                    $value = '+' . $value;
                    $row->setColor('LightGreen');
                } elseif ($value == 0) {
                    $value = ' ' . $value;
                } else {
                    if (isset($statSettings['threshold']) && -$value >= $statSettings['threshold']) {
                        $row->setColor('Red');
                    } else {
                        $row->setColor('LightCoral');
                    }
                }
                return sprintf('%s$%s', substr($value, 0, 1), substr($value, 1));
            }
        );

        $grid->getColumn('createdAt')->manipulateRenderCell(
            function ($value, Row $row) {
                $time = new \DateTime($value);
                $timeNextHour = clone $time;
                $timeNextHour->add(new \DateInterval('PT1H'));
                return sprintf('%s - %s', $time->format('D, d M y h:iA'), $timeNextHour->format('h:iA'));
            }
        );

        if ($createdAt) {
            $rowAction = new RowAction('Show Game Log Page', 'admin_gamelog');
            $grid->addRowAction($rowAction);
        } else {
            $rowAction = new RowAction('Show', 'admin_winning_statistics');
            $rowAction->manipulateRender(function (RowAction $action, Row $row) {
                /** @var \DateTime $createdAt */
                $createdAt = $row->getField('createdAt');
                $action->setRouteParameters(['createdAt' => $createdAt->format(\DateTime::ATOM)]);
                return $action;
            });
            $grid->addRowAction($rowAction);
        }

        if ($grid->isReadyForRedirect()) {
            return $grid->getGridResponse();
        }

        return $this->render(
            'CasinoGameBundle:GameAdmin:indexWinningStatistics.html.twig',
            [
                'grid' => $grid,
                'createdAt' => $createdAt,
            ]
        );
    }
}
