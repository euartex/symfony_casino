<?php

namespace Casino\GameBundle\Controller;

use APY\DataGridBundle\Grid\Column\TextColumn;
use APY\DataGridBundle\Grid\Row;
use APY\DataGridBundle\Grid\Source\Entity;
use Casino\GameBundle\Entity\GameLog;
use Casino\TreasuryBundle\Entity\TreasuryDepositBonuses;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $games = $em->getRepository('CasinoGameBundle:Game')->findBy([], ['sort' => 'asc']);

        return $this->render('CasinoGameBundle:Default:index.html.twig', ['games' => $games]);
    }

    public function runAction($game, $type)
    {
        $em = $this->getDoctrine()->getManager();
        $game = $em->getRepository('CasinoGameBundle:Game')->findBy(['slug' => $game]);

        if (!$game) {
            throw new Exception('Page not found', 404);
        }
        $game = $game[0];

        if ($game->getIsCashDisabled() && ( $type=="real" )) {
            return $this->redirect(
                $this->generateUrl(
                    'casino_games_list'
                )
            );
        }

        $token = 'test-1000';
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if (is_object($user) && ($type !== 'demo')) {
            $token = md5($user->getId() . microtime() . $user->getBalance() . 'salt!');
            /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
            $userManager = $this->get('fos_user.user_manager');
            $user->setApiToken($token);
            $userManager->updateUser($user);

            $gameLog = new GameLog();
            $gameLog->setStatus(GameLog::STATUS_INITIAL);
            $gameLog->setToken($token);
            $gameLog->setUser($user);
            $gameLog->setGame($game);

            if ($type === 'bonus') {
                $gameLog->setDepObjId($user->getCurrentBonus()->getId());
                $balance = $user->getBonusBalance();
            } else {
                $balance = $user->getBalance();

            }

            $gameLog->setBalanceBefore($balance);
            $gameLog->setType($type === 'real');

            $em->persist($gameLog);
            $em->flush();
        }

        return $this->render(
            'CasinoGameBundle:Default:run.html.twig',
            ['entity' => $game, 'type' => $type, 'token' => $token]
        );
    }

    public function chooseAction($game, $type)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if (!is_object($user)) {
            return $this->redirect(
                $this->generateUrl(
                    'fos_user_security_login'
                )
            );
            //throw new Exception('user not found');
        }
        $em = $this->getDoctrine()->getManager();

        /** @var TreasuryDepositBonuses $userCurrentBonus */
        $userCurrentBonus = $user->getCurrentBonus();
        $userHasActiveBonus = !is_null($userCurrentBonus);

        //if bonus expired by time - deactivate it
        if ($userHasActiveBonus){
            if (!$userCurrentBonus->getIsActive() || ($userCurrentBonus->getAmount() <= 0)){
            $userHasActiveBonus = false;
            $userCurrentBonus->setIsActive(false);
            $userCurrentBonus->setExpireReason(TreasuryDepositBonuses::BY_TIME);

            $user->setBonusBalance(0);
            $user->setCurrentBonus(null);
            $em->persist($user);
            $em->persist($userCurrentBonus);
            $em->flush();
            }
        }

        /** @var Query $query */
        $query = $em->createQuery('SELECT g.is_cash_disabled FROM CasinoGameBundle:Game g WHERE g.slug = :slug')->setParameter('slug', $game);
        $isCashDisabled = $query->getSingleResult();
        $isCashDisabled = $isCashDisabled['is_cash_disabled'];

        $token = 'test-100';
        if (is_object($user)) {
            $token = md5($user->getId() . microtime() . $user->getBalance() . 'salad');
            /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
            $userManager = $this->get('fos_user.user_manager');
            $user->setApiToken($token);
            $userManager->updateUser($user);
        }

        return $this->render(
            'CasinoGameBundle:Default:choose.html.twig',
            ['currentBonus' => $userCurrentBonus, 'game' => $game, 'type' => $type, 'token' => $token, 'has_bonus' => $userHasActiveBonus, 'isCashDisabled' => $isCashDisabled]
        );
    }

    public function gameLogsAction()
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if (!is_object($user)) {

            return $this->redirect(
                $this->generateUrl(
                    'fos_user_security_login'
                )
            );
        }

        $uid = $user->getId();
        $source = new Entity('CasinoGameBundle:GameLog');
        $tableAlias = $source->getTableAlias();
        $source->manipulateQuery(
            function (QueryBuilder $query) use ($tableAlias, $uid) {
                $query->addSelect( 'g.slug');
                $query->andWhere( $tableAlias . '.user  = ' . intval($uid));
                $query->leftJoin( 'Casino\GameBundle\Entity\Game', "g", "WITH", $tableAlias . ".game = g.id");
            }
        );

        $source->manipulateRow(
            function (Row $row)
            {
                $row->setField('game_name', $row->getEntity()->getGame()->getNameEn());
                return $row;
            }
        );
        $gameName = new TextColumn(
            [
                'id'=>'game_name',
                'title'=>'Game name',
                'filterable' => false,
               // 'sortable' => false
            ]);

        $grid = $this->get('grid');
        $grid->addColumn($gameName);
        $grid->setSource($source);
        $grid->setId('id');
        $grid->setLimits([30, 50, 100]);

        $grid->getColumn('createdAt')->setTitle('Date');

        $grid->getColumn('type')->setTitle('Game type');
        $grid->getColumn('type')->manipulateRenderCell(
            function($value) {
                return ($value == 'true')? 'real': 'bonus';
            }
        );

        $grid->getColumn('balanceAfter')->setTitle('Resulting balance');
        $grid->getColumn('balanceAfter')->manipulateRenderCell(
            function($value) {
                return $value.'$';
            }
        );
        $grid->getColumn('balanceDelta')->setTitle('Game result');
        $grid->getColumn('balanceDelta')->manipulateRenderCell(
            function($value) {
                if (intval($value) >0 )
                    $value = '+'.$value;
                return $value.'$';
            }
        );
        $grid->hideColumns(
            [
                'token', 'id', 'changedAt', 'balanceBefore', 'depObjId', 'status'
            ]
        );


        if ($grid->isReadyForRedirect()) {
            return $grid->getGridResponse();
        }

        return $this->render(
            'CasinoUserBundle:Default:game.html.twig',
            ['grid' => $grid]
        );
    }
}
