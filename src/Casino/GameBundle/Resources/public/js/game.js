/**
 * Created by melnik on 24.01.15.
 */

function GameResize() {
    var width = $(window).width();
    var height = $(window).height();
    var gameWidth = width - 90;
    var gameHeight = height - 50;
    if (gameWidth / 1.6 > gameHeight) {
        gameWidth = gameHeight * 1.6;
        console.log('Game type size: B');
    } else {
        gameHeight = gameWidth / 1.6;
        console.log('Game type size: A');
    }
    $('#flashContent').css('width', gameWidth);
    $('#flashContent').css('height', gameHeight);
}

//window.addEventListener('resize', function (event) {
    GameResize();
//});

