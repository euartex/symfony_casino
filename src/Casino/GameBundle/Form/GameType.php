<?php

namespace Casino\GameBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class GameType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('slug')
            ->add('icon')
            ->add('iconBig')
            ->add('seoTitleEn')
            ->add('seoDescriptionEn')
            ->add('seoKeywordsEn')
            ->add('seoTitleEs')
            ->add('seoDescriptionEs')
            ->add('seoKeywordsEs')
            ->add('nameEn')
            ->add('nameEs')
            ->add('script')
            ->add('object')
            ->add('mainPosition')
            ->add('is_active')
            ->add('is_cash_disabled')
            ->add('wageRate','percent', ['empty_data'  => 100, 'type'=>'integer'])
            ->add('sort');
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'Casino\GameBundle\Entity\Game'
            ]
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'casino_gamebundle_game';
    }
}
