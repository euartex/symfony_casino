<?php

namespace Casino\GameBundle\Entity;

use Doctrine\ORM\EntityRepository;

class GameRepository extends EntityRepository
{
    public function findBySlug($slug)
    {
        return $this->getEntityManager()
            ->getRepository('CasinoGameBundle:Game')
            ->createQueryBuilder('n')
            ->setParameter('slug', $slug)
            ->where('n.slug = :slug')
            ->getQuery()->getSingleResult();
    }
}