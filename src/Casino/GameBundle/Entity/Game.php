<?php

namespace Casino\GameBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="Casino\GameBundle\Entity\GameRepository")
 * @ORM\Table(name="game")
 * @UniqueEntity(fields="slug", message="Slug already taken")
 */
class Game
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @ORM\Column(type="string", length=255) */
    protected $slug;

    /** @ORM\Column(type="string") */
    protected $icon;

    /** @ORM\Column(name="icon_big", type="string", nullable=true) */
    protected $iconBig;

    /** @ORM\Column(name="seo_title_en", type="string", length=255, nullable=true) */
    protected $seoTitleEn;

    /** @ORM\Column(name="seo_description_en", type="string", length=255, nullable=true) */
    protected $seoDescriptionEn;

    /** @ORM\Column(name="seo_keywords_en", type="string", length=255, nullable=true) */
    protected $seoKeywordsEn;

    /** @ORM\Column(name="seo_title_es", type="string", length=255, nullable=true) */
    protected $seoTitleEs;

    /** @ORM\Column(name="seo_description_es", type="string", length=255, nullable=true) */
    protected $seoDescriptionEs;

    /** @ORM\Column(name="seo_keywords_es", type="string", length=255, nullable=true) */
    protected $seoKeywordsEs;

    /** @ORM\Column(name="name_en", type="string", length=255) */
    protected $nameEn;

    /** @ORM\Column(name="name_es", type="string", length=255) */
    protected $nameEs;

    /** @ORM\Column(type="text", nullable=true) */
    protected $script;

    /** @ORM\Column(type="text", nullable=true) */
    protected $object;

    /** @ORM\Column(name="main_position", type="integer", nullable=true) */
    protected $mainPosition;

    /** @ORM\Column(type="boolean") */
    protected $is_active = true;

    /** @ORM\Column(type="boolean") */
    protected $is_cash_disabled = false;

    /** @ORM\Column(type="integer") */
    protected $sort = 0;

    /**
     * @var integer in percents
     * @ORM\Column(name="wage_rate", type="decimal", precision=10, scale=0, nullable=false)
     * @Assert\GreaterThan(
     *     value = 0
     * )
     */
    protected $wageRate = 100;

    /** @ORM\Column(type="datetime") */
    protected $created_at;

    public function __construct()
    {
        $this->created_at = new \DateTime("now");
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function getMainPosition()
    {
        return $this->mainPosition;
    }

    public function setMainPosition($mainPosition)
    {
        $this->mainPosition = $mainPosition;
    }

    public function getSort()
    {
        return $this->sort;
    }

    public function setSort($sort)
    {
        $this->sort = $sort;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Game
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    public function setIcon($icon)
    {
        $this->icon = $icon;
    }

    public function getIcon()
    {
        return $this->icon;
    }

    public function setIconBig($icon)
    {
        $this->iconBig = $icon;
    }

    public function getIconBig()
    {
        return $this->iconBig;
    }


    /**
     * Set seoTitleEn
     *
     * @param string $seoTitleEn
     * @return Game
     */
    public function setSeoTitleEn($seoTitleEn)
    {
        $this->seoTitleEn = $seoTitleEn;

        return $this;
    }

    /**
     * Get seoTitleEn
     *
     * @return string
     */
    public function getSeoTitleEn()
    {
        return $this->seoTitleEn;
    }

    /**
     * Set seoDescriptionEn
     *
     * @param string $seoDescriptionEn
     * @return Game
     */
    public function setSeoDescriptionEn($seoDescriptionEn)
    {
        $this->seoDescriptionEn = $seoDescriptionEn;

        return $this;
    }

    /**
     * Get seoDescriptionEn
     *
     * @return string
     */
    public function getSeoDescriptionEn()
    {
        return $this->seoDescriptionEn;
    }

    /**
     * Set seoKeywordsEn
     *
     * @param string $seoKeywordsEn
     * @return Game
     */
    public function setSeoKeywordsEn($seoKeywordsEn)
    {
        $this->seoKeywordsEn = $seoKeywordsEn;

        return $this;
    }

    /**
     * Get seoKeywordsEn
     *
     * @return string
     */
    public function getSeoKeywordsEn()
    {
        return $this->seoKeywordsEn;
    }

    /**
     * Set seoTitleEs
     *
     * @param string $seoTitleEs
     * @return Game
     */
    public function setSeoTitleEs($seoTitleEs)
    {
        $this->seoTitleEs = $seoTitleEs;

        return $this;
    }

    /**
     * Get seoTitleEs
     *
     * @return string
     */
    public function getSeoTitleEs()
    {
        return $this->seoTitleEs;
    }

    /**
     * Set seoDescriptionEs
     *
     * @param string $seoDescriptionEs
     * @return Game
     */
    public function setSeoDescriptionEs($seoDescriptionEs)
    {
        $this->seoDescriptionEs = $seoDescriptionEs;

        return $this;
    }

    /**
     * Get seoDescriptionEs
     *
     * @return string
     */
    public function getSeoDescriptionEs()
    {
        return $this->seoDescriptionEs;
    }

    /**
     * Set seoKeywordsEs
     *
     * @param string $seoKeywordsEs
     * @return Game
     */
    public function setSeoKeywordsEs($seoKeywordsEs)
    {
        $this->seoKeywordsEs = $seoKeywordsEs;

        return $this;
    }

    /**
     * Get seoKeywordsEs
     *
     * @return string
     */
    public function getSeoKeywordsEs()
    {
        return $this->seoKeywordsEs;
    }

    /**
     * Set nameEn
     *
     * @param string $nameEn
     * @return Game
     */
    public function setNameEn($nameEn)
    {
        $this->nameEn = $nameEn;

        return $this;
    }

    /**
     * Get nameEn
     *
     * @return string
     */
    public function getNameEn()
    {
        return $this->nameEn;
    }

    /**
     * Set nameEs
     *
     * @param string $nameEs
     * @return Game
     */
    public function setNameEs($nameEs)
    {
        $this->nameEs = $nameEs;

        return $this;
    }

    /**
     * Get nameEs
     *
     * @return string
     */
    public function getNameEs()
    {
        return $this->nameEs;
    }

    /**
     * Set is_active
     *
     * @param boolean $isActive
     * @return Game
     */
    public function setIsActive($isActive)
    {
        $this->is_active = $isActive;

        return $this;
    }

    /**
     * Get is_active
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->is_active;
    }


    /**
     * Set is cash disabled
     *
     * @param boolean $is_cash_disabled
     * @return Game
     */
    public function setIsCashDisabled($is_cash_disabled)
    {
        $this->is_cash_disabled = $is_cash_disabled;

        return $this;
    }

    /**
     * Get  is cash disabled
     *
     * @return boolean
     */
    public function getIsCashDisabled()
    {
        return $this->is_cash_disabled;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Game
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function getScript()
    {
        return $this->script;
    }

    public function setScript($script)
    {
        $this->script = $script;

        return $this;
    }

    public function getObject()
    {
        return $this->object;
    }

    public function setObject($object)
    {
        $this->object = $object;

        return $this;
    }

    public function getName($ln)
    {
        if ($ln == 'es') {
            return $this->getNameEs();
        }

        return $this->getNameEn();
    }

    public function getSeoTitle($ln)
    {
        if ($ln == 'es') {
            return $this->getSeoTitleEs();
        }

        return $this->getSeoTitleEn();
    }

    public function getSeoDescription($ln)
    {
        if ($ln == 'es') {
            return $this->getSeoDescriptionEs();
        }

        return $this->getSeoDescriptionEn();
    }

    public function getSeoKeywords($ln)
    {
        if ($ln == 'es') {
            return $this->getSeoKeywordsEs();
        }

        return $this->getSeoKeywordsEn();
    }

    public function getContent($ln)
    {
        if ($ln == 'es') {
            return $this->getContentEs();
        }

        return $this->getContentEn();
    }

    public function getWageRate()
    {
        return $this->wageRate;
    }

    public function setWageRate($wageRate)
    {
        $this->wageRate = $wageRate;
    }
}
