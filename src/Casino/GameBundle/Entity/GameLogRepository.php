<?php

namespace Casino\GameBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;

class GameLogRepository extends EntityRepository
{
    /**
     * @param \DateTime $lastProcessedTimestamp
     * @param \DateTime $currentTimestamp
     * @return ResultSetMapping
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findUnprocessedLogs($lastProcessedTimestamp, $currentTimestamp)
    {
        $format = 'Y-m-d H:i:s';
        $sql = sprintf("
            SELECT id, SUM(balance_delta) as balance, created_at, game_id
            FROM game_log
            WHERE type = %d and STATUS = '%s'
        ", GameLog::GAME_TYPE_REAL, GameLog::STATUS_PROCESSED);
        if (!$lastProcessedTimestamp) {
            $sql .= sprintf(" AND created_at <= '%s' ", $currentTimestamp->format($format));
        } else {
            $sql .= sprintf(
                " AND created_at > '%s' AND created_at <= '%s' ",
                $lastProcessedTimestamp->format($format),
                $currentTimestamp->format($format)
            );
        }
        $sql .= "
            GROUP BY YEAR(created_at), MONTH(created_at), DAY(created_at), HOUR(created_at), game_id
            ORDER BY created_at;
        ";
        $rsm = new ResultSetMapping();
        $rsm->addEntityResult('Casino\GameBundle\Entity\GameLogAggregation', 'a')
            ->addFieldResult('a', 'id', 'id')
            ->addFieldResult('a', 'balance', 'balance')
            ->addFieldResult('a', 'created_at', 'createdAt')
            ->addJoinedEntityResult('Casino\GameBundle\Entity\Game' , 'g', 'a', 'game')
            ->addFieldResult('g', 'game_id', 'id');


        return $this->getEntityManager()->createNativeQuery($sql, $rsm)->getResult();
    }
}
