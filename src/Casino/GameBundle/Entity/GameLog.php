<?php

namespace Casino\GameBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GameLog
 *
 * @ORM\Table(
 *  name="game_log",
 *  indexes={
 *      @ORM\Index(name="token_index", columns={"token"}),
 *      @ORM\Index(name="dep_bonus_idx", columns={"dep_obj_id"})
 *  }
 * )
 * @ORM\Entity(repositoryClass="Casino\GameBundle\Entity\GameLogRepository")
 */
class GameLog
{
    /* Real money */
    const GAME_TYPE_REAL = true;

    /* Bonus money */
    const GAME_TYPE_BONUS = false;

    /* Initial status - before the game starts */
    const STATUS_INITIAL = 'initial';

    /* Processed status - after game win/lose */
    const STATUS_PROCESSED = 'processed';

    /**
     * @var boolean
     *
     * @ORM\Column(name="type", type="boolean", nullable=true)
     */
    private $type = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="dep_obj_id", type="bigint", nullable=true)
     */
    private $depObjId;

    /**
     * @ORM\ManyToOne(targetEntity="Casino\UserBundle\Entity\User", inversedBy="gameLogs")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Casino\GameBundle\Entity\Game")
     * @ORM\JoinColumn(name="game_id", referencedColumnName="id")
     */
    private $game;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=32, nullable=true)
     */
    private $token;

    /**
     * @var string
     *
     * @ORM\Column(name="balance_before", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $balanceBefore = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="balance_after", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $balanceAfter = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="balance_delta", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $balanceDelta = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=32, nullable=true)
     */
    private $status = 'initial';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="changed_at", type="datetime", nullable=false)
     */
    private $changedAt = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt = 'CURRENT_TIMESTAMP';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    public function __construct()
    {
        $this->createdAt = new \DateTime("now");
        $this->changedAt = new \DateTime("now");
    }

    /**
     * Set type
     *
     * @param boolean $type
     *
     * @return GameLog
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return boolean
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set depObjId
     *
     * @param integer $depObjId
     *
     * @return GameLog
     */
    public function setDepObjId($depObjId)
    {
        $this->depObjId = $depObjId;

        return $this;
    }

    /**
     * Get depObjId
     *
     * @return integer
     */
    public function getDepObjId()
    {
        return $this->depObjId;
    }

    /**
     * Set user
     *
     * @param \Casino\UserBundle\Entity\User $user
     * @return GameLog
     */
    public function setUser(\Casino\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Casino\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set game
     *
     * @param \Casino\GameBundle\Entity\Game $game
     * @return GameLog
     */
    public function setGame(\Casino\GameBundle\Entity\Game $game = null)
    {
        $this->game = $game;

        return $this;
    }

    /**
     * Get game
     *
     * @return \Casino\GameBundle\Entity\Game
     */
    public function getGame()
    {
        return $this->game;
    }

    /**
     * Set token
     *
     * @param string $token
     *
     * @return GameLog
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set balanceBefore
     *
     * @param string $balanceBefore
     *
     * @return GameLog
     */
    public function setBalanceBefore($balanceBefore)
    {
        $this->balanceBefore = $balanceBefore;

        return $this;
    }

    /**
     * Get balanceBefore
     *
     * @return string
     */
    public function getBalanceBefore()
    {
        return $this->balanceBefore;
    }

    /**
     * Set balanceAfter
     *
     * @param string $balanceAfter
     *
     * @return GameLog
     */
    public function setBalanceAfter($balanceAfter)
    {
        $this->balanceAfter = $balanceAfter;

        return $this;
    }

    /**
     * Get balanceAfter
     *
     * @return string
     */
    public function getBalanceAfter()
    {
        return $this->balanceAfter;
    }

    /**
     * Set balanceDelta
     *
     * @param string $balanceDelta
     *
     * @return GameLog
     */
    public function setBalanceDelta($balanceDelta)
    {
        $this->balanceDelta = $balanceDelta;

        return $this;
    }

    /**
     * Get balanceDelta
     *
     * @return string
     */
    public function getBalanceDelta()
    {
        return $this->balanceDelta;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return GameLog
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return GameLog
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set changedAt
     *
     * @param \DateTime $changedAt
     *
     * @return GameLog
     */
    public function setChangedAt($changedAt)
    {
        $this->changedAt = $changedAt;

        return $this;
    }

    /**
     * Get changedAt
     *
     * @return \DateTime
     */
    public function getChangedAt()
    {
        return $this->changedAt;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
