<?php

namespace Casino\GameBundle\Twig;

class GameExtension extends \Twig_Extension
{
    const DEFAULT_TEMPLATE = 'CasinoGameBundle::blocks.html.twig';

    /**
     * @var \Twig_Environment
     */
    protected $environment;

    /**
     * @var \Twig_TemplateInterface[]
     */
    protected $templates = [];

    /**
     * @var string
     */
    protected $theme;

    /**
     * @var \Symfony\Component\Routing\Router
     */
    protected $router;

    /**
     * @var array
     */
    protected $names;

    /**
     * @var array
     */
    protected $params = [];

    /**
     *
     * @var array
     */
    protected $pagerFantaDefs;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    public function __construct(
        \Doctrine\ORM\EntityManager $em
    ) {
        $this->em = $em;
    }

    public function initRuntime(\Twig_Environment $environment)
    {
        $this->environment = $environment;
    }

    /**
     * Returns a list of functions to add to the existing list.
     *
     * @return array An array of functions
     */
    public function getFunctions()
    {
        return [
            'game_item' => new \Twig_Function_Method($this, 'getGameItem', ['is_safe' => ['html']]),
            'game_item_by_position' => new \Twig_Function_Method(
                $this, 'getGameItemByPosition', ['is_safe' => ['html']]
            ),
            'game_item_big'         => new \Twig_Function_Method($this, 'getGameItemBig', ['is_safe' => ['html']]),
            'game_item_pay_chooser'         => new \Twig_Function_Method($this, 'getPayChooser', ['has_current_bonus' => false]),
        ];
    }

    /**
     * @return string
     */
    public function getGameItem($slug)
    {
        $game = $this->em->getRepository('CasinoGameBundle:Game')->findOneBy(['slug' => $slug]);

        return $this->renderBlock('game_item', ['game' => $game]);
    }

    /**
     * @return string
     */
    public function getGameItemBig($position)
    {
        $game = $this->em->getRepository('CasinoGameBundle:Game')->findOneBy(['mainPosition' => $position]);

        return $this->renderBlock('game_item_big', ['game' => $game]);
    }

    /**
     * @return string
     */
    public function getPayChooser()
    {

        //$game = $this->em->getRepository('CasinoGameBundle:Game')->findOneBy(['mainPosition' => $position]);

        return $this->renderBlock('game_item_pay_chooser', ['game' => 1]);
    }

    /**
     * @return string
     */
    public function getGameItemByPosition($position)
    {
        $game = $this->em->getRepository('CasinoGameBundle:Game')
            ->findOneBy(['mainPosition' => $position], ['sort' => 'asc']);
        if (!$game) {
            return;
        }

        return $this->getGameItem($game->getSlug());
    }

    /**
     * Render block
     *
     * @param $name string
     * @param $parameters string
     * @return string
     */
    protected function renderBlock($name, $parameters)
    {
        foreach ($this->getTemplates() as $template) {
            if ($template->hasBlock($name)) {
                return $template->renderBlock(
                    $name,
                    array_merge($this->environment->getGlobals(), $parameters, $this->params)
                );
            }
        }

        throw new \InvalidArgumentException(
            sprintf('Block "%s" doesn\'t exist in grid template "%s".', $name, $this->theme)
        );
    }

    /**
     * Has block
     *
     * @param $name string
     * @return boolean
     */
    protected function hasBlock($name)
    {
        foreach ($this->getTemplates() as $template) {
            if ($template->hasBlock($name)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Template Loader
     *
     * @return \Twig_TemplateInterface[]
     * @throws \Exception
     */
    protected function getTemplates()
    {
        if (empty($this->templates)) {
            if ($this->theme instanceof \Twig_Template) {
                $this->templates[] = $this->theme;
                $this->templates[] = $this->environment->loadTemplate(static::DEFAULT_TEMPLATE);
            } elseif (is_string($this->theme)) {
                $this->templates = $this->getTemplatesFromString($this->theme);
            } elseif ($this->theme === null) {
                $this->templates[] = $this->environment->loadTemplate(static::DEFAULT_TEMPLATE);
            } else {
                throw new \Exception('Unable to load template');
            }
        }

        return $this->templates;
    }

    protected function getTemplatesFromString($theme)
    {
        $this->templates = [];

        $template = $this->environment->loadTemplate($theme);
        while ($template != null) {
            $this->templates[] = $template;
            $template = $template->getParent([]);
        }

        return $this->templates;
    }

    public function getName()
    {
        return 'game_twig_extension';
    }
}
