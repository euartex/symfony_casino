<?php

namespace Casino\GameBundle\Command;

use Casino\GameBundle\Entity\GameLogAggregation;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class AggregateGameLog extends Command
{
    use ContainerAwareTrait;

    protected function configure()
    {
        $this
            ->setName('lotedo:game:aggregate-logs')
            ->setDescription('Aggregate game logs for winning statistic representation')
            ->addOption(
                'force',
                null,
                InputOption::VALUE_NONE,
                'If set, the last processed timestamp is ignored'
            )
            ->addOption(
                'no-email',
                null,
                InputOption::VALUE_NONE,
                'Do not send email notification on winning threshold exceeding'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $event = 'aggregation';
        $stopwatch = $this->container->get('debug.stopwatch');
        $stopwatch->start($event);

        $gameLogRepo = $this->container->get('doctrine')->getRepository('CasinoGameBundle:GameLog');
        $dbSettingService = $this->container->get('casino.db_setting_manager');
        $dbTimestamp = $dbSettingService->get(GameLogAggregation::DB_KEY_TIMESTAMP);
        if ($dbTimestamp && !$input->getOption('force')) {
            $lastProcessedTimestamp = new \DateTime($dbTimestamp);
        } else {
            $lastProcessedTimestamp = null;
        }
        $currentTimestamp = new \DateTime();
        $currentTimestamp->setTime($currentTimestamp->format('H'), 0, 0);
        $gameLogAggregations = $gameLogRepo->findUnprocessedLogs($lastProcessedTimestamp, $currentTimestamp);
        $dbSettingService->set(
            GameLogAggregation::DB_KEY_TIMESTAMP,
            $currentTimestamp->format(\DateTime::ATOM)
        );

        $manager = $this->container->get('doctrine')->getManager();
        /** @var GameLogAggregation $item */
        $count = 0;
        $aggregationSum = [];
        $statSettings = unserialize($dbSettingService->get(GameLogAggregation::DB_KEY_SETTINGS));
        $statNotificationsEnabled = !empty($statSettings['enabled']) && !$input->getOption('no-email');
        foreach ($gameLogAggregations as $item) {
            $count++;
            $game = $manager->getRepository('CasinoGameBundle:Game')->findOneBy(['id' => $item->getGame()->getId()]);
            $aggregation = new GameLogAggregation();
            $timestamp = $item->getCreatedAt();
            $timestamp->setTime($timestamp->format('H'), 0, 0);
            $aggregation->setBalance($item->getBalance())
                ->setCreatedAt($timestamp)
                ->setGame($game);
            $manager->persist($aggregation);
            // sum up all games for a particular timestamp
            if ($statNotificationsEnabled) {
                $timestampFormatted = $timestamp->format(\DateTime::RFC1123);
                if (!isset($aggregationSum[$timestampFormatted])) {
                    $aggregationSum[$timestampFormatted] = 0;
                }
                $aggregationSum[$timestampFormatted] += $aggregation->getBalance();
            }
        }
        $manager->flush();

        // check if any of the time spans exceeds the winning threshold
        $thresholdExceeded = false;
        foreach ($aggregationSum as $time => $sum) {
            if (empty($statSettings['threshold'])) {
                break;
            } else {
                if ($sum >= $statSettings['threshold']) {
                    $thresholdExceeded = true;
                    $statSettings['emailText'] .= sprintf(
                        "\n$%s was lost during an hour at %s",
                        $sum,
                        $time
                    );
                }
            }
        }
        $output->writeln((new \DateTime())->format(\DateTime::RFC1123));
        if ($thresholdExceeded) {
            $emails = explode("\n", trim($statSettings['emails']));
            $this->container->get('casino.mailing.mailer')->sendRaw(
                $emails,
                $statSettings['emailSubject'],
                $statSettings['emailText']
            );
            $output->writeln('Winning threshold notification was sent.');
        }
        $stop = $stopwatch->stop($event);
        $output->writeln('New records: ' . $count);
        $output->writeln(sprintf('Time: %.2fs', $stop->getDuration() / 1000));
        $output->writeln('Task finished.');
    }
}
