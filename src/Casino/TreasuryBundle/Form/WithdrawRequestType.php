<?php

namespace Casino\TreasuryBundle\Form;

use Casino\TreasuryBundle\Entity\WithdrawRequest;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class WithdrawRequestType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('amount', 'money', array('currency' => 'USD'))
            ->add('status','choice', ['required'=>true, 'label'=>'Request status','choices'=> WithdrawRequest::$statuses])
//            ->add('createdAt', 'datetime', ['widget'=>'single_text'])
//            ->add('changedAt', 'datetime', ['widget'=>'single_text'])
//            ->add('details', 'textarea', ['required'=> false])
            ->add('user', 'entity', [
                'label' => 'User',
                'empty_value' => '- empty -',
                'class' => 'CasinoUserBundle:User',
                'property' => 'username',
                'required' => false,
            ])
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Casino\TreasuryBundle\Entity\WithdrawRequest'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'casino_treasurybundle_withdrawrequest';
    }
}
