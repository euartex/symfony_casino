<?php

namespace Casino\TreasuryBundle\Entity;

use Casino\BonusBundle\Entity\Bonuses;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * TreasuryDepositBonuses
 *
 * @ORM\Table(name="treasury_deposit_bonuses", indexes={ @ORM\Index(name="FK_treasury_deposit_bonuses_bonuses", columns={"bonus_id"}), @ORM\Index(name="FK_treasury_deposit_bonuses_treasury_deposit", columns={"deposit_id"})})
 * @ORM\Entity
 */
class TreasuryDepositBonuses
{
    const BY_USER = 'BY_USER';
    const BY_ADMIN = 'BY_ADMIN';
    const BY_TIME = 'BY_TIME';
    const BY_WAGER = 'BY_WAGER';

    /**
     * @var string
     *
     * @ORM\Column(name="balance_before", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $balanceBefore = 0.0;

    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $amount = '0.00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="changed_at", type="datetime", nullable=true)
     */
    private $changedAt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false)
     */
    private $isActive = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_cashed_out", type="boolean", nullable=false)
     */
    private $isCashedOut = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="expire_reason", type="string", length=64, nullable=true)
     */
    private $expireReason;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="lifespan", type="datetime", nullable=true)
     */
    private $lifespan;

    /**
     * @var string
     *
     * @ORM\Column(name="wage", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $wage;

    /**
     * @var string
     *
     * @ORM\Column(name="total_wage", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $totalWage;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Casino\UserBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Casino\UserBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var \Casino\TreasuryBundle\Entity\Deposit
     *
     * @ORM\ManyToOne(targetEntity="Casino\TreasuryBundle\Entity\Deposit")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="deposit_id", referencedColumnName="id")
     * })
     */
    private $deposit;

    /**
     * @var \Casino\BonusBundle\Entity\Bonuses
     *
     * @ORM\ManyToOne(targetEntity="Casino\BonusBundle\Entity\Bonuses")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="bonus_id", referencedColumnName="id")
     * })
     */
    private $bonus;

    /**
     * @var \Casino\BonusBundle\Entity\Promocode
     *
     * @ORM\ManyToOne(targetEntity="Casino\BonusBundle\Entity\Promocode")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="promo_id", referencedColumnName="id")
     * })
     */
    private $promocode;


    public function __construct(){
        $now = new \DateTime('now');
        $this->changedAt = $now;
        $this->createdAt = $now;
    }

    /**
     * Set amount
     *
     * @param string $amount
     *
     * @return TreasuryDepositBonuses
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string
     */
    public function getAmount()
    {
        return  $this->amount;
    }

    /**
     * Set balanceBefore
     *
     * @param string $balanceBefore
     *
     * @return TreasuryDepositBonuses
     */
    public function setBalanceBefore($balanceBefore)
    {
        $this->balanceBefore = $balanceBefore;

        return $this;
    }

    /**
     * Get balanceBefore
     *
     * @return string
     */
    public function getBalanceBefore()
    {
        return  $this->balanceBefore;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return TreasuryDepositBonuses
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set changedAt
     *
     * @param \DateTime $changedAt
     *
     * @return TreasuryDepositBonuses
     */
    public function setChangedAt($changedAt)
    {
        $this->changedAt = $changedAt;

        return $this;
    }

    /**
     * Get changedAt
     *
     * @return \DateTime
     */
    public function getChangedAt()
    {
        return $this->changedAt;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return TreasuryDepositBonuses
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        if ( $this->lifespan < new \DateTime('now') )
            $this->isActive = false;
        return $this->isActive;
    }

    /**
     * Set isCashedOut
     *
     * @param boolean $isCashedOut
     *
     * @return TreasuryDepositBonuses
     */
    public function setIsCashedOut($isCashedOut)
    {
        $this->isCashedOut = $isCashedOut;
        return $this;
    }

    /**
     * Get isCashedOut
     *
     * @return boolean
     */
    public function getIsCashedOut()
    {
        return $this->isCashedOut;
    }

    /**
     * Set expireReason
     *
     * @param string $expireReason
     *
     * @return TreasuryDepositBonuses
     */
    public function setExpireReason($expireReason)
    {
        $this->expireReason = $expireReason;

        return $this;
    }

    /**
     * Get expireReason
     *
     * @return string
     */
    public function getExpireReason()
    {
        return $this->expireReason;
    }

    /**
     * Set lifespan
     *
     * @param \DateTime $lifespan
     *
     * @return TreasuryDepositBonuses
     */
    public function setLifespan(\DateTime $lifespan)
    {
        $this->lifespan = $lifespan;

        return $this;
    }

    /**
     * Get lifespan
     *
     * @return \DateTime
     */
    public function getLifespan()
    {
        return $this->lifespan;
    }

    /**
     * Set wage
     *
     * @param string $wage
     *
     * @return TreasuryDepositBonuses
     */
    public function setWage($wage)
    {
        $this->wage = $wage;

        return $this;
    }

    /**
     * Get wage
     *
     * @return string
     */
    public function getWage()
    {
        return $this->wage;
    }

    /**
     * Set total wage needed to withdraw money
     *
     * @param string $totalWage
     *
     * @return TreasuryDepositBonuses
     */
    public function setTotalWage($totalWage)
    {
        $this->totalWage = $totalWage;

        return $this;
    }

    /**
     * Get total wage needed to withdraw money
     *
     * @return string
     */
    public function getTotalWage()
    {
        return $this->totalWage;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \Casino\UserBundle\Entity\User $user
     *
     * @return TreasuryDepositBonuses
     */
    public function setUser(\Casino\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Casino\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set deposit
     *
     * @param \Casino\TreasuryBundle\Entity\Deposit $deposit
     *
     * @return TreasuryDepositBonuses
     */
    public function setDeposit(\Casino\TreasuryBundle\Entity\Deposit $deposit = null)
    {
        $this->deposit = $deposit;

        return $this;
    }

    /**
     * Get deposit
     *
     * @return \Casino\TreasuryBundle\Entity\Deposit
     */
    public function getDeposit()
    {
        return $this->deposit;
    }

    /**
     * Set bonus
     *
     * @param \Casino\BonusBundle\Entity\Bonuses $bonus
     *
     * @return TreasuryDepositBonuses
     */
    public function setBonus(\Casino\BonusBundle\Entity\Bonuses $bonus = null)
    {
        $this->bonus = $bonus;

        return $this;
    }

    /**
     * Get bonus
     *
     * @return \Casino\BonusBundle\Entity\Bonuses
     */
    public function getBonus()
    {
        return $this->bonus;
    }

    /**
     * Set promocode
     *
     * @param \Casino\UserBundle\Entity\User $user
     *
     * @return TreasuryDepositBonuses
     */
    public function setPromocode(\Casino\BonusBundle\Entity\Promocode $promocode = null)
    {
        $this->promocode = $promocode;

        return $this;
    }

    /**
     * Get promocode
     *
     * @return \Casino\BonusBundle\Entity\Promocode
     */
    public function getPromocode()
    {
        return $this->promocode;
    }


    public function getInputChoise() {
        $wagePercents = $this->getWagedPercents();
        return '"' . $this->getBonus()->getName() . '" '. $this->getAmount() .'$ (' . $wagePercents  . '% wage)';
    }

    public function getWagedPercents() {
        return ($this->getTotalWage() == 0)? 0 : round( ( $this->getWage() / $this->getTotalWage() * 100 ), 2, PHP_ROUND_HALF_DOWN);
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpdate() {
        if ( $this->lifespan < new \DateTime('now') )
            $this->isActive = false;
    }

    public function getBonusName(){
        return $this->bonus->getName();
    }
}
