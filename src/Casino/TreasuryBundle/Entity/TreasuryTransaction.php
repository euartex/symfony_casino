<?php

namespace Casino\TreasuryBundle\Entity;

use Casino\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * TreasuryTransactions
 *
 * @ORM\Table(name="treasury_transactions")
 * @ORM\Entity
 */
class TreasuryTransaction
{
    const TRANSACTION_TYPE_INGOING = 1;
    const TRANSACTION_TYPE_OUTGOING = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="Id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="integer", length=1, nullable=false)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="decimal", precision=10, scale=2)
     */
    private $amount = '0.00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     *
     * @ORM\Column(name="gateway", type="string", length=64)
     */
    private $gateway = 'neteller';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="changed_at", type="datetime")
     */
    private $changedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="currency", type="string", length=3)
     */
    private $currency = 'USD';

    /**
     * @var string
     *
     * @ORM\Column(name="order_id", type="string", length=255)
     */
    private $orderID;

    /**
     * @var string
     *
     * @ORM\Column(name="last_err_msg", type="string", length=2000, nullable=true)
     */
    private $lastErrorMSG;

    /**
     * @ORM\ManyToOne(targetEntity="Casino\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="Casino\TreasuryBundle\Entity\Deposit")
     * @ORM\JoinColumn(name="deposit_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $deposit;

    /**
     * @var string
     *
     * @ORM\Column(name="details_json", type="json_array", nullable=true)
     */
    private $detailsJson;

    public function __construct(){
        $now = new \DateTime('now');
        $this->createdAt = $now;
        $this->changedAt = $now;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return TreasuryTransaction
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set amount
     *
     * @param string $amount
     *
     * @return TreasuryTransaction
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return TreasuryTransaction
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set gateway
     *
     * @param string $gateway
     *
     * @return TreasuryTransaction
     */
    public function setGateway($gateway)
    {
        $this->gateway = $gateway;

        return $this;
    }

    /**
     * Get gateway
     *
     * @return string
     */
    public function getGateway()
    {
        return $this->gateway;
    }

    /**
     * Set changedAt
     *
     * @param \DateTime $changedAt
     *
     * @return TreasuryTransaction
     */
    public function setChangedAt($changedAt)
    {
        $this->changedAt = $changedAt;

        return $this;
    }

    /**
     * Get changedAt
     *
     * @return \DateTime
     */
    public function getChangedAt()
    {
        return $this->changedAt;
    }

    /**
     * Set currency
     *
     * @param string $currency
     *
     * @return TreasuryTransaction
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set orderID
     *
     * @param string $orderID
     *
     * @return TreasuryTransaction
     */
    public function setOrderID($orderID)
    {
        $this->orderID = $orderID;

        return $this;
    }

    /**
     * Get orderID
     *
     * @return string
     */
    public function getOrderID()
    {
        return $this->orderID;
    }

    /**
     * Set lastErrorMSG
     *
     * @param string $lastErrorMSG
     *
     * @return TreasuryTransaction
     */
    public function setLastErrorMSG($lastErrorMSG)
    {
        $this->lastErrorMSG = $lastErrorMSG;

        return $this;
    }

    /**
     * Get lastErrorMSG
     *
     * @return string
     */
    public function getLastErrorMSG()
    {
        return $this->lastErrorMSG;
    }

    /**
     * Set user
     *
     * @param \Casino\UserBundle\Entity\User $user
     *
     * @return TreasuryTransaction
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Casino\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set deposit
     *
     * @param \Casino\TreasuryBundle\Entity\Deposit $deposit
     *
     * @return TreasuryTransaction
     */
    public function setDeposit(Deposit $deposit = null)
    {
        $this->deposit = $deposit;

        return $this;
    }

    /**
     * Get deposit
     *
     * @return \Casino\TreasuryBundle\Entity\Deposit
     */
    public function getDeposit()
    {
        return $this->deposit;
    }

    /**
     * Set detailsJson
     *
     * @param array $detailsJson
     *
     * @return TreasuryTransaction
     */
    public function setDetailsJson($detailsJson)
    {
        $this->detailsJson = $detailsJson;

        return $this;
    }

    /**
     * Get detailsJson
     *
     * @return array
     */
    public function getDetailsJson()
    {
        return $this->detailsJson;
    }
}
