<?php

namespace Casino\TreasuryBundle\Entity;

use Casino\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Casino\TreasuryBundle\Entity\DepositRepository")
 * @ORM\Table(name="treasury_deposit")
 */
class Deposit
{
    const STATUS_NEW = 'new';
    const STATUS_WAIT = 'pending';
    const STATUS_SUCCESS = 'accepted';
    const STATUS_CANCEL = 'cancelled';
    const STATUS_FAIL = 'failed';
    const STATUS_REJECT = 'declined';

    const FROM_ADMIN = 'admin';
    const FROM_NETELLER = 'Neteller';
    const FROM_WAGER = 'wager';
    const FROM_REAL = '$ wallet';

    const TO_BONUS = 'bonus wallet';
    const TO_REAL = '$ wallet';
    const TO_NETELLER = 'Neteller wallet';

    const PAY_DIRECTION_INTERNAL = null;
    const PAY_DIRECTION_INGOING = 'ingoing';
    const PAY_DIRECTION_OUTGOING = 'outgoing';

    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Casino\UserBundle\Entity\User", inversedBy="deposits")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /** @ORM\Column(name="balance_before", type="decimal", scale=2, nullable=true) */
    protected $balanceBefore = null;

    /** @ORM\Column(name="balance_after", type="decimal", scale=2, nullable=true) */
    protected $balanceAfter = null;

    /** @ORM\Column(type="decimal", scale=2) */
    protected $amount;

    /** @ORM\Column(type="text", length=32) */
    protected $status = self::STATUS_NEW;

    /** @ORM\Column(name="pay_direction", type="string", length=8, nullable=true) */
    protected $payDirection = self::PAY_DIRECTION_INTERNAL;

    /** @ORM\Column(type="text", length=32, nullable=true) */
    protected $merchRefId = '';

    /** @ORM\Column(name="from_money", type="text", length=64, nullable=false) */
    protected $from = self::FROM_NETELLER;

    /** @ORM\Column(name="to_money", type="text", length=64, nullable=false) */
    protected $to = self::TO_REAL;

    /** @ORM\Column(name="created_at", type="datetime") */
    protected $createdAt;

    /**
     * Small text in json about current deposit. Depends of deposit $type
     *
     * @ORM\Column(type="text", length=255) */
    protected $detailsJson;

    public function __construct()
    {
        $this->createdAt = new \DateTime("now");
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amount
     *
     * @param string $amount
     * @return Deposit
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Deposit
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Deposit
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set user
     *
     * @param \Casino\UserBundle\Entity\User $user
     * @return Deposit
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Casino\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    public function getBalanceBefore()
    {
        return $this->balanceBefore;
    }

    public function setBalanceBefore($balance)
    {
        $this->balanceBefore = $balance;

        return $this;
    }

    /**
     * Set detailsJson
     *
     * @param string $detailsJson
     *
     * @return Deposit
     */
    public function setDetailsJson($detailsJson)
    {
        $this->detailsJson = $detailsJson;

        return $this;
    }

    /**
     * Get detailsJson
     *
     * @return string
     */
    public function getDetailsJson()
    {
        return $this->detailsJson;
    }

    /**
     * Set balanceAfter
     *
     * @param string $balanceAfter
     *
     * @return Deposit
     */
    public function setBalanceAfter($balanceAfter)
    {
        $this->balanceAfter = $balanceAfter;

        return $this;
    }

    /**
     * Get balanceAfter
     *
     * @return string
     */
    public function getBalanceAfter()
    {
        return $this->balanceAfter;
    }

    /**
     * Set from
     *
     * @param string $from
     *
     * @return Deposit
     */
    public function setFrom($from)
    {
        $this->from = $from;

        return $this;
    }

    /**
     * Get from
     *
     * @return string
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * Set to
     *
     * @param string $to
     *
     * @return Deposit
     */
    public function setTo($to)
    {
        $this->to = $to;

        return $this;
    }

    /**
     * Get to
     *
     * @return string
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param mixed $merchRefId
     * @return Deposit
     */
    public function setMerchRefId($merchRefId)
    {
        $this->merchRefId = $merchRefId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMerchRefId()
    {
        return $this->merchRefId;
    }

    /**
     * @param mixed $payDirection
     * @return Deposit
     */
    public function setPayDirection($payDirection){
        $this->payDirection = $payDirection;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPayDirection(){
        return $this->payDirection;
    }
}
