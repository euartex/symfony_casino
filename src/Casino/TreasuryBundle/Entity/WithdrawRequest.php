<?php

namespace Casino\TreasuryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity(repositoryClass="Casino\TreasuryBundle\Entity\DepositRepository")
 * @ORM\Table(name="treasury_withdraw")
 */
class WithdrawRequest
{
    const STATUS_NEW = 0;
    const STATUS_IN_PROGREESS_ADMIN = 1;
    const STATUS_IN_PROGREESS_PAY_SYSTEM = 3;
    const STATUS_REJECTED_ADMIN = 4;
    const STATUS_REJECTED_PAY_SYSTEM = 5;
    const STATUS_CANCEL = 6;
    const STATUS_SUCCESS = 7;
    const STATUS_IN_PROGREESS_USER_SIGNUP = 8;

    public static $unProcessedStatuses = [
        self::STATUS_NEW,
        self::STATUS_IN_PROGREESS_PAY_SYSTEM,
        self::STATUS_IN_PROGREESS_ADMIN
    ];

    public static $statuses = [self::STATUS_NEW => 'new', self::STATUS_IN_PROGREESS_ADMIN=>'processed by admin', self::STATUS_IN_PROGREESS_PAY_SYSTEM=>'processed by payment gateway',
        self::STATUS_REJECTED_ADMIN=>'rejected by admin', self::STATUS_REJECTED_PAY_SYSTEM=>'rejected by payment system', self::STATUS_CANCEL=>'canceled',
        self::STATUS_SUCCESS=>'withdraw successful', self::STATUS_IN_PROGREESS_USER_SIGNUP=>'Pending user`s sign up'];

    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Casino\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /** @ORM\Column(type="decimal", scale=2) */
    protected $amount;

    /** @ORM\Column(type="integer", length=2) */
    protected $status = self::STATUS_NEW;

    /** @ORM\Column(name="created_at", type="datetime") */
    protected $createdAt;

    /** @ORM\Column(name="changed_at", type="datetime") */
    protected $changedAt = 'now';

    /**
     * @ORM\OneToOne(targetEntity="Casino\TreasuryBundle\Entity\Deposit")
     * @ORM\JoinColumn(name="deposit_id", referencedColumnName="id")
     */
    private $deposit;

    public function __construct()
    {
        $this->createdAt = new \DateTime("now");
        $this->changedAt = new \DateTime("now");
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amount
     *
     * @param string $amount
     *
     * @return WithdrawRequest
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return WithdrawRequest
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return WithdrawRequest
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set changedAt
     *
     * @param \DateTime $changedAt
     *
     * @return WithdrawRequest
     */
    public function setChangedAt($changedAt)
    {
        $this->changedAt = $changedAt;

        return $this;
    }

    /**
     * Get changedAt
     *
     * @return \DateTime
     */
    public function getChangedAt()
    {
        return $this->changedAt;
    }

    /**
     * Set user
     *
     * @param \Casino\UserBundle\Entity\User $user
     *
     * @return WithdrawRequest
     */
    public function setUser(\Casino\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Casino\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set deposit
     *
     * @param \Casino\TreasuryBundle\Entity\Deposit $deposit
     *
     * @return WithdrawRequest
     */
    public function setDeposit(\Casino\TreasuryBundle\Entity\Deposit $deposit = null)
    {
        $this->deposit = $deposit;

        return $this;
    }

    /**
     * Get deposit
     *
     * @return \Casino\TreasuryBundle\Entity\Deposit
     */
    public function getDeposit()
    {
        return $this->deposit;
    }
}
