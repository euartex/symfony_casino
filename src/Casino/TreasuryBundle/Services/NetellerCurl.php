<?php

namespace Casino\TreasuryBundle\Services;

use Casino\TreasuryBundle\Entity\Deposit;
use Casino\TreasuryBundle\Entity\TreasuryTransaction;
use Casino\TreasuryBundle\Entity\WithdrawRequest;
use Casino\UserBundle\Entity\User;
use Craue\ConfigBundle\DependencyInjection\CraueConfigExtension;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Router;

/**
 * @package Casino\TreasuryBundle\Controller
 */
class NetellerCurl {

    const BASE_TEST_URL = 'https://test.api.neteller.com/v1/';
    const BASE_URL = 'https://api.neteller.com/v1/';
    const TOKEN_LINK = 'oauth2/token?grant_type=client_credentials';
    const ORDERS_LINK = 'orders';
    const PAYMENTS_LINK = 'payments';
    const CUSTOMERS = 'customers';
    const TRANSFEROUT = 'transferOut';


    private $testmode = false;
    private $netellerParams = [];
    private $client_id = 'AAABTt-88twj9CQs';//'AAABTpzJ4ZX9fjsP';
    private $client_secret = '0.a-RZkrKU1n3DaUoWBSUH5HnIbMadEiEt3s1LQicnBJA.TpzRk6oYAmyc1Z3vev5Mz0rKl7c';//'0.NUBdMHxwvWYaaaTd_uqrrc-yVwBOfuSlUG-Zob7bM9c.7KY2KMXgVXEHe_uOCnXU-sIr7MM';
    private $token = null;
    private $lastOrderTransaction;
    private $amount;

    private $user = null;
    private $em;
    private $curl = null;
    private $craue = null;


    public function __construct( EntityManager $entityManager, Router $router, $craue){
        $this->em = $entityManager;
        $this->curl = new CurlHandler();
        $this->router = $router;
        $this->craue = $craue;
        $this->setNetellerSettings();
    }

    private function setNetellerSettings(){
        $this->netellerParams = unserialize($this->craue->get('neteller.config'));
        $defaults = [
            'testMode' => 'off',
            'clientId' => $this->client_id,
            'clientSecret' => $this->client_secret,
        ];

        $this->netellerParams = $this->netellerParams + $defaults;
        $this->testmode = ($this->netellerParams['testMode'] == 'on')? true : false;
    }

    private function getNetellerSettings(){
        return $this->netellerParams;
    }

    private function getBaseUrl(){
        $settings = $this->getNetellerSettings();
        return ($settings['testMode'] == 'on')? self::BASE_TEST_URL : self::BASE_URL;
    }

    private function getClientId(){
        return $this->getNetellerSettings()['clientId'];
    }

    private function getClientSecret(){
        return $this->getNetellerSettings()['clientSecret'];
    }

    public function setUser(User $user){
        $this->user = $user;
    }

    public function setAmount( $amount){
        $this->amount = $amount;
    }

    public function setLastTransaction(TreasuryTransaction $tt){
        $this->lastOrderTransaction = $tt;
    }
    public function getLastTransaction(){
        return $this->lastOrderTransaction;
    }

    protected function getAccessToken(){
        $result = ['data'=>'', 'error'=>''];

        $auth = $this->getClientId().':'.$this->getClientSecret();
        $headers = [
            'Authorization' => ' Basic '.base64_encode($auth),
        ];

        $responseArray = $this->curl
            ->prepare($headers)
            ->setUrl( $this->getBaseUrl().self::TOKEN_LINK)
            ->exec();

        if (!is_null($responseArray)){
            if (isset($responseArray['error'])){
                $result['error'] = $responseArray['error'];
            }
            if (isset($responseArray['accessToken'])){
                $this->token = $responseArray['accessToken'];
            }
        } else {
            $result['error'] = 'Malformed json';
        }
        return $result;
    }

    public function makeOrder($items){
        $orderParams = $this->getOrderConfig($items);
        $ordersResult = $this->getAccessToken();
        if (!is_null($this->token)){
            $ordersResult = $this->makeOrderRequest($orderParams);
        }
        return $ordersResult;
    }

    private function makeOrderRequest($orderData){
        $result = ['data'=>'', 'error'=>''];
        $headers = [
            'Authorization' => 'Bearer '.$this->token,
            'Content-Length'=> strlen($orderData)
        ];
        $responseArray = $this->curl
            ->prepare($headers)
            ->setUrl( $this->getBaseUrl().self::ORDERS_LINK)
            ->setBody( $orderData)
            ->exec();

        if (!is_null($responseArray)){
            dump('create trans before redirect');
            dump($responseArray);
            if (isset($responseArray['error'])){
                $result['error'] = json_encode($responseArray['error']);
            }


            if (isset($responseArray['links'])){
                foreach($responseArray['links'] as $link){
                    if ($link['rel'] == 'hosted_payment'){
                        $result['data'] = $link['url'];
                        $this->createTransaction( $responseArray);
                    }
                }
            }
        } else {
            $result['error'] = 'Malformed json';
        }
        return $result;
    }

    /**
     * Populates order information
     *
     * @param array $items
     * @return array
     */
    private function getOrderConfig($items = []){
        //randomize order id
        $merchID = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 20).time();

        $totalAmount = 0;
        $jsonItems = [];
        foreach ($items as $item){
            $quantity = (!isset($item['quantity']))? 1 : $item['quantity'];
            $totalAmount += $item['amount']*$quantity;
            $jsonItems[]= json_encode($item, JSON_FORCE_OBJECT);
        }
        $jsonItems = implode(',',$jsonItems);

        $lotedoBaseUrl = $this->router->getContext()->getHost();
        $json='{
            "order": {
                "merchantRefId": "'.$merchID.'",
                "totalAmount": '.$totalAmount.',
                "currency": "USD",
                "lang": "en_US",
                "items": ['.$jsonItems.'],
                "redirects": [{
                    "rel": "on_success",
                    "returnKeys": [
                        "id"
                    ],
                    "uri": "https://'.$lotedoBaseUrl.'/api/neteller/result?status=success"
                }, {
                    "rel": "on_cancel",
                    "returnKeys": ["id"],
                    "uri": "https://'.$lotedoBaseUrl.'/api/neteller/result?status=cancel"
                }, {
                    "rel": "on_error",
                    "returnKeys": ["id"],
                    "uri": "https://'.$lotedoBaseUrl.'/api/neteller/result?status=error"
                }]
            }
        }';

        return $json;
    }
    

    public function orderLookup(){
        $ordersResult = $this->getAccessToken();
        if (!is_null($this->token)){
            $ordersResult = $this->orderLookupRequest();
        }
        return $ordersResult;
    }

    private function orderLookupRequest(){
        $result = ['data'=>'', 'error'=>''];
        $headers = [
            'Authorization' => 'Bearer '.$this->token,
        ];

        /** @var $lt TreasuryTransaction */
        $lt = $this->lastOrderTransaction;
        $orderId = $lt->getOrderID();

        $responseArray = $this->curl
            ->prepare($headers)
            ->setUrl($this->getBaseUrl().self::ORDERS_LINK.'/'.$orderId)
            ->setOpt(CURLOPT_POST, 0)
            ->exec();

        if (!is_null($responseArray)){
            if (isset($responseArray['error'])){
                $result['error'] = json_encode($responseArray['error']);
            }
            dump('order on lookup');
            dump($responseArray);
            $this->updateOrderTransaction($responseArray);

        } else {
            $result['error'] = 'Malformed json';
        }

        return $result;
    }


    private function createTransaction( $orderDetails) {
        $te = new TreasuryTransaction();// Transaction Entity
        $te->setAmount((float)$orderDetails['totalAmount']/100);
        $te->setOrderID($orderDetails['orderId']);
        $te->setType(TreasuryTransaction::TRANSACTION_TYPE_INGOING);
        $te->setUser($this->user);
        $te->setMerchantRefId($orderDetails['merchantRefId']);
        $this->em->persist($te);
        $this->em->flush();
    }

    private function updateOrderTransaction( $orderDetails){
        /** @var $user \Casino\UserBundle\Entity\User */
        $user = $this->user;

        /** @var $lt \Casino\TreasuryBundle\Entity\TreasuryTransaction */
        $lt =  $this->lastOrderTransaction;
        $lt->setStatus($orderDetails['status']);
        $lt->setChangedAt(new \DateTime('now'));

        if ($orderDetails['status'] !== 'pending') {

            if (($orderDetails['status'] == 'paid') && ($lt->getIsCompleted() == false)){

                $paidAmount = (float)$orderDetails['totalAmount']/100;
                $userDeposit = new Deposit();
                $userDeposit->setBalanceBefore($user->getBalance());
                $userDeposit->setStatus('completed');
                $userDeposit->setTo(Deposit::TO_REAL);
                $userDeposit->setFrom(Deposit::FROM_NETELLER);
                $userDeposit->setAmount($paidAmount);
                $userDeposit->setBalanceAfter($user->getBalance() + $paidAmount);
                $userDeposit->setDetailsJson(json_encode( array('from'=> 'neteller', 'dest'=>'user' ,'order_id' => $orderDetails['orderId'] )));
                $userDeposit->setUser($user);

                $user->setBalance($user->getBalance() + $paidAmount);
                $user->addDeposit($userDeposit);

                $lt->setDeposit($userDeposit);

                //give rewarding bonus after depositing
                $this->processDepositRewards($userDeposit);
                $this->em->persist($userDeposit);
                $this->em->persist($user);
            }

            //$lt->setIsCompleted(true);
        }
        $this->em->persist($lt);
        $this->em->flush();
    }


    public function customerLookup(){
        $registered = false;
        $this->getAccessToken();
        if (!is_null($this->token)){
            $registered = $this->customerLookupRequest();
        }
        return $registered['registered'];
    }

    private function customerLookupRequest(){
        $result = ['registered' => false, 'error' => ''];

        /** @var $user \Casino\UserBundle\Entity\User */
        $user = $this->user;
        $email = $user->getEmail();//'netellertest_USD@neteller.com';

        $headers = [
            'Authorization' => 'Bearer '.$this->token,
        ];

        $responseArray = $this->curl
            ->prepare($headers)
            ->setUrl( $this->getBaseUrl() . self::CUSTOMERS . '/?email=' .$email) //'netellertest_USD@neteller.com');
            ->setOpt(CURLOPT_POST, 0)->exec();

        if (!is_null($responseArray)){
            if (isset($responseArray['error'])){
                $result['error'] = json_encode($responseArray['error']);
            }
            if (isset($responseArray['customerId'])){
                $result['registered'] = true;
            }

        } else {
            $result['error'] = 'Malformed json';
        }
        return $result;
    }

    public function redirectRegister(){
        $this->getAccessToken();
        if (!is_null($this->token)){
            return $this->registerLinkRequest();
        }
        return false;
    }

    private function registerLinkRequest(){
        $result = ['url' => '', 'error' => ''];

        $accountProfile = $this->populateAccountProfileJson();

        $headers = [
            'Authorization' => 'Bearer '.$this->token,
            'Content-Length'=> strlen($accountProfile)
        ];

        $responseArray = $this->curl
            ->prepare($headers)
            ->setUrl( $this->getBaseUrl() . self::CUSTOMERS) //'netellertest_USD@neteller.com');
            ->setBody($accountProfile)
            ->exec();

        if (!is_null($responseArray)){
            if (isset($responseArray['error'])){
                $result['error'] = json_encode($responseArray['error']);
            }
            if (isset($responseArray['links'])){
                foreach($responseArray['links'] as $link){
                    if ($link['rel'] == 'member_signup_redirect'){
                        $link['url'] = str_replace ('https://test.','https://',$link['url']);
                        $result['url'] = $link['url'];
                    }
                }
            }
        } else {
            $result['error'] = 'Malformed json';
        }
        return $result;
    }

    private function populateAccountProfileJson(){
        /** @var $user \Casino\UserBundle\Entity\User */
        $user = $this->user;

        $email = $user->getEmail();
        $userName = ( $user->getFirstName() !== '' )? ('"firstName": "' . $user->getFirstName() . '",') : ''  ;
        $userLastName = ( $user->getLastName() !== '' )? ('"lastName": "' . $user->getLastName() . '",') : ''  ;
        list($year, $month, $day) = explode("-", $user->getBirthDay()->format('Y-m-d'));
        $userGender = '';
        if ( $user->getGender() !== -1 ){
            $userGender = '"gender": "' . ( $user->getGender()? 'm' : 'f') . '"';
        }

        $redirectUrl = $this->router->generate( 'admin_treasury_neteller', [ 'amount'=> $this->amount, 'send'=>''], UrlGeneratorInterface::ABSOLUTE_PATH);
        $accountProfile = '{
         "accountProfile": {
          '.$userName.'
          '.$userLastName.'
          "email": "'.$email.'",' .
            $userGender .
            ',
          "dateOfBirth": {
            "year": "'.$year.'",
            "month": "'.$month.'",
            "day": "'.$day.'"
          },
          "accountPreferences": {
            "lang": "en",
            "currency": "USD"
          }
         },
         "linkBackUrl": "'.$redirectUrl.'"
        }';
        return $accountProfile;
    }

    public function payout(WithdrawRequest $withdrawRequest){
        $this->getAccessToken();
        if (!is_null($this->token)){
            return $this->payoutRequest($withdrawRequest);
        }
        return false;
    }

    private function payoutRequest(WithdrawRequest $withdrawRequest){
        $result = ['status' => '', 'error' => '', 'transaction'=>''];
        $merchID = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 20).time();

        $email = $withdrawRequest->getUser()->getEmail();
        $accountProfile =  '{
             "payeeProfile": {
                 "email": "'.$email.'"
             },
             "transaction": {
                 "amount": '.intval($withdrawRequest->getAmount()*100).',
                 "currency": "USD",
                 "merchantRefId": "'.$merchID.'"
             },
             "message": "Lotedo payout for ticket #'.$withdrawRequest->getId().'"
         }';

        $headers = [
            'Authorization' => 'Bearer '.$this->token,
            'Content-Length'=> strlen($accountProfile)
        ];

        $responseArray =$this->curl
            ->prepare($headers)
            ->setUrl( $this->getBaseUrl() . self::TRANSFEROUT)
            ->setBody( $accountProfile )
            ->exec();


        if (!is_null($responseArray)){


            if (isset($responseArray['error'])){
                $result['error'] = json_encode($responseArray['error']);
            }

            if (isset($responseArray['transaction'])){
                $transactionArray = $responseArray['transaction'];
                $paidAmount = (float)$transactionArray['amount']/100;

                $te = new TreasuryTransaction();// Transaction Entity
                $te->setAmount($paidAmount);
                $te->setDetailsJson($responseArray);
                $te->setOrderID($transactionArray['id']);
                $te->setType(TreasuryTransaction::TRANSACTION_TYPE_OUTGOING);
                $te->setMerchantRefId($transactionArray['merchantRefId']);
                $te->setUser($this->user);
                $te->setStatus($transactionArray['status']);
                //save error details
                if (isset($responseArray['error'])) {
                    $te->setLastErrorMSG($result['error']);
                }
                //if payment successfull but pending for user signup - set status
                if (isset($responseArray['links'])){
                    foreach($responseArray['links'] as $link){
                        if ($link['rel'] == 'member_signup_redirect'){
                            $te->setStatus($te->getStatus().' (user signup)');
                        }
                    }
                }
                $balanceAfter = $this->user->getBalance() - $paidAmount;
                $userDeposit = new Deposit();
                $userDeposit->setBalanceBefore($this->user->getBalance());
                $userDeposit->setStatus(Deposit::STATUS_SUCCESS);
                $userDeposit->setTo(Deposit::TO_NETELLER);
                $userDeposit->setFrom(Deposit::FROM_ADMIN);
                $userDeposit->setAmount($paidAmount);
                $userDeposit->setBalanceAfter($balanceAfter);
                $userDeposit->setDetailsJson(json_encode( array('from'=> 'admin', 'dest'=>'user`s neteller' ,'order_id' => $transactionArray['merchantRefId'] )));
                $userDeposit->setUser($this->user);

                $this->user->setBalance($balanceAfter);
                $this->user->addDeposit($userDeposit);
                $te->setDeposit($userDeposit);
                $withdrawRequest->setStatus(WithdrawRequest::STATUS_SUCCESS);
                $withdrawRequest->setChangedAt( new \DateTime('now'));
                $withdrawRequest->setDeposit($userDeposit);
                $withdrawRequest->setDetails(json_encode($responseArray));

                $this->em->persist($withdrawRequest);
                $this->em->persist($te);
                $this->em->persist($userDeposit);
                $this->em->persist($this->user);

                $this->em->flush();

                $result['status'] = $te->getStatus();
                $result['transaction'] = $te;
            }

        } else {
            $result['error'] = 'Malformed json';
        }
        return $result;
    }

    public function paymentLookup($lookUrl){
        $ordersResult = $this->getAccessToken();
        if (!is_null($this->token)){
            $ordersResult = $this->paymentLookupRequest($lookUrl);
        }
        return $ordersResult;
    }

    private function paymentLookupRequest($lookUrl){
        $result = ['data'=>'', 'error'=>''];
        $headers = [
            'Authorization' => 'Bearer '.$this->token,
        ];

        $responseArray = $this->curl
            ->prepare($headers)
            ->setUrl($lookUrl)
            ->setOpt(CURLOPT_POST, 0)
            ->exec();

        if (!is_null($responseArray)){
            if (isset($responseArray['error'])){
                $result['error'] = json_encode($responseArray['error']);
            }
            $result['data'] = $responseArray;

        } else {
            $result['error'] = 'Malformed json';
        }
        return $result;
    }

    /**
     * Method to add bonus for depositing (ex: bonus for 3rd deposit)
     *
     * @param Deposit $deposit
     * @param bool|false $flush
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function processDepositRewards(Deposit $deposit, $flush = false){
        /** @var User $user */
        $user = $deposit->getUser();

        //count user`s deposit number
        $depositsNumber = $this->em->createQueryBuilder()
            ->select("count(d.id)")
            ->from("CasinoTreasuryBundle:Deposit","d")
            ->where("d.user = :userId AND d.amount > 0 AND d.from ='".Deposit::FROM_NETELLER."' and d.to = '". Deposit::TO_REAL."'" )
            ->setParameter("userId", $user->getId())->getQuery()->getSingleResult();
        $depositsNumber = intval(array_pop($depositsNumber));

        //get list deposits needed to add to user asa reward for deposit
        $depositBonuses = $this->em->createQueryBuilder()
            ->select("b")
            ->from("CasinoBonusBundle:Bonuses","b")
            ->where("b.rewardForNthDep = :dnum AND b.isPublic = 0")
            ->setParameter("dnum", ($depositsNumber + 1))
            ->getQuery()->getResult();
        foreach ($depositBonuses as $deposit){
            $user->addAvBonus($deposit);
        }
        $this->em->persist($user);

        if ($flush) {
            $this->em->flush();
        }
    }

}