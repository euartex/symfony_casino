<?php

namespace Casino\TreasuryBundle\Services;

use Casino\TreasuryBundle\Entity\Deposit;
use Casino\TreasuryBundle\Entity\TreasuryTransaction;
use Casino\TreasuryBundle\Entity\WithdrawRequest;
use Casino\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Acl\Exception\Exception;

/**
 * @package Casino\TreasuryBundle\Controller
 */
class NetellerCurl2 extends  PaymentCurl {

    const BASE_TEST_URL = 'https://test.api.neteller.com/v1/';
    const PAYMENT_BASE_URL = 'https://api.neteller.com/v1/';
    const TOKEN_LINK = 'oauth2/token?grant_type=client_credentials';
    const ORDERS_LINK = 'orders';
    const PAYMENTS_LINK = 'payments';
    const CUSTOMERS = 'customers';
    const TRANSFEROUT = 'transferOut';
    const REFTYPE = '?refType=merchantRefId ';

    
    private $netellerParams = [];
    protected $client_id = 'AAABTt-88twj9CQs';
    protected $client_secret = '0.a-RZkrKU1n3DaUoWBSUH5HnIbMadEiEt3s1LQicnBJA.TpzRk6oYAmyc1Z3vev5Mz0rKl7c';

    public function __construct( EntityManager $entityManager, Router $router, $craue, $debug){
        parent::__construct( $entityManager, $router, $craue, $debug);
        $this->setNetellerSettings();
    }

    private function setNetellerSettings(){
        $this->netellerParams = unserialize($this->craue->get('neteller.config'));
        $defaults = [
            'testMode' => 'off',
            'clientId' => $this->client_id,
            'clientSecret' => $this->client_secret,
        ];

        $this->netellerParams = $this->netellerParams + $defaults;
        $this->testmode = ($this->netellerParams['testMode'] == 'on')? true : false;
    }

    protected function getNetellerSettings(){
        return $this->netellerParams;
    }

    protected function getBaseUrl(){
        return ($this->getNetellerSettings()['testMode'] == 'on')? self::BASE_TEST_URL : self::PAYMENT_BASE_URL;
    }

    protected function getClientId(){
        return $this->getNetellerSettings()['clientId'];
    }

    protected function getClientSecret(){
        return $this->getNetellerSettings()['clientSecret'];
    }

    protected function getAccessToken(){
        $result = ['data'=>'', 'error'=>''];
        $auth = $this->getClientId().':'.$this->getClientSecret();
        $headers = [
            'Authorization' => ' Basic '.base64_encode($auth),
        ];

        $responseArray = $this->curl
            ->prepare($headers)
            ->setUrl( $this->getBaseUrl().self::TOKEN_LINK)
            ->exec();

        if (!is_null($responseArray)){
            if (isset($responseArray['error'])){
                $result['error'] = $responseArray['error'];
            }
            if (isset($responseArray['accessToken'])){
                $this->token = $responseArray['accessToken'];
            }
        } else {
            $result['error'] = 'Malformed json';
        }
        return $result;
    }

    /**
     * Populates order information
     *
     * @param array $items
     * @return array
     */
    protected function getOrderConfig($items = []){
        //randomize order id
        $merchID = $this->createMerchRefId();

        $totalAmount = 0;
        $jsonItems = [];
        foreach ($items as $item){
            $quantity = (!isset($item['quantity']))? 1 : $item['quantity'];
            $totalAmount += $item['amount']*$quantity;
            $jsonItems[]= json_encode($item, JSON_FORCE_OBJECT);
        }
        $jsonItems = implode(',',$jsonItems);

        $lotedoBaseUrl = $this->router->getContext()->getHost();
        $json='{
            "order": {
                "merchantRefId": "'.$merchID.'",
                "totalAmount": '.$totalAmount.',
                "currency": "USD",
                "lang": "en_US",
                "items": ['.$jsonItems.'],
                "redirects": [{
                    "rel": "on_success",
                    "returnKeys": [
                        "id"
                    ],
                    "uri": "https://'.$lotedoBaseUrl.'/api/neteller/result?status=success"
                }, {
                    "rel": "on_cancel",
                    "returnKeys": ["id"],
                    "uri": "https://'.$lotedoBaseUrl.'/api/neteller/result?status=cancel"
                }, {
                    "rel": "on_error",
                    "returnKeys": ["id"],
                    "uri": "https://'.$lotedoBaseUrl.'/api/neteller/result?status=error"
                }]
            }
        }';

        return $json;
    }

    protected function makeOrderRequest($orderData){
        $result = ['data'=>'', 'error'=>''];
        $headers = [
            'Authorization' => 'Bearer '.$this->token,
            'Content-Length'=> strlen($orderData)
        ];
        $responseArray = $this->curl
            ->prepare($headers)
            ->setUrl( $this->getBaseUrl().self::ORDERS_LINK)
            ->setBody( $orderData )
            ->exec();

        if (!is_null($responseArray)){

            if($this->debug){
                dump('create deposit & transaction before redirect');
                dump($responseArray);
            }
            if (isset($responseArray['error'])){
                $result['error'] = json_encode($responseArray['error']);
            }

            if (isset($responseArray['links'])){
                foreach($responseArray['links'] as $link){
                    //we got link to check
                    if ($link['rel'] == 'hosted_payment'){
                        $result['data'] = $link['url'];
                        $dep = $this->createDeposit( $responseArray );
                        $dep->setPayDirection(Deposit::PAY_DIRECTION_INGOING);
                        $this->em->persist($dep);
                        $tran =$this->createTransaction( $responseArray );
                        $tran->setType(TreasuryTransaction::TRANSACTION_TYPE_INGOING);
                        $tran->setDeposit($dep);
                        $this->em->persist($tran);
                        $this->em->flush();
                        if($this->debug){
                            dump($dep);
                        }
                    }
                }
            }
        } else {
            $result['error'] = 'Malformed json';
        }
        return $result;
    }

    protected function paymentLookupRequest($urlOrObject){
        $result = ['data'=>'', 'error'=>''];
        $headers = [
            'Authorization' => 'Bearer '.$this->token,
        ];

        if ($urlOrObject instanceof Deposit) {
            $paymentLookUrl =  $this->getBaseUrl() . self::PAYMENTS_LINK . '/' . $urlOrObject->getMerchRefId() . self::REFTYPE;
        } else {
            $paymentLookUrl = $urlOrObject;
        }

        $responseArray = $this->curl
            ->prepare($headers)
            ->setUrl($paymentLookUrl)
            ->setOpt(CURLOPT_POST, 0)
            ->exec();

        if($this->debug){
            dump('callback lookup result');
            dump($responseArray);
        }

        if (!is_null($responseArray)){
            //if error just show error
            if (isset($responseArray['error'])){
                $result['error'] = json_encode($responseArray['error']);
            } else { //if all ok - update payment status
                $this->updatePaymentStatus($responseArray);
            }
            $result['data'] = $responseArray;

        } else {
            $result['error'] = 'Malformed json';
        }
        return $result;
    }

    /**
     * @param array $orderDetails
     * @param bool $write
     * @return Deposit
     */
    private function createDeposit($orderDetails, $write = false)
    {
        $deposit = new Deposit();
       // $deposit->setBalanceBefore($this->user->getBalance());
        $deposit->setStatus(Deposit::STATUS_WAIT);
        $deposit->setTo(Deposit::TO_REAL);
        $deposit->setFrom(Deposit::FROM_NETELLER);
        $deposit->setAmount((float) $orderDetails['totalAmount']/100);
       // $deposit->setBalanceAfter((float) $this->user->getBalance() + (float) $orderDetails['totalAmount']);
        $deposit->setMerchRefId($orderDetails['merchantRefId']);
        $deposit->setDetailsJson(json_encode( array('from'=> 'neteller', 'dest'=>'user' ,'order_id' => $orderDetails['orderId'] )));
        $deposit->setUser($this->user);
        if ($write){
            $this->em->persist($deposit);
            $this->em->flush();
        }
        return $deposit;
    }

    /**
     * @param array $orderDetails
     * @param bool $write
     * @return TreasuryTransaction
     */
    private function createTransaction($orderDetails, $write = false)
    {
        $transaction = new TreasuryTransaction();
        $transaction->setAmount((float) $orderDetails['totalAmount']/100);
        $transaction->setOrderID($orderDetails['orderId']);
        $transaction->setDetailsJson($orderDetails);
        $transaction->setUser($this->user);
        if ($write){
            $this->em->persist($transaction);
            $this->em->flush();
        }
        return $transaction;
    }

    private function updatePaymentStatus($orderDetails, $write = true) {
        if ( isset($orderDetails['transaction']['merchantRefId'])) {
            $merchantRefId = $orderDetails['transaction']['merchantRefId'];
            $paymentStatus = $orderDetails['transaction']['status'];

            if($this->debug){
                dump($orderDetails);
            }
            $deposit = $this->em->getRepository('CasinoTreasuryBundle:Deposit')->findOneBy(['merchRefId'=>$merchantRefId]);
            if (!($deposit instanceof Deposit)) {
                throw new Exception('Deposit not Found', 404);
            }
            if($this->debug){
                dump($deposit);
            }
            $isTransOutgoing =  $deposit->getPayDirection() == Deposit::PAY_DIRECTION_OUTGOING;

            $transactionParams = [];
            $transactionParams['totalAmount'] = $orderDetails['transaction']['amount'];
            $transactionParams['orderId'] = $orderDetails['transaction']['id'];
            $transactionParams['details'] = json_encode($orderDetails['transaction']);
            $tran = $this->createTransaction( $transactionParams );
            $tran->setUser($deposit->getUser());
            $tran->setType( ($isTransOutgoing)? TreasuryTransaction::TRANSACTION_TYPE_OUTGOING : TreasuryTransaction::TRANSACTION_TYPE_INGOING);
            $tran->setDeposit($deposit);
            $this->em->persist($tran);

            //update deposit only if dep is not complete
            if (in_array($deposit->getStatus(), [Deposit::STATUS_NEW, Deposit::STATUS_WAIT]))
            {
                $deposit->setStatus($paymentStatus);
                if ($paymentStatus == Deposit::STATUS_SUCCESS) {

                    $amount = (float)$deposit->getAmount();
                    //for outgoing transactions we need to substract balance
                    if ($isTransOutgoing){
                        $amount = -$amount;
                    } else {
                        $this->processDepositRewards($deposit);
                    }
                    $deposit->setBalanceBefore($deposit->getUser()->getBalance());
                    $deposit->setBalanceAfter($deposit->getUser()->getBalance() + $amount);
                    $deposit->getUser()->setBalance($deposit->getUser()->getBalance() + $amount);
                }
                $this->updateRelatedWithDrawrequest( $deposit );
                $this->em->persist($deposit);
                if($this->debug){
                    dump($deposit);
                }
            }

            if ($write) {
                $this->em->flush();
            }
        }
    }

    /**
     * Method to add bonus for depositing (ex: bonus for 3rd deposit)
     *
     * @param Deposit $deposit
     * @param bool|false $flush
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function processDepositRewards(Deposit $deposit, $flush = false){
        /** @var User $user */
        $user = $deposit->getUser();

        //count user`s deposit number
        $depositsNumber = $this->em->createQueryBuilder()
            ->select("count(d.id)")
            ->from("CasinoTreasuryBundle:Deposit","d")
            ->where("d.user = :userId AND d.amount > 0 AND d.from ='".Deposit::FROM_NETELLER."' and d.to = '". Deposit::TO_REAL."'" )
            ->setParameter("userId", $user->getId())->getQuery()->getSingleResult();
        $depositsNumber = intval(array_pop($depositsNumber));

        //get list deposits needed to add to user asa reward for deposit
        $depositBonuses = $this->em->createQueryBuilder()
            ->select("b")
            ->from("CasinoBonusBundle:Bonuses","b")
            ->where("b.rewardForNthDep = :dnum AND b.isPublic = 0")
            ->setParameter("dnum", ($depositsNumber + 1))
            ->getQuery()->getResult();
        foreach ($depositBonuses as $deposit){
            $user->addAvBonus($deposit);
        }
        $this->em->persist($user);

        if ($flush) {
            $this->em->flush();
        }
    }

    protected function payoutRequest(WithdrawRequest $withdrawRequest){
        $result = ['status' => '', 'error' => '', 'transaction'=>''];
        $merchID = $this->createMerchRefId();
        $email = $withdrawRequest->getUser()->getEmail();
        $accountProfile =  '{
             "payeeProfile": {
                 "email": "'.$email.'"
             },
             "transaction": {
                 "amount": '.intval($withdrawRequest->getAmount()*100).',
                 "currency": "USD",
                 "merchantRefId": "'.$merchID.'"
             },
             "message": "Lotedo payout for ticket #'.$withdrawRequest->getId().'"
         }';

        $headers = [
            'Authorization' => 'Bearer '.$this->token,
            'Content-Length'=> strlen($accountProfile)
        ];

        $responseArray =$this->curl
            ->prepare($headers)
            ->setUrl( $this->getBaseUrl() . self::TRANSFEROUT)
            ->setBody( $accountProfile )
            ->exec();

        if (!is_null($responseArray)){
            if (isset($responseArray['error'])){
                $result['error'] = json_encode($responseArray['error']);
            }
            if($this->debug){
                dump($responseArray);
            }

            if (isset($responseArray['transaction'])) {
                $transactionInfo = $responseArray['transaction'];

                $depOpts = [];
                $depOpts['totalAmount'] = $transactionInfo['amount'];
                $depOpts['merchantRefId'] = $transactionInfo['merchantRefId'];
                $depOpts['orderId'] = $transactionInfo['id'];
                $depOpts['detailsJson'] = $transactionInfo;
                $dep = $this->createDeposit($depOpts);
                $dep->setPayDirection(Deposit::PAY_DIRECTION_OUTGOING);
                $dep->setStatus($transactionInfo['status']);
                $dep->setFrom(Deposit::FROM_ADMIN);
                $dep->setTo(Deposit::TO_NETELLER);
                $withdrawRequest->setDeposit($dep);

                if (isset($responseArray['links'])){
                    foreach($responseArray['links'] as $link){
                        if ($link['rel'] == 'member_signup_redirect'){
                            $withdrawRequest->setStatus(WithdrawRequest::STATUS_IN_PROGREESS_USER_SIGNUP);
                            break;
                        }
                    }
                }

                $transaction = $this->createTransaction($depOpts);
                if (isset($responseArray['error'])) {
                    $transaction->setLastErrorMSG($result['error']);
                }
                $transaction->setDeposit($dep);
                $transaction->setType(TreasuryTransaction::TRANSACTION_TYPE_OUTGOING);

                if($this->debug){
                    dump('before check to success');
                    dump($dep);
                }

                //if we got success just now
                if ( $dep->getStatus() == Deposit::STATUS_SUCCESS) {
                    $paidAmount = (float) $depOpts['totalAmount']/100;
                    $balanceAfter = $dep->getUser()->getBalance() - $paidAmount;
                    $dep->setBalanceAfter($balanceAfter);
                    $dep->setBalanceBefore($dep->getUser()->getBalance());
                    $dep->getUser()->setBalance($balanceAfter);
                    $withdrawRequest->setStatus(WithdrawRequest::STATUS_SUCCESS);
                }

                $this->em->persist($withdrawRequest);
                $this->em->persist($dep);
                $this->em->persist($transaction);
                $this->em->persist($this->user);

                $this->em->flush();

                $result['status'] = $dep->getStatus();
                $result['dep'] = $dep;
            }

        } else {
            $result['error'] = 'Malformed json';
        }
        return $result;
    }

    private function createMerchRefId() {
        return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 20).time().'l';
    }

    private function updateRelatedWithDrawrequest(Deposit $deposit) {
        $wr = $this->em->getRepository('CasinoTreasuryBundle:WithdrawRequest')->findOneBy(['deposit'=>$deposit]);
        if ($wr instanceof WithdrawRequest) {

            if ($this->debug){
                dump($wr);
                dump($deposit);
            }
            if ($deposit->getStatus()!== Deposit::STATUS_WAIT) {
                $status = $wr->getStatus();
                switch ($deposit->getStatus()) {
                    case Deposit::STATUS_SUCCESS :
                        $status = WithdrawRequest::STATUS_SUCCESS;
                        break;
                    case Deposit::STATUS_CANCEL :
                        $status = WithdrawRequest::STATUS_CANCEL;
                        break;
                    case Deposit::STATUS_FAIL :
                        $status = WithdrawRequest::STATUS_REJECTED_PAY_SYSTEM;
                        break;
                    case Deposit::STATUS_REJECT :
                        $status = WithdrawRequest::STATUS_REJECTED_ADMIN;
                        break;
                }
                if ($this->debug){
                    dump($status);
                }
                $wr->setStatus($status);
            }
            $wr->setChangedAt(new \DateTime());
            $this->em->persist($wr);
            $this->em->flush($wr);
        }
    }
}