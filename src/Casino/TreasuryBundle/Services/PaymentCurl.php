<?php

namespace Casino\TreasuryBundle\Services;

use Casino\TreasuryBundle\Entity\WithdrawRequest;
use Casino\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Routing\Router;


abstract class PaymentCurl {

    const BASE_TEST_URL = 'https://test.api.neteller.com/v1/';
    const PAYMENT_BASE_URL = 'https://api.neteller.com/v1/';
    const TOKEN_LINK = 'oauth2/token?grant_type=client_credentials';
    const ORDERS_LINK = 'orders';
    const PAYMENTS_LINK = 'payments';
    const CUSTOMERS = 'customers';
    const TRANSFEROUT = 'transferOut';


    protected $testmode = false;
    protected $client_id;
    protected $client_secret;
    protected $token = null;

    protected $user = null;
    protected $em;
    protected $curl = null;
    protected $craue = null;
    protected $router = null;
    protected $debug = false;


    public function __construct( EntityManager $entityManager, Router $router, $craue, $debug){
        $this->em = $entityManager;
        $this->curl = new CurlHandler();
        $this->router = $router;
        $this->craue = $craue;
        $this->debug = $debug;
    }

    protected function getBaseUrl(){
        return ($this->testmode)? self::BASE_TEST_URL : self::PAYMENT_BASE_URL;
    }

    protected function getClientId(){
        return $this->client_id;
    }

    protected function getClientSecret(){
        return $this->client_secret;
    }

    public function setUser(User $user){
        $this->user = $user;
    }

    abstract protected function getAccessToken();

    /**
     * Populates order information
     *
     * @param array $items
     * @return array
     */
    abstract protected function getOrderConfig($items = []);

    public function makeOrder($items){
        $orderParams = $this->getOrderConfig($items);
        $ordersResult = $this->getAccessToken();
        if (!is_null($this->token)){
            $ordersResult = $this->makeOrderRequest($orderParams);
        }
        return $ordersResult;
    }

    abstract protected function makeOrderRequest($orderData);

    public function paymentLookup($urlOrObj){
        $ordersResult = $this->getAccessToken();
        if (!is_null($this->token)){
            $ordersResult = $this->paymentLookupRequest($urlOrObj);
        }
        return $ordersResult;
    }

    abstract protected function paymentLookupRequest($urlOrObj);

    public function payout(WithdrawRequest $withdrawRequest){
        $this->getAccessToken();
        if (!is_null($this->token)){
            return $this->payoutRequest($withdrawRequest);
        }
        return false;
    }

    protected abstract function payoutRequest(WithdrawRequest $withdrawRequest);

}