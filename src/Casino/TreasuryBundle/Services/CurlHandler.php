<?php
/**
 * Created by PhpStorm.
 * User: Share
 * Date: 31.07.2015
 * Time: 11:57
 */

namespace Casino\TreasuryBundle\Services;


class CurlHandler
{
    private $curl = null;

    public function prepare($headers){
        if ($this->curl !== null){
            $this->reset();
        } else {
            $this->curl = curl_init();
        }

        $headers = $this->mergeHeaders($headers);

        curl_setopt($this->curl, CURLOPT_POST, 1);
        curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($this->curl, CURLOPT_HEADER, 1);
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($this->curl, CURLOPT_VERBOSE, 1);
        curl_setopt($this->curl, CURLINFO_HEADER_OUT, true);
        curl_setopt($this->curl, CURLOPT_SSLVERSION, 1);
        return $this;
    }

    public function setOpt($opt, $value){
        curl_setopt($this->curl, $opt, $value);
        return $this;
    }

    public function setUrl($value){
        curl_setopt($this->curl, CURLOPT_URL, $value);
        return $this;
    }

    public function setBody($value){
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $value);
        return $this;
    }

    private function mergeHeaders($headers){
        $defaultHeaders = [
            'Cache-Control' => 'no-chache',
            'Content-Type' => 'application/json',
            'Expect' => '',
            'Content-Length' => '',
            'Accept' => '',
            'Host' => ''
        ];
        $flatHeaders = [];
        $headers = $headers + $defaultHeaders;
        foreach ($headers as $key=>$value){
            $flatHeaders[]=$key.':'.$value;
        }
        return $flatHeaders;
    }

    public function exec(){
        return $this->handleResponse(curl_exec($this->curl));
    }

    private function reset(){
        if (function_exists('curl_reset'))
        curl_reset($this->curl);
    }

    public function handleResponse($response){
        $header_size = curl_getinfo($this->curl, CURLINFO_HEADER_SIZE);
        $responseBody = substr($response, $header_size);
        $arrayResponse = json_decode($responseBody, true);
        if (is_null($arrayResponse)){
            dump($response);
        }
        $this->reset();
        return $arrayResponse;
    }
}