<?php

namespace Casino\TreasuryBundle\Controller;

use APY\DataGridBundle\Grid\Action\RowAction;
use APY\DataGridBundle\Grid\Source\Entity;
use Casino\TreasuryBundle\Entity\Deposit;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;

class DepositUserController extends Controller
{

    public function indexAction(){
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if (!is_object($user)) {
            return $this->redirect(
                $this->generateUrl(
                    'fos_user_security_login'
                )
            );
        }

        $grid = $this->generateDepositsGrid($user, [10, 15, 20]);
        $grid->hideColumns(['payDirection']);
        if ($grid->isReadyForRedirect()) {
            return $grid->getGridResponse();
        }
        return $this->render(
            'CasinoTreasuryBundle:Default:index.html.twig',
            ['grid' => $grid, 'title' => 'Your deposits']
        );
    }

    /*
     * Make deposit Action
     */
    public function makeDepositAction(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if (!is_object($user)) {
            return $this->redirect(
                $this->generateUrl(
                    'fos_user_security_login'
                )
            );
        }

        $form = $this->container->get('form.factory')->createNamedBuilder('deposit_form', 'form', array(), array())
          //  ->setAction($this->generateUrl('casino_users_get_bonuses'))
            ->setMethod('post')
            ->add('amount', 'money', array(
                'currency' => 'USD',
                'required' => true,
                'attr' => array(
                    'placeholder' => 'Money you want to deposit',
                )
            ))
            ->add('awareness',
                'checkbox', array(
                    'label'    => 'I am agreed with terms.',
                    'required' => true,
                ))
            ->add('Make deposit', 'submit', array(
                'attr' => array('class' => 'submit button_decoration wide-btn'),
            ))
            ->getForm();


        $form->handleRequest($request);

        if ($form->isValid()) {

            $amount = floatval($request->get('deposit_form')['amount']);
            if ($amount <= 0) {
                $form->get('amount')->addError(new FormError('This field must be number greater than 0.'));
            } else {

                //redirect to neteller buy url generration page
                return $this->redirect(
                    $this->generateUrl(
                        'admin_treasury_neteller', ['amount'=>$amount, 'send'=>'']
                    )
                );


            }
        }

        $grid = $this->generateDepositsGrid($user);
        $grid->hideColumns('merchRefId');

        if ($grid->isReadyForRedirect()) {
            return $grid->getGridResponse();
        }

        return $this->render(
            'CasinoTreasuryBundle:Default:deposit.html.twig',
            [ 'form' => $form->createView(), 'grid' => $grid ]
        );
    }

    private function generateDepositsGrid($user,$limitPerPAge = [5]){
        $em = $this->getDoctrine()->getManager();
        $deposits = $em->getRepository('CasinoTreasuryBundle:Deposit')->findBy( array('user' => $user ), array('createdAt'=>'DESC') );

        $source = new Entity('CasinoTreasuryBundle:Deposit');
        $source->setData($deposits);

        $grid = $this->get('grid');
        $grid->setSource($source);

        $rowAction = new RowAction('Re-check status', 'user_treasury_check_deposit');
        $rowAction->setRouteParameters(['id']);
        $grid->addRowAction($rowAction);
        $column = $grid->getColumn('balanceAfter');
        $column->setTitle('Balance');
        $grid->getColumn('amount')->manipulateRenderCell(
            function($value) {
                if ($value < 0){
                    $value = 0;
                }
                return $value. '$';
            }
        );
        $grid->getColumn('createdAt')->setTitle('Date');
        $grid->setId('id');
        $grid->setLimits($limitPerPAge);

        $grid->hideColumns(
            [
                'id','balanceBefore', 'detailsJson', 'merchRefId', 'payDirection'
            ]
        );
        return $grid;
    }
}
