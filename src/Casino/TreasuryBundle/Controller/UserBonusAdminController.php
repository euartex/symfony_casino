<?php

namespace Casino\TreasuryBundle\Controller;

use APY\DataGridBundle\Grid\Column\TextColumn;
use APY\DataGridBundle\Grid\Source\Entity;
use Casino\DefaultBundle\Controller\BaseAdminController;
use Casino\UserBundle\Entity\User;
use Doctrine\ORM\QueryBuilder;

class UserBonusAdminController extends BaseAdminController
{

    public function indexAction($userid)
    {
        $userid = intval($userid);
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var User $user */
        $user = $userManager->findUserBy(['id'=>$userid]);
        if (!is_object($user)){
            throw new \Exception(404,'User not found!');
        }

        $source = new Entity('CasinoTreasuryBundle:TreasuryDepositBonuses');

        $tableAlias = $source->getTableAlias();
        $source->manipulateQuery(
            function (QueryBuilder $query) use ($tableAlias, $userid) {
                $query->leftJoin('Casino\UserBundle\Entity\User', "user", "WITH", $tableAlias . ".user = user.id");
                $query->orderBy($tableAlias . '.changedAt', 'DESC');
                $query->andWhere('user.id =' . $userid);
                $query->addSelect(" (($tableAlias.wage / $tableAlias.totalWage)*100) as wage_percents");
            }
        );

        $grid = $this->get('grid');
        $grid->setId('id');
        $grid->setSource($source);

        $grid->setLimits([5, 10, 15]);

        $wagePercents = new TextColumn(
            [
                'id'=>'wage_percents',
                'title'=>'Wage percents',
                'field' => $tableAlias.'.wage_percents',
                 'isManualField' => true,
                'isAggregate' => false, // Defaults to false, set true if using aggregate func. like SUM()
                'source' => true,
                'filterable' => false,
                'sortable' => false
            ]);
        $grid->addColumn($wagePercents);

        if ($grid->isReadyForRedirect()) {
            return $grid->getGridResponse();
        }

        return $this->render(
            'CasinoTreasuryBundle:DepositAdmin:index.html.twig',
            ['grid' => $grid, 'user'=>$user]
        );

    }

}
