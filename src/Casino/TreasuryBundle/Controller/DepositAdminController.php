<?php

namespace Casino\TreasuryBundle\Controller;

use APY\DataGridBundle\Grid\Column\TextColumn;
use Casino\DefaultBundle\Controller\BaseAdminController;
use Casino\TreasuryBundle\Entity\TreasuryTransaction;
use Casino\UserBundle\Entity\User;
use Doctrine\ORM\QueryBuilder;
use APY\DataGridBundle\Grid\Source\Entity;
use APY\DataGridBundle\Grid\Action\RowAction;
use Casino\TreasuryBundle\Entity\Deposit;
use Symfony\Component\HttpFoundation\Request;

/**
 * Deposit controller.
 *
 */
class DepositAdminController extends BaseAdminController
{
    public $statuses = [Deposit::STATUS_NEW, Deposit::STATUS_SUCCESS, Deposit::STATUS_WAIT, Deposit::STATUS_CANCEL, Deposit::STATUS_FAIL, Deposit::STATUS_REJECT];

    /**
     * Lists all Deposit entities.
     *
     */
    public function indexAction(Request $request, $userid=null)
    {
        if ( !is_null($userid) )
            $userid = intval($userid);

        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var User $user */
        $user = $userManager->findUserBy(['id'=>$userid]);
        if (!is_object($user) && $userid != null) {
            throw new \Exception('User not found!', 404);
        }

        $grid = $this->get('grid');
        $source = new Entity('CasinoTreasuryBundle:Deposit');

        $tableAlias = $source->getTableAlias();
        $source->manipulateQuery(
            function (QueryBuilder $query) use ($tableAlias, $userid) {
                $query->leftJoin( 'Casino\UserBundle\Entity\User', "user", "WITH", $tableAlias . ".user = user.id");
                //if user id is present - filter with that user
                if ( !is_null($userid) ){
                    $query->andWhere('user.id ='.$userid);
                } else {
                    $query->addSelect( 'user.username');
                }
            }
        );

        if ( is_null($userid) ){
            $userName = new TextColumn(
                [
                    'id'=>'username',
                    'title'=>'User name',
                    'sortable' => false
                ]);
            $userName->setIsManualField(true);
            $userName->setOperatorsVisible(false);
            $grid->addColumn($userName);
        }
        $grid->setSource($source);
        $grid->setId('id');
        $grid->setLimits([5, 10, 15]);

        $grid->hideColumns(
            ['detailsJson'
            ]
        );
        $grid->setColumnsOrder(['id', 'nameEn', 'slug',]);

        $rowAction = new RowAction('Show', 'admin_treasury_deposit_show');
        $rowAction->setRouteParameters(['id']);
        $grid->addRowAction($rowAction);

        $rowAction = new RowAction('Edit', 'admin_treasury_deposit_edit');
        $rowAction->setRouteParameters(['id']);
        $grid->addRowAction($rowAction);

        if ($grid->isReadyForRedirect()) {
            return $grid->getGridResponse();
        }

        return $this->render(
            'CasinoTreasuryBundle:DepositAdmin:index.html.twig',
            ['grid' => $grid, 'title'=>'Treasury Deposits',
             'user' => $user]
        );
    }

    /**
     * Finds and displays a Deposit entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CasinoTreasuryBundle:Deposit')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Deposit entity.');
        }

        return $this->render(
            'CasinoTreasuryBundle:DepositAdmin:show.html.twig',
            [
                'entity' => $entity,
                'title' => 'Deposit'
            ]
        );
    }

    /**
     * Finds and displays a Deposit entity.
     *
     */
    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CasinoTreasuryBundle:Deposit')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Deposit entity.');
        }
        $statuses = [];
        foreach ( $this->statuses as $k=>$v){
            $statuses[$v]=$v;
        }

        $form = $this->createFormBuilder($entity)
            ->add('amount', 'money', array('currency' => 'usd',))
            ->add('balanceBefore', 'money', array('currency' => 'usd',))
            ->add('status', 'choice',['choices'=>$statuses])
            ->add('save', 'submit')
            //->add('type', 'text')
            ->getForm();

        if ('POST' == $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $entity->setAmount($form->get('amount')->getData());
                $entity->setBalanceBefore($form->get('balanceBefore')->getData());
                $entity->setStatus($form->get('status')->getData());
//                $entity->setTo($request->get('to'));
//                $entity->setFrom($request->get('from'));

                $em->persist($entity);
                $em->flush();

                $request->getSession()->getFlashBag()->add(
                    'notice',
                    'Your changes were saved!'
                );
            } else {
                $request->getSession()->getFlashBag()->add(
                    'warning',
                    'Wrong input data!'
                );

                return $this->redirect(
                    $this->generateUrl(
                        'admin_treasury_deposit_edit',
                        ['id' => $id]
                    )
                );
            }
        }

        return $this->render(
            'CasinoTreasuryBundle:DepositAdmin:edit.html.twig',
            [
                'entity' => $entity,
                'form'=>$form->createView()
            ]
        );
    }

    /**
     * Lists all Deposit entities.
     *
     */
    public function transactionsAction($userid = null)
    {
        $user = null;
        if (!is_null($userid)) {
            $userid = intval($userid);
            /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
            $userManager = $this->get('fos_user.user_manager');
            /** @var User $user */
            $user = $userManager->findUserBy(['id' => $userid]);
            if (!is_object($user)) {
                throw new \Exception('User not found!');
            }
        }
        
        $source = new Entity('CasinoTreasuryBundle:TreasuryTransaction');

        $grid = $this->get('grid');
        $grid->setSource($source);
        $tableAlias = $source->getTableAlias();
        $source->manipulateQuery(
            function (QueryBuilder $query) use ($tableAlias, $userid) {
                $query->leftJoin( 'Casino\UserBundle\Entity\User', "user", "WITH", $tableAlias . ".user = user.id");
                //if user id is present - filter with that user
                if ( !is_null($userid) ){
                    $query->andWhere('user.id ='.$userid);
                } else {
                    $query->addSelect( 'user.username');
                }
            }
        );
        $grid->setId('id');

        if ( is_null($userid) ) {
            $userName = new TextColumn(
                [
                    'id'=>'username',
                    'title'=>'User name',
                    'field' => 'user.username',
                    // 'isManualField' => true,
                    'isAggregate' => false, // Defaults to false, set true if using aggregate func. like SUM()
                    'source' => true,
                    'filterable' => true,
                    'sortable' => true
                ]);
            $grid->addColumn($userName);
        }
        $grid->setLimits([5, 10, 15]);

        $grid->hideColumns(
            [
                'detailsJson'//, 'deposit'
            ]
        );

        $rowAction = new RowAction('Show', 'user_treasury_transactions_show');
        $rowAction->setRouteParameters(['id']);
        $grid->addRowAction($rowAction);
        $grid->getColumn('type')->manipulateRenderCell(
            function($value) {
                return ($value == TreasuryTransaction::TRANSACTION_TYPE_INGOING)?  'ingoing' : 'outgoing';
            }
        );

        if ($grid->isReadyForRedirect()) {
            return $grid->getGridResponse();
        }

        return $this->render(
            'CasinoTreasuryBundle:DepositAdmin:index.html.twig',
            ['grid' => $grid, 'title'=>'Treasury Transactions',
            'user'=>$user]
        );
    }

    /**
     * Finds and displays a Deposit entity.
     *
     */
    public function showTransactionAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CasinoTreasuryBundle:TreasuryTransaction')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Transaction entity.');
        }


        return $this->render(
            'CasinoTreasuryBundle:DepositAdmin:show.transaction.html.twig',
            [
                'entity' => $entity,
                'title' => 'Transaction'
            ]
        );
    }

    private function getGridParam($paramName, $request){
        $gridId = 'grid_id';
        $userName = $request->request->get($gridId)[$paramName]['from'];
        if (!isset($userName)){
            $gridSession = $this->get('session')->get($gridId);
            $userName = (isset($gridSession['username']))? $gridSession['username']['from'] : null;
        }
        return $userName;
    }
}
