<?php

namespace Casino\TreasuryBundle\Controller;

use Casino\TreasuryBundle\Entity\Deposit;
use Casino\TreasuryBundle\Entity\TreasuryTransaction;
use Casino\TreasuryBundle\Entity\WithdrawRequest;
use Casino\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class NetellerController extends Controller
{

    public function netellerRequestAction($amount, Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if (!is_object($user)) {
            return $this->redirect(
                $this->generateUrl(
                    'fos_user_security_login'
                )
            );
        }
        $amount = intval($amount);
        $neteller = $this->get('casino.api.curl.neteller2');
        $neteller->setUser($user);

        /*//check if customer registered on Neteller. if not - redirect to registration then - back to deposition on Neteller
        if(!$neteller->customerLookup()){
            $registerurl = $neteller->redirectRegister();
            if (is_array($registerurl)){
                if (isset($registerurl['url'])){
                   return $this->redirect($registerurl['url']);
                }
            }
        }*/

        $items = [[
            'quantity' => 1,
            'name' => 'Lotedo account deposit',
            'description' => 'lotedo.com casino deposit.',
            'amount' => $amount * 100,
        ]];

        $content = $neteller->makeOrder($items);

        // if "send" param is set  - redirect to neteller pay page
        if (!is_null($request->get('send')) &&($content['error'] == '')){
            return $this->redirect(
                $content['data']
            );
        }
        if (!empty($content['error'])) {
            $this->container->get('logger')->error('Neteller error: ' . $content['error']);
        }

        return $this->render(
            'CasinoTreasuryBundle:Merchant:index.html.twig',
            [
                'content' => $content['data'],
                'error' => $content['error'],
            ]
        );
    }

    public function netellerRedirectAction(Request $request)
    {
        $content = ['data'=>'', 'error'=>''];
        $status = $request->get('status');
        if ($status){
            $user = $this->get('security.token_storage')->getToken()->getUser();
            if (!is_object($user)) {
                return $this->redirect(
                    $this->generateUrl(
                        'fos_user_security_login'
                    )
                );
            }
            $em = $this->getDoctrine()->getManager();

            $lastDepArr = $em->getRepository('CasinoTreasuryBundle:Deposit')->findBy(['user'=>$user],['createdAt'=>'DESC'], 1);
            if ( $lastDepArr ){
                /** @var Deposit $lastDep */
                $lastDep = array_pop($lastDepArr);
                $neteller = $this->get('casino.api.curl.neteller2');
                $neteller->setUser($user);
                if ( $lastDep->getStatus() !== Deposit::STATUS_WAIT ) {
                    $content['error'] ='Your last payment processed out.';
                } else {
                    dump('trans before lookup');
                    dump($lastDep);
                    $content = $neteller->paymentLookup($lastDep);

                    //neteller let its users cancel payment even if its completed.
                    if (!empty($content['error']) && $status == 'cancel'){
                        $lastDep->setStatus(Deposit::STATUS_CANCEL);
                        $em->persist($lastDep);
                        $em->flush($lastDep);
                    }
                }
                $content['data'] = $lastDep->getStatus();
            }
        }

        return $this->render(
            'CasinoTreasuryBundle:Merchant:result.html.twig',
            [
                'content' => $content['data'],
                'error' => $content['error']
            ]
        );
    }

    public function netellerResponseAction(Request $request) {
        $data =  $request->getContent();
        $data = json_decode($data, true);
        if (!$data) {
            throw new \Exception('Malformed json.');
        }

        dump('Nete`s callback');
        dump($data);
        $links = $data['links'];
        $paymentLookupLink = null;
        foreach ($links as $link){
            if ($link['rel'] == 'payment') {
                $paymentLookupLink = $link['url'];
            }
        }
        if (!isset($paymentLookupLink)) {
            throw new \Exception('No urls provided');
        }

        $neteller = $this->get('casino.api.curl.neteller2');
        $neteller->paymentLookup($paymentLookupLink);
        return new JsonResponse();
    }

    /**
     * @param $id
     * @return array|bool
     */
    public function payoutAction($id) {
        $securityContext = $this->container->get('security.authorization_checker');
        if (!$securityContext->isGranted('ROLE_ADMIN')) {
            return $this->redirect(
                $this->generateUrl(
                    'admin_login'
                )
            );
        }

        $result = [ 'status'=>'', 'error' => '', 'transaction'=>''];
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CasinoTreasuryBundle:WithdrawRequest')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find WithdrawRequest entity.');
        }

        if (!in_array($entity->getStatus(), WithdrawRequest::$unProcessedStatuses)) {
            $result['status'] = 'This request has been already processed out.';
            $result['dep'] = is_object($entity->getDeposit())? $entity->getDeposit() : '';
        } else {
            $neteller = $this->get('casino.api.curl.neteller2');
            $neteller->setUser( $entity->getUser());
            $result = $neteller->payout($entity);
        }

        return $this->render(
            'CasinoTreasuryBundle:Merchant:resultAdmin.html.twig',
            [
                'status' => $result['status'],
                'error' => $result['error'],
                'dep' => $result['dep']
            ]
        );
    }

    public function checkPaymentStatusAction($id, Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if (!is_object($user)) {
            return $this->redirect(
                $this->generateUrl(
                    'fos_user_security_login'
                )
            );
        }
        $em = $this->getDoctrine()->getManager();
        $deposit = $em->getRepository('CasinoTreasuryBundle:Deposit')->findOneBy(['user'=>$user, 'id'=>$id]);
        if ( is_object($deposit) ) {
            $neteller = $this->get('casino.api.curl.neteller2');
            $neteller->setUser($user);
            if ( in_array($deposit->getStatus(), [Deposit::STATUS_NEW, Deposit::STATUS_WAIT]) ) {
                $content = $neteller->paymentLookup($deposit);
            } else {
                $content['error'] ='Your last deposit has been processed out already.';
            }
            $content['data'] = $deposit->getStatus();
        } else {
            throw new \Exception(404, 'Deposit not found.');
        }

        return $this->render(
            'CasinoTreasuryBundle:Merchant:result.html.twig',
            [
                'content' => $content['data'],
                'error' => $content['error']
            ]
        );
    }
}
