<?php

namespace Casino\TreasuryBundle\Controller;

use Casino\DefaultBundle\Controller\BaseAdminController;
use Symfony\Component\HttpFoundation\Request;

class SettingAdminController extends BaseAdminController
{
    public function indexAction()
    {
        return $this->render(
            'CasinoTreasuryBundle:SettingAdmin:index.html.twig'
        );
    }

    public function updateAction(Request $request, $name = 'Neteller')
    {
        $request = $this->get('request');

        if ($request->getMethod() == 'POST') {

            $this->get('craue_config')->set(
                'Neteller.config',
                serialize($request->get('gateway'))
            );

            $request->getSession()->getFlashBag()->add(
                'notice',
                'Your changes were saved!'
            );
            return $this->redirect($this->generateUrl(
                    'casino_treasury_admin_setting_update',
                    ['name'=>'Neteller']
                ));
        }

        $settings = [];
        try {
            $settings = unserialize($this->get('craue_config')->get('neteller.config'));
        } catch (\Exception $e) {

        }

        return $this->render(
            'CasinoTreasuryBundle:SettingAdmin:update.html.twig',
            [
                'gatewayName' => 'Neteller',
                'settings' => $settings
            ]
        );
    }
}
