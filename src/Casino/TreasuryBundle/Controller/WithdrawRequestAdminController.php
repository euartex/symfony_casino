<?php

namespace Casino\TreasuryBundle\Controller;

use APY\DataGridBundle\Grid\Action\RowAction;
use APY\DataGridBundle\Grid\Column\TextColumn;
use APY\DataGridBundle\Grid\Row;
use APY\DataGridBundle\Grid\Source\Entity;
use Casino\DefaultBundle\Controller\BaseAdminController;
use Casino\TreasuryBundle\Entity\WithdrawRequest;
use Casino\TreasuryBundle\Form\WithdrawRequestType;
use Casino\UserBundle\Entity\User;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\Request;

/**
 * WithdrawRequestAdmin controller.
 *
 */
class WithdrawRequestAdminController extends BaseAdminController
{
    /**
     * Lists all WithdrawRequest entities.
     *
     */
    public function indexAction($userid = null)
    {
        $user = null;
        if (!is_null($userid)) {
            $userid = intval($userid);
            /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
            $userManager = $this->get('fos_user.user_manager');
            /** @var User $user */
            $user = $userManager->findUserBy(['id' => $userid]);
            if (!is_object($user)) {
                throw new \Exception('User not found!');
            }
        }

        $source = new Entity('CasinoTreasuryBundle:WithdrawRequest');

        $tableAlias = $source->getTableAlias();
        $source->manipulateQuery(
            function (QueryBuilder $query) use ($tableAlias, $userid) {
                $query->leftJoin( 'Casino\UserBundle\Entity\User', "user", "WITH", $tableAlias . ".user = user.id");
                $query->orderBy($tableAlias . '.createdAt','DESC');
                if ( !is_null($userid) ){
                    $query->andWhere('user.id ='.$userid);
                } else {
                    $query->addSelect( 'user.username');
                }
            }
        );
        $grid = $this->get('grid');
        if ( is_null($userid) ) {
            $userName = new TextColumn(
                [
                    'id'=>'username',
                    'title'=>'User name',
                    'field' => 'user.username',
                    // 'isManualField' => true,
                    'isAggregate' => false, // Defaults to false, set true if using aggregate func. like SUM()
                    'source' => true,
                    'filterable' => true,
                    'sortable' => true
                ]);
            $grid->addColumn($userName);
        }

        $grid->setSource($source);
        $grid->setId('id');
        $grid->setLimits([5, 10, 15]);

        $grid->getColumn('status')->manipulateRenderCell(
            function($value, $row) {
                return WithdrawRequest::$statuses[$value];
            }
        );
        $grid->getColumn('amount')->manipulateRenderCell(
            function($value, $row) {
                return $value.'$';
            }
        );

        $rowAction = new RowAction('Show', 'admin_withdraw_show');
        $rowAction->setRouteParameters(['id']);
        $grid->addRowAction($rowAction);

        $rowAction = new RowAction('Edit', 'admin_withdraw_edit');
        $rowAction->setRouteParameters(['id']);
        $grid->addRowAction($rowAction);

        $rowAction = new RowAction('Payout', 'admin_withdraw_payout',true);
        $rowAction->manipulateRender(function(RowAction $action,Row $row){
            /** @var WithdrawRequest $wr */
            $wr = $row->getEntity();
            //if already processed or insufficient funds
            if ((!in_array($wr->getStatus(), WithdrawRequest::$unProcessedStatuses))
                ||
                ($wr->getUser()->getBalance() < $wr->getAmount())) {
                return null;
            }
            return $action;
        });
        $rowAction->setRouteParameters(['id']);
        $grid->addRowAction($rowAction);

        if ($grid->isReadyForRedirect()) {
            return $grid->getGridResponse();
        }

        return $this->render(
            'CasinoTreasuryBundle:WithdrawRequest:index.html.twig',
            ['grid' => $grid, 'title'=>'Treasury Deposits',
            'user'=>$user]
        );
    }


    /**
     * Creates a new WithdrawRequest entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new WithdrawRequest();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_withdraw_show', array('id' => $entity->getId())));
        }

        return $this->render('CasinoTreasuryBundle:WithdrawRequest:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a WithdrawRequest entity.
     *
     * @param WithdrawRequest $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(WithdrawRequest $entity)
    {
        $form = $this->createForm(new WithdrawRequestType(), $entity, array(
            'action' => $this->generateUrl('admin_withdraw_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new WithdrawRequest entity.
     *
     */
    public function newAction()
    {
        $entity = new WithdrawRequest();
        $form   = $this->createCreateForm($entity);

        return $this->render('CasinoTreasuryBundle:WithdrawRequest:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a WithdrawRequest entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CasinoTreasuryBundle:WithdrawRequest')->find($id);

        $entity->setStatus( WithdrawRequest::$statuses[$entity->getStatus()]);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find WithdrawRequest entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CasinoTreasuryBundle:WithdrawRequest:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to delete a WithdrawRequest entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_withdraw_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm();
    }

    /**
     * Displays a form to edit an existing WithdrawRequest entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CasinoTreasuryBundle:WithdrawRequest')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find WithdrawRequest entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CasinoTreasuryBundle:WithdrawRequest:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'id' => $id
        ));
    }

    /**
    * Creates a form to edit a WithdrawRequest entity.
    *
    * @param WithdrawRequest $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(WithdrawRequest $entity)
    {
        $form = $this->createForm(new WithdrawRequestType(), $entity, array(
            'action' => $this->generateUrl('admin_withdraw_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing WithdrawRequest entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CasinoTreasuryBundle:WithdrawRequest')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find WithdrawRequest entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('admin_withdraw_edit', array('id' => $id)));
        }

        return $this->render('CasinoTreasuryBundle:WithdrawRequest:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a WithdrawRequest entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CasinoTreasuryBundle:WithdrawRequest')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find WithdrawRequest entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_withdraw'));
    }
}
