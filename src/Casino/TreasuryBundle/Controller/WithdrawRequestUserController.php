<?php

namespace Casino\TreasuryBundle\Controller;

use Casino\TreasuryBundle\Entity\WithdrawRequest;
use Casino\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use APY\DataGridBundle\Grid\Source\Entity;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;

/**
 * Withdraw request controller.
 *
 */
class WithdrawRequestUserController extends Controller
{

    public function indexAction(Request $request)
    {
        /** @var User $user */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if (!is_object($user)) {
            return $this->redirect(
                $this->generateUrl(
                    'fos_user_security_login'
                )
            );
        }

        $em = $this->getDoctrine()->getManager();
        /** @var Form $form */
        $form = $this->container->get('form.factory')->createNamedBuilder('wrq', 'form', array(), array())
            ->setMethod('post')
            ->add('amount', 'money', array(
                'currency' => 'USD',
                'required' => true,
            ))
            ->getForm();
        $form->handleRequest($request);
        if (!$user->getIsDocsValidated()){
            $form->addError(new FormError('You must verify your person first.'));
        }

        if ($form->isValid()) {
            if (floatval($form->get('amount')->getData()) > floatval($user->getBalance()))
            {
                $form->get('amount')->addError(new FormError('Insufficient funds!'));
            } else {
                $wrq = new WithdrawRequest();
                $wrq->setStatus(WithdrawRequest::STATUS_NEW);
                $wrq->setAmount($form->get('amount')->getData());
                $wrq->setUser($user);

                $em->persist($wrq);
                $em->flush();
            }
        }

        $requests = $em->getRepository('CasinoTreasuryBundle:WithdrawRequest')->findBy(['user' => $user],['createdAt' => 'DESC']);

        $source = new Entity('CasinoTreasuryBundle:WithdrawRequest');
        $source->setData($requests);

        $grid = $this->get('grid');
        $grid->setSource($source);
        $grid->getColumn('status')->manipulateRenderCell(
            function($value, $row) {
                return WithdrawRequest::$statuses[$value];
            }
        );
        $grid->setId('id');
        $grid->setLimits([15, 30, 50]);

        $grid->hideColumns(
            [
                'id', 'changedAt'
            ]
        );

        if ($grid->isReadyForRedirect()) {
            return $grid->getGridResponse();
        }

        return $this->render(
            'CasinoTreasuryBundle:Default:withdraw.index.html.twig',
            ['grid' => $grid, 'title' => 'Withdraw requests', 'form' => $form->createView(),
             'money'=>$user->getBalance(), 'user_verifyed'=>$user->getIsDocsValidated()]
        );
    }

    /**
     * Abandons the request.
     *
     */
    public function abandonAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if (!is_object($user)) {

            return $this->redirect(
                $this->generateUrl(
                    'fos_user_security_login'
                )
            );
        }

        $wreq = $em->getRepository('CasinoTreasuryBundle:WithdrawRequest')->find($id);
        if (!$wreq) {
            throw $this->createNotFoundException('Unable to find withdrawal request.');
        }
        if ($wreq->getUser()->getId() !== $user->getId()) {
            throw $this->createNotFoundException('You has no rights to perform this action.');
        }

        $wreq->setChangedAt(new \DateTime());
        $wreq->setStatus(WithdrawRequest::STATUS_CANCEL);
        $em->persist($wreq);
        $em->flush();

        return $this->render(
            'CasinoBonusBundle:BonusUser:show.html.twig',
            [
                'entity' => $wreq,
            ]
        );
    }
}
