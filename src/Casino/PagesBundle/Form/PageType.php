<?php

namespace Casino\PagesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PageType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('slug')
            ->add('seoTitleEn')
            ->add('seoDescriptionEn')
            ->add('seoKeywordsEn')
            ->add('seoTitleEs')
            ->add('seoDescriptionEs')
            ->add('seoKeywordsEs')
            ->add('nameEn')
            ->add('nameEs')
            ->add('contentEn')
            ->add('contentEs')
            ->add('is_active')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Casino\PagesBundle\Entity\Page'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'casino_pagesbundle_page';
    }
}
