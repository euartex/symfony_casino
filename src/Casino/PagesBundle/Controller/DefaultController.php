<?php

namespace Casino\PagesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;

class DefaultController extends Controller
{
    public function indexAction($slug)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CasinoPagesBundle:Page')->findBy(['slug' => $slug]);

        if (!$entity) {
            throw new Exception('Page not found', 404);
        }

        return $this->render(
            'CasinoPagesBundle:Default:index.html.twig',
            [
                'entity' => reset($entity),
            ]
        );
    }
}
