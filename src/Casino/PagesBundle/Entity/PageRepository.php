<?php

namespace Casino\PagesBundle\Entity;

use Doctrine\ORM\EntityRepository;

class PageRepository extends EntityRepository
{
    public function findBySlug($slug)
    {
        return $this->getEntityManager()
            ->getRepository('CasinoPagesBundle:Page')
            ->createQueryBuilder('n')
            ->setParameter('slug',$slug)
            ->where('n.slug = :slug')
            ->getQuery()->getSingleResult();
    }
}