<?php

namespace Casino\PagesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="Casino\PagesBundle\Entity\PageRepository")
 * @ORM\Table(name="page")
 * @UniqueEntity(fields="slug", message="Slug already taken")
 */
class Page
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @ORM\Column(type="string", length=255) */
    protected $slug;

    /** @ORM\Column(name="seo_title_en", type="string", length=255, nullable=true) */
    protected $seoTitleEn;

    /** @ORM\Column(name="seo_description_en", type="string", length=255, nullable=true) */
    protected $seoDescriptionEn;

    /** @ORM\Column(name="seo_keywords_en", type="string", length=255, nullable=true) */
    protected $seoKeywordsEn;

    /** @ORM\Column(name="seo_title_es", type="string", length=255, nullable=true) */
    protected $seoTitleEs;

    /** @ORM\Column(name="seo_description_es", type="string", length=255, nullable=true) */
    protected $seoDescriptionEs;

    /** @ORM\Column(name="seo_keywords_es", type="string", length=255, nullable=true) */
    protected $seoKeywordsEs;

    /** @ORM\Column(name="name_en", type="string", length=255) */
    protected $nameEn;

    /** @ORM\Column(name="name_es", type="string", length=255) */
    protected $nameEs;

    /** @ORM\Column(name="content_en", type="text", nullable=true) */
    protected $contentEn = '';

    /** @ORM\Column(name="content_es", type="text", nullable=true) */
    protected $contentEs = '';

    /** @ORM\Column(type="boolean") */
    protected $is_active = true;

    /** @ORM\Column(type="datetime") */
    protected $created_at;

    public function __construct()
    {
        $this->created_at = new \DateTime("now");
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Page
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set seoTitleEn
     *
     * @param string $seoTitleEn
     * @return Page
     */
    public function setSeoTitleEn($seoTitleEn)
    {
        $this->seoTitleEn = $seoTitleEn;

        return $this;
    }

    /**
     * Get seoTitleEn
     *
     * @return string
     */
    public function getSeoTitleEn()
    {
        return $this->seoTitleEn;
    }

    /**
     * Set seoDescriptionEn
     *
     * @param string $seoDescriptionEn
     * @return Page
     */
    public function setSeoDescriptionEn($seoDescriptionEn)
    {
        $this->seoDescriptionEn = $seoDescriptionEn;

        return $this;
    }

    /**
     * Get seoDescriptionEn
     *
     * @return string
     */
    public function getSeoDescriptionEn()
    {
        return $this->seoDescriptionEn;
    }

    /**
     * Set seoKeywordsEn
     *
     * @param string $seoKeywordsEn
     * @return Page
     */
    public function setSeoKeywordsEn($seoKeywordsEn)
    {
        $this->seoKeywordsEn = $seoKeywordsEn;

        return $this;
    }

    /**
     * Get seoKeywordsEn
     *
     * @return string
     */
    public function getSeoKeywordsEn()
    {
        return $this->seoKeywordsEn;
    }

    /**
     * Set seoTitleEs
     *
     * @param string $seoTitleEs
     * @return Page
     */
    public function setSeoTitleEs($seoTitleEs)
    {
        $this->seoTitleEs = $seoTitleEs;

        return $this;
    }

    /**
     * Get seoTitleEs
     *
     * @return string
     */
    public function getSeoTitleEs()
    {
        return $this->seoTitleEs;
    }

    /**
     * Set seoDescriptionEs
     *
     * @param string $seoDescriptionEs
     * @return Page
     */
    public function setSeoDescriptionEs($seoDescriptionEs)
    {
        $this->seoDescriptionEs = $seoDescriptionEs;

        return $this;
    }

    /**
     * Get seoDescriptionEs
     *
     * @return string
     */
    public function getSeoDescriptionEs()
    {
        return $this->seoDescriptionEs;
    }

    /**
     * Set seoKeywordsEs
     *
     * @param string $seoKeywordsEs
     * @return Page
     */
    public function setSeoKeywordsEs($seoKeywordsEs)
    {
        $this->seoKeywordsEs = $seoKeywordsEs;

        return $this;
    }

    /**
     * Get seoKeywordsEs
     *
     * @return string
     */
    public function getSeoKeywordsEs()
    {
        return $this->seoKeywordsEs;
    }

    /**
     * Set nameEn
     *
     * @param string $nameEn
     * @return Page
     */
    public function setNameEn($nameEn)
    {
        $this->nameEn = $nameEn;

        return $this;
    }

    /**
     * Get nameEn
     *
     * @return string
     */
    public function getNameEn()
    {
        return $this->nameEn;
    }

    /**
     * Set nameEs
     *
     * @param string $nameEs
     * @return Page
     */
    public function setNameEs($nameEs)
    {
        $this->nameEs = $nameEs;

        return $this;
    }

    /**
     * Get nameEs
     *
     * @return string
     */
    public function getNameEs()
    {
        return $this->nameEs;
    }

    /**
     * Set contentEn
     *
     * @param string $contentEn
     * @return Page
     */
    public function setContentEn($contentEn)
    {
        $this->contentEn = $contentEn;

        return $this;
    }

    /**
     * Get contentEn
     *
     * @return string
     */
    public function getContentEn()
    {
        return $this->contentEn;
    }

    /**
     * Set contentEs
     *
     * @param string $contentEs
     * @return Page
     */
    public function setContentEs($contentEs)
    {
        $this->contentEs = $contentEs;

        return $this;
    }

    /**
     * Get contentEs
     *
     * @return string
     */
    public function getContentEs()
    {
        return $this->contentEs;
    }

    /**
     * Set is_active
     *
     * @param boolean $isActive
     * @return Page
     */
    public function setIsActive($isActive)
    {
        $this->is_active = $isActive;

        return $this;
    }

    /**
     * Get is_active
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->is_active;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Page
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function getName($ln){
        if($ln == 'es')
            return $this->getNameEs();

        return $this->getNameEn();
    }

    public function getSeoTitle($ln){
        if($ln == 'es')
            return $this->getSeoTitleEs();

        return $this->getSeoTitleEn();
    }

    public function getSeoDescription($ln){
        if($ln == 'es')
            return $this->getSeoDescriptionEs();

        return $this->getSeoDescriptionEs();
    }

    public function getSeoKeywords($ln){
        if($ln == 'es')
            return $this->getSeoKeywordsEs();

        return $this->getSeoKeywordsEn();
    }

    public function getContent($ln){
        if($ln == 'es')
            return $this->getContentEs();

        return $this->getContentEn();
    }
}
