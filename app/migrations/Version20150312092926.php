<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150312092926 extends AbstractMigration implements ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    public function up(Schema $schema)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $entity = $em->getRepository('CraueConfigBundle:Setting')->findBy(['name' => 'mailing.registration']);
        if ($entity) {
            return false;
        }
        $setting = new \Craue\ConfigBundle\Entity\Setting();
        $setting->setName('mailing.registration');
        $setting->setValue('a:2:{s:7:"subject";s:4:"Test";s:7:"content";s:24:"<p>Test registration</p>";}');
        $em->persist($setting);
        $em->flush();

    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
