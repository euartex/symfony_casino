<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160516152002 extends AbstractMigration implements ContainerAwareInterface
{
    /** @var ContainerInterface */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function up(Schema $schema)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        // spinbar option
        $key = 'spinBar';
        $entity = $em->getRepository('CraueConfigBundle:Setting')->findBy(['name' => $key]);
        if (!$entity) {
            $settingSpinBar = new \Craue\ConfigBundle\Entity\Setting();
            $settingSpinBar->setName($key);
            $settingSpinBar->setValue('a:2:{s:8:"switcher";b:1;s:4:"text";s:30:"***WELCOME TO LOTEDO CASINO***";}');
            $em->persist($settingSpinBar);
        }
        // slider option
        $key = 'main.slider';
        $entity = $em->getRepository('CraueConfigBundle:Setting')->findBy(['name' => $key]);
        if (!$entity) {
            $setting = new \Craue\ConfigBundle\Entity\Setting();
            $setting->setName($key);
            $setting->setValue('a:3:{i:0;s:19:"slider_slide_01.jpg";i:1;s:19:"slider_slide_02.jpg";i:2;s:19:"slider_slide_03.jpg";}');
            $em->persist($setting);
        }
        $em->flush();

    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
