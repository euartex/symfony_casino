<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160517152002 extends AbstractMigration implements ContainerAwareInterface
{
    /** @var ContainerInterface */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function up(Schema $schema)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $entity1 = $em->getRepository('CasinoMailingBundle:EmailTemplate')->findOneBy(['templateName' => 'en-resetpassword']);
        if ($entity1) {
            $entity1->setTemplateId('51a7d28a-7a70-4d4f-a080-5853eec59839');
            $em->persist($entity1);
        }
        $entity2 = $em->getRepository('CasinoMailingBundle:EmailTemplate')->findOneBy(['templateName' => 'en-wager-completed']);
        if ($entity2) {
            $entity2->setTemplateId('5f4b9c08-d8b8-4b82-a9dc-f6f02e9353e8');
            $em->persist($entity2);
        }
        $entity3 = $em->getRepository('CasinoMailingBundle:EmailTemplate')->findOneBy(['templateName' => 'en-activation-mail']);
        if ($entity3) {
            $entity3->setTemplateId('4396aaa0-6571-4182-99fa-cace70885cfd');
            $em->persist($entity3);
        }
        $entity4 = $em->getRepository('CasinoMailingBundle:EmailTemplate')->findOneBy(['templateName' => 'en-contacts']);
        if ($entity4) {
            $entity4->setTemplateId('f97f684d-3c9a-4013-9d6d-c6482bd76ac9');
            $em->persist($entity4);
        }
        $em->flush();

    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
