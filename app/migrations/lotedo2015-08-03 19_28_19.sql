-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.6.26-log - MySQL Community Server (GPL)
-- ОС Сервера:                   Win64
-- HeidiSQL Версия:              9.2.0.4953
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- Дамп данных таблицы lotedodb.bonuses: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `bonuses` DISABLE KEYS */;
/*!40000 ALTER TABLE `bonuses` ENABLE KEYS */;

-- Дамп данных таблицы lotedodb.craue_config_setting: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `craue_config_setting` DISABLE KEYS */;
INSERT IGNORE INTO `craue_config_setting` (`name`, `value`, `section`) VALUES
	('mailing.registration', 'a:2:{s:7:"subject";s:4:"Test";s:7:"content";s:24:"<p>Test registration</p>";}', NULL),
	('omnipay.Neteller', 'a:3:{s:10:"merchantId";s:0:"";s:11:"merchantKey";s:0:"";s:8:"testMode";s:1:"1";}', NULL);
/*!40000 ALTER TABLE `craue_config_setting` ENABLE KEYS */;

-- Дамп данных таблицы lotedodb.email_templates: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `email_templates` DISABLE KEYS */;
INSERT IGNORE INTO `email_templates` (`id`, `email_from`, `email_from_name`, `email_subject`, `vars`, `template_name`) VALUES
	(1, 'noreply@lotedo.com', 'Lotedo system', 'Password recovery', '"[\\"UserName\\",\\"RestoreAccountLink\\"]"', 'en-resetpassword'),
	(2, 'noreply@lotedo.com', 'Lotedo wager', 'You have completed your wager', '"[\\"UserName\\",\\"BonusAmount\\",\\"Route\\"]"', 'en-wager-completed');
/*!40000 ALTER TABLE `email_templates` ENABLE KEYS */;

-- Дамп данных таблицы lotedodb.game: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `game` DISABLE KEYS */;
INSERT IGNORE INTO `game` (`id`, `slug`, `icon`, `icon_big`, `seo_title_en`, `seo_description_en`, `seo_keywords_en`, `seo_title_es`, `seo_description_es`, `seo_keywords_es`, `name_en`, `name_es`, `script`, `object`, `main_position`, `is_active`, `sort`, `wage_rate`, `created_at`) VALUES
	(1, 'ruletta-all', '/games/img/r_50.png', '/assets/uploads/r_all.png', NULL, NULL, NULL, NULL, NULL, NULL, 'Ruleta', 'Ruleta', 'flashvars["game"] = "lra";\r\n                params.bgcolor = "#E38E01";\r\n                attributes.id = "ruletta";\r\n                attributes.name = "ruletta";\r\n                attributes.align = "middle";\r\n                swfobject.embedSWF(\r\n                "web/games/ruletta.swf", "flashContent",\r\n                "1280", "800",\r\n                swfVersionStr, xiSwfUrlStr,\r\n                flashvars, params, attributes);', NULL, 2, 1, 0, 100, '2015-08-03 12:41:31'),
	(2, 'ruletta-100', '/games/img/r_100.png', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Ruleta $1', 'Ruleta $1', 'flashvars["game"] = "lrb";\r\n                params.bgcolor = "#1584c8";\r\n                attributes.id = "ruletta";\r\n                attributes.name = "ruletta";\r\n                attributes.align = "middle";\r\n                swfobject.embedSWF(\r\n                "http://dev.test.creawill.com/container.swf", "flashContent",\r\n                "1280", "800",\r\n                swfVersionStr, xiSwfUrlStr,\r\n                flashvars, params, attributes);', '', 3, 1, 0, 100, '2015-08-03 12:41:31'),
	(3, 'ruletta-50', '/games/img/r_50.png', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Ruleta 50', 'Ruleta 50', 'flashvars["game"] = "lrr";\r\n                params.bgcolor = "#ca2b14";\r\n                attributes.id = "ruletta";\r\n                attributes.name = "ruletta";\r\n                attributes.align = "middle";\r\n                swfobject.embedSWF(\r\n                "http://dev.test.creawill.com/container.swf", "flashContent",\r\n                "1280", "800",\r\n                swfVersionStr, xiSwfUrlStr,\r\n                flashvars, params, attributes);', '', 4, 1, 0, 100, '2015-08-03 12:41:31'),
	(4, 'ruletta-25', '/games/img/r_25.png', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Ruleta 25', 'Ruleta 25', 'flashvars["game"] = "lrg";\r\n                params.bgcolor = "#459b38";\r\n                attributes.id = "ruletta";\r\n                attributes.name = "ruletta";\r\n                attributes.align = "middle";\r\n                swfobject.embedSWF(\r\n                "http://dev.test.creawill.com/container.swf", "flashContent",\r\n                "1280", "800",\r\n                swfVersionStr, xiSwfUrlStr,\r\n                flashvars, params, attributes);', '', 5, 1, 0, 100, '2015-08-03 12:41:31'),
	(5, 'ruletta-new-all', '/games/img/rn_all.png', '', NULL, NULL, NULL, NULL, NULL, NULL, 'NEW LUCKY RULETA', 'NEW LUCKY RULETA', 'flashvars["game"] = "lrrn";\r\n                params.bgcolor = "#E38E01";\r\n                attributes.id = "ruletta";\r\n                attributes.name = "ruletta";\r\n                attributes.align = "middle";\r\n                swfobject.embedSWF(\r\n                "http://dev.test.creawill.com/container.swf", "flashContent",\r\n                "1280", "800",\r\n                swfVersionStr, xiSwfUrlStr,\r\n                flashvars, params, attributes);', '', 1, 1, 0, 100, '2015-08-03 12:41:31'),
	(6, 'ruletta-new-25', '/games/img/rn_25.png', '', NULL, NULL, NULL, NULL, NULL, NULL, 'NEW LUCKY RULETA 25', 'NEW LUCKY RULETA 25', 'flashvars["game"] = "lrgn";\r\n                params.bgcolor = "#459b38";\r\n                attributes.id = "ruletta";\r\n                attributes.name = "ruletta";\r\n                attributes.align = "middle";\r\n                swfobject.embedSWF(\r\n                "http://dev.test.creawill.com/container.swf", "flashContent",\r\n                "1280", "800",\r\n                swfVersionStr, xiSwfUrlStr,\r\n                flashvars, params, attributes);', '', 6, 1, 0, 100, '2015-08-03 12:41:31'),
	(7, 'ruletta-new-100', '/games/img/rn_100.png', '', NULL, NULL, NULL, NULL, NULL, NULL, 'NEW LUCKY RULETA $1', 'NEW LUCKY RULETA $1', 'flashvars["game"] = "lrbn";\r\n                params.bgcolor = "#1584c8";\r\n                attributes.id = "ruletta";\r\n                attributes.name = "ruletta";\r\n                attributes.align = "middle";\r\n                swfobject.embedSWF(\r\n                "http://dev.test.creawill.com/container.swf", "flashContent",\r\n                "1280", "800",\r\n                swfVersionStr, xiSwfUrlStr,\r\n                flashvars, params, attributes);', '', 7, 1, 0, 100, '2015-08-03 12:41:31'),
	(8, 'fairytale-ruletta', '/games/img/ftr.png', '', NULL, NULL, NULL, NULL, NULL, NULL, 'FAIRYTALE RULETA', 'FAIRYTALE RULETA', 'flashvars["game"] = "lran";\r\n                params.bgcolor = "#E38E01";\r\n                attributes.id = "ruletta";\r\n                attributes.name = "ruletta";\r\n                attributes.align = "middle";\r\n                swfobject.embedSWF(\r\n                "http://dev.test.creawill.com/container.swf", "flashContent",\r\n                "1280", "800",\r\n                swfVersionStr, xiSwfUrlStr,\r\n                flashvars, params, attributes);', '', 8, 1, 0, 100, '2015-08-03 12:41:31');
/*!40000 ALTER TABLE `game` ENABLE KEYS */;

-- Дамп данных таблицы lotedodb.game_log: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `game_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `game_log` ENABLE KEYS */;

-- Дамп данных таблицы lotedodb.mailing: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `mailing` DISABLE KEYS */;
INSERT IGNORE INTO `mailing` (`id`, `template_id`, `title`, `slug`, `is_active`) VALUES
	(1, 1, 'test', 'en-resetpassword', 1),
	(2, 2, 'Wager completion', 'en-wager-completed', 1);
/*!40000 ALTER TABLE `mailing` ENABLE KEYS */;

-- Дамп данных таблицы lotedodb.mailing_groups: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `mailing_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `mailing_groups` ENABLE KEYS */;

-- Дамп данных таблицы lotedodb.news: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
/*!40000 ALTER TABLE `news` ENABLE KEYS */;

-- Дамп данных таблицы lotedodb.page: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `page` DISABLE KEYS */;
INSERT IGNORE INTO `page` (`id`, `slug`, `seo_title_en`, `seo_description_en`, `seo_keywords_en`, `seo_title_es`, `seo_description_es`, `seo_keywords_es`, `name_en`, `name_es`, `content_en`, `content_es`, `is_active`, `created_at`) VALUES
	(1, 'about', NULL, NULL, NULL, NULL, NULL, NULL, 'About us', 'About us', 'About us Page', 'About us Page', 1, '2015-08-03 12:41:32'),
	(2, 'contact', NULL, NULL, NULL, NULL, NULL, NULL, 'Contacts', 'Contacts', 'Contacts Page', 'Contacts Page', 1, '2015-08-03 12:41:32'),
	(3, 'deposit', NULL, NULL, NULL, NULL, NULL, NULL, 'Deposit options', 'Deposit options', 'Deposit options Page', 'Deposit options Page', 1, '2015-08-03 12:41:32'),
	(4, 'promo', NULL, NULL, NULL, NULL, NULL, NULL, 'PROMOTIONS', 'PROMOTIONS', 'PROMOTIONS Page', 'PROMOTIONS Page', 1, '2015-08-03 12:41:32');
/*!40000 ALTER TABLE `page` ENABLE KEYS */;

-- Дамп данных таблицы lotedodb.promocode: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `promocode` DISABLE KEYS */;
/*!40000 ALTER TABLE `promocode` ENABLE KEYS */;

-- Дамп данных таблицы lotedodb.promocode_conds_groups: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `promocode_conds_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `promocode_conds_groups` ENABLE KEYS */;

-- Дамп данных таблицы lotedodb.promocode_conds_not_groups: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `promocode_conds_not_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `promocode_conds_not_groups` ENABLE KEYS */;

-- Дамп данных таблицы lotedodb.promocode_conds_not_users: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `promocode_conds_not_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `promocode_conds_not_users` ENABLE KEYS */;

-- Дамп данных таблицы lotedodb.promocode_conds_users: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `promocode_conds_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `promocode_conds_users` ENABLE KEYS */;

-- Дамп данных таблицы lotedodb.treasury_deposit: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `treasury_deposit` DISABLE KEYS */;
/*!40000 ALTER TABLE `treasury_deposit` ENABLE KEYS */;

-- Дамп данных таблицы lotedodb.treasury_deposit_bonuses: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `treasury_deposit_bonuses` DISABLE KEYS */;
/*!40000 ALTER TABLE `treasury_deposit_bonuses` ENABLE KEYS */;

-- Дамп данных таблицы lotedodb.treasury_transactions: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `treasury_transactions` DISABLE KEYS */;
/*!40000 ALTER TABLE `treasury_transactions` ENABLE KEYS */;

-- Дамп данных таблицы lotedodb.treasury_withdraw: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `treasury_withdraw` DISABLE KEYS */;
/*!40000 ALTER TABLE `treasury_withdraw` ENABLE KEYS */;

-- Дамп данных таблицы lotedodb.users: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT IGNORE INTO `users` (`id`, `current_bonus`, `affiliated_by`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`, `photo`, `nickname`, `tel`, `gender`, `balance`, `create_date`, `last_activity`, `network`, `network_uid`, `social_data`, `bonusBalance`, `api_token`, `first_name`, `last_name`, `birth_day`, `address`) VALUES
	(1, NULL, NULL, 'share', 'share', '1337.um@gmail.com', '1337.um@gmail.com', 1, 'rlnwxmeqapwgw0ksw4k448gkg08woco', '0gke7VZCeFx17AvCKoJPeJ8r0n76QJtH/EqyZsIJdA9gt8M5jPhtKw+m4uoeap9oo30AagMo7ZEL0Jl0RfgG4g==', '2015-08-03 19:22:18', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, NULL, 'share', NULL, -1, 0.00, '2015-08-03 12:30:44', '2015-08-03 19:25:59', NULL, NULL, 'N;', 0.00, NULL, NULL, NULL, NULL, ''),
	(2, NULL, NULL, 'admin3', 'admin3', 'admin@lotedo.nodeart.io', 'admin@lotedo.nodeart.io', 1, '8m878uetz604wk88kccowcsccs0w8ws', 'PmhWUR/sFn7xcQK9vs/x2xgpbGBXY+T0r+jzjYm+ubwEdlpD2dapjyw6vT/yXynIyeAlNaRhQ/pV8XHtHhIh8w==', '2015-08-03 18:16:20', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:16:"ROLE_SUPER_ADMIN";}', 0, NULL, NULL, 'admin', NULL, -1, 0.00, '2015-08-03 12:37:21', '2015-08-03 19:04:27', NULL, NULL, 'N;', 0.00, NULL, NULL, NULL, NULL, '');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Дамп данных таблицы lotedodb.users_bonuses: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `users_bonuses` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_bonuses` ENABLE KEYS */;

-- Дамп данных таблицы lotedodb.users_groups: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `users_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_groups` ENABLE KEYS */;

-- Дамп данных таблицы lotedodb.user_address: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `user_address` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_address` ENABLE KEYS */;

-- Дамп данных таблицы lotedodb.user_affiliate: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `user_affiliate` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_affiliate` ENABLE KEYS */;

-- Дамп данных таблицы lotedodb.user_group: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `user_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_group` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
