<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Static pages
 */
class Version20150122213046 extends AbstractMigration implements ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function up(Schema $schema)
    {
        $this->addPage('about', 'About us', 'About us Page');
        $this->addPage('contact', 'Contacts', 'Contacts Page');
        $this->addPage('deposit', 'Deposit options', 'Deposit options Page');
        $this->addPage('promo', 'PROMOTIONS', 'PROMOTIONS Page');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    protected function addPage($slug, $name, $content)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $entity = $em->getRepository('CasinoPagesBundle:Page')->findBy(['slug' => $slug]);
        if ($entity) {
            return false;
        }
        $page = new \Casino\PagesBundle\Entity\Page();

        $page->setSlug($slug);
        $page->setNameEn($name);
        $page->setNameEs($name);
        $page->setContentEn($content);
        $page->setContentEs($content);

        $em->persist($page);
        $em->flush();
    }
}
