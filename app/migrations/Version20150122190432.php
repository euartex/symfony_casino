<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Creating default games
 */
class Version20150122190432 extends AbstractMigration implements ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function up(Schema $schema)
    {
        $this->addGame(
            'ruletta-all',
            [
                'name'     => 'Ruleta',
                'icon'     => '/games/img/r_50.png',
                'iconBig'  => '/assets/uploads/r_all.png',
                'position' => 2,
                'script'   => 'flashvars["game"] = "lra";
                params.bgcolor = "#E38E01";
                attributes.id = "ruletta";
                attributes.name = "ruletta";
                attributes.align = "middle";
                swfobject.embedSWF(
                "http://dev.test.creawill.com/container.swf", "flashContent",
                "1280", "800",
                swfVersionStr, xiSwfUrlStr,
                flashvars, params, attributes);'
            ]
        );

        $this->addGame(
            'ruletta-100',
            [
                'name'     => 'Ruleta $1',
                'icon'     => '/games/img/r_100.png',
                'iconBig'  => '',
                'position' => 3,
                'script'   => 'flashvars["game"] = "lrb";
                params.bgcolor = "#1584c8";
                attributes.id = "ruletta";
                attributes.name = "ruletta";
                attributes.align = "middle";
                swfobject.embedSWF(
                "http://dev.test.creawill.com/container.swf", "flashContent",
                "1280", "800",
                swfVersionStr, xiSwfUrlStr,
                flashvars, params, attributes);'
            ]
        );

        $this->addGame(
            'ruletta-50',
            [
                'name'     => 'Ruleta 50',
                'icon'     => '/games/img/r_50.png',
                'iconBig'  => '',
                'position' => 4,
                'script'   => 'flashvars["game"] = "lrr";
                params.bgcolor = "#ca2b14";
                attributes.id = "ruletta";
                attributes.name = "ruletta";
                attributes.align = "middle";
                swfobject.embedSWF(
                "http://dev.test.creawill.com/container.swf", "flashContent",
                "1280", "800",
                swfVersionStr, xiSwfUrlStr,
                flashvars, params, attributes);'
            ]
        );

        $this->addGame(
            'ruletta-25',
            [
                'name'     => 'Ruleta 25',
                'icon'     => '/games/img/r_25.png',
                'iconBig'  => '',
                'position' => 5,
                'script'   => 'flashvars["game"] = "lrg";
                params.bgcolor = "#459b38";
                attributes.id = "ruletta";
                attributes.name = "ruletta";
                attributes.align = "middle";
                swfobject.embedSWF(
                "http://dev.test.creawill.com/container.swf", "flashContent",
                "1280", "800",
                swfVersionStr, xiSwfUrlStr,
                flashvars, params, attributes);'
            ]
        );

        $this->addGame(
            'ruletta-new-all',
            [
                'name'     => 'NEW LUCKY RULETA',
                'icon'     => '/games/img/rn_all.png',
                'iconBig'  => '',
                'position' => 1,
                'script'   => 'flashvars["game"] = "lrrn";
                params.bgcolor = "#E38E01";
                attributes.id = "ruletta";
                attributes.name = "ruletta";
                attributes.align = "middle";
                swfobject.embedSWF(
                "http://dev.test.creawill.com/container.swf", "flashContent",
                "1280", "800",
                swfVersionStr, xiSwfUrlStr,
                flashvars, params, attributes);'
            ]
        );

        $this->addGame(
            'ruletta-new-25',
            [
                'name'     => 'NEW LUCKY RULETA 25',
                'icon'     => '/games/img/rn_25.png',
                'iconBig'  => '',
                'position' => 6,
                'script'   => 'flashvars["game"] = "lrgn";
                params.bgcolor = "#459b38";
                attributes.id = "ruletta";
                attributes.name = "ruletta";
                attributes.align = "middle";
                swfobject.embedSWF(
                "http://dev.test.creawill.com/container.swf", "flashContent",
                "1280", "800",
                swfVersionStr, xiSwfUrlStr,
                flashvars, params, attributes);'
            ]
        );

        $this->addGame(
            'ruletta-new-100',
            [
                'name'     => 'NEW LUCKY RULETA $1',
                'icon'     => '/games/img/rn_100.png',
                'iconBig'  => '',
                'position' => 7,
                'script'   => 'flashvars["game"] = "lrbn";
                params.bgcolor = "#1584c8";
                attributes.id = "ruletta";
                attributes.name = "ruletta";
                attributes.align = "middle";
                swfobject.embedSWF(
                "http://dev.test.creawill.com/container.swf", "flashContent",
                "1280", "800",
                swfVersionStr, xiSwfUrlStr,
                flashvars, params, attributes);'
            ]
        );

        $this->addGame(
            'fairytale-ruletta',
            [
                'name'     => 'FAIRYTALE RULETA',
                'icon'     => '/games/img/ftr.png',
                'iconBig'  => '',
                'position' => 8,
                'script'   => 'flashvars["game"] = "lran";
                params.bgcolor = "#E38E01";
                attributes.id = "ruletta";
                attributes.name = "ruletta";
                attributes.align = "middle";
                swfobject.embedSWF(
                "http://dev.test.creawill.com/container.swf", "flashContent",
                "1280", "800",
                swfVersionStr, xiSwfUrlStr,
                flashvars, params, attributes);'
            ]
        );
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    protected function addGame($slug, $data)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $entity = $em->getRepository('CasinoGameBundle:Game')->findBy(['slug' => $slug]);
        if ($entity) {
            return false;
        }
        $game = new \Casino\GameBundle\Entity\Game();

        $game->setSlug($slug);
        $game->setNameEn($data['name']);
        $game->setNameEs($data['name']);
        $game->setIcon($data['icon']);
        $game->setIconBig($data['iconBig']);
        $game->setMainPosition($data['position']);
        $game->setScript($data['script']);
        $game->setObject('');

        $em->persist($game);
        $em->flush();
    }
}
