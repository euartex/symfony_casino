-- phpMyAdmin SQL Dump
-- version 4.0.10.6
-- http://www.phpmyadmin.net
--
-- Хост: 192.168.88.28:3306
-- Время создания: Авг 05 2015 г., 19:41
-- Версия сервера: 5.5.41-log
-- Версия PHP: 5.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `lotedodb`
--
CREATE DATABASE IF NOT EXISTS `lotedodb` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `lotedodb`;

-- --------------------------------------------------------

--
-- Структура таблицы `bonuses`
--

DROP TABLE IF EXISTS `bonuses`;
CREATE TABLE IF NOT EXISTS `bonuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code_id` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `reason_msg` text COLLATE utf8_unicode_ci,
  `provider` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_public` tinyint(1) NOT NULL,
  `wage_rate` int(11) DEFAULT NULL,
  `buy_rate` int(11) DEFAULT NULL,
  `game_rate` int(11) NOT NULL,
  `name` tinytext COLLATE utf8_unicode_ci,
  `amount` decimal(10,2) DEFAULT NULL,
  `price` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8535CFD227DAFE17` (`code_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `bonuses`
--

INSERT INTO `bonuses` (`id`, `code_id`, `type`, `reason_msg`, `provider`, `is_public`, `wage_rate`, `buy_rate`, `game_rate`, `name`, `amount`, `price`) VALUES
(1, NULL, 'fixed_val', NULL, NULL, 1, 200, 100, 100, 'bonus #1', '25.00', '10.00');

-- --------------------------------------------------------

--
-- Структура таблицы `craue_config_setting`
--

DROP TABLE IF EXISTS `craue_config_setting`;
CREATE TABLE IF NOT EXISTS `craue_config_setting` (
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `section` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `UNIQ_B95BA9425E237E06` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `craue_config_setting`
--

INSERT INTO `craue_config_setting` (`name`, `value`, `section`) VALUES
('mailing.registration', 'a:2:{s:7:"subject";s:4:"Test";s:7:"content";s:24:"<p>Test registration</p>";}', NULL),
('omnipay.Neteller', 'a:3:{s:10:"merchantId";s:0:"";s:11:"merchantKey";s:0:"";s:8:"testMode";s:1:"1";}', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `email_templates`
--

DROP TABLE IF EXISTS `email_templates`;
CREATE TABLE IF NOT EXISTS `email_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email_from` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_from_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vars` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json_array)',
  `template_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `email_templates`
--

INSERT INTO `email_templates` (`id`, `email_from`, `email_from_name`, `email_subject`, `vars`, `template_name`) VALUES
(1, 'noreply@lotedo.com', 'Lotedo system', 'Password recovery', '"[\\"UserName\\",\\"RestoreAccountLink\\"]"', 'en-resetpassword'),
(2, 'noreply@lotedo.com', 'Lotedo wager', 'You have completed your wager', '"[\\"UserName\\",\\"BonusAmount\\",\\"Route\\"]"', 'en-wager-completed'),
(3, 'noreply@lotedo.com', 'Lotedo Casino', 'Welcome to Lotedo casino!', '"[\\"UserName\\",\\"Route\\"]"', 'en-activation-mail');

-- --------------------------------------------------------

--
-- Структура таблицы `game`
--

DROP TABLE IF EXISTS `game`;
CREATE TABLE IF NOT EXISTS `game` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `icon_big` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_title_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_keywords_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_title_es` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description_es` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_keywords_es` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_es` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `script` longtext COLLATE utf8_unicode_ci,
  `object` longtext COLLATE utf8_unicode_ci,
  `main_position` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  `sort` int(11) NOT NULL,
  `wage_rate` decimal(10,0) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Дамп данных таблицы `game`
--

INSERT INTO `game` (`id`, `slug`, `icon`, `icon_big`, `seo_title_en`, `seo_description_en`, `seo_keywords_en`, `seo_title_es`, `seo_description_es`, `seo_keywords_es`, `name_en`, `name_es`, `script`, `object`, `main_position`, `is_active`, `sort`, `wage_rate`, `created_at`) VALUES
(1, 'ruletta-all', '/games/img/r_50.png', '/assets/uploads/r_all.png', NULL, NULL, NULL, NULL, NULL, NULL, 'Ruleta', 'Ruleta', 'flashvars["game"] = "lra";\r\n                params.bgcolor = "#E38E01";\r\n                attributes.id = "ruletta";\r\n                attributes.name = "ruletta";\r\n                attributes.align = "middle";\r\n                swfobject.embedSWF(\r\n                "web/games/ruletta.swf", "flashContent",\r\n                "1280", "800",\r\n                swfVersionStr, xiSwfUrlStr,\r\n                flashvars, params, attributes);', NULL, 2, 1, 0, '100', '2015-08-03 12:41:31'),
(2, 'ruletta-100', '/games/img/r_100.png', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Ruleta $1', 'Ruleta $1', 'flashvars["game"] = "lrb";\r\n                params.bgcolor = "#1584c8";\r\n                attributes.id = "ruletta";\r\n                attributes.name = "ruletta";\r\n                attributes.align = "middle";\r\n                swfobject.embedSWF(\r\n                "http://dev.test.creawill.com/container.swf", "flashContent",\r\n                "1280", "800",\r\n                swfVersionStr, xiSwfUrlStr,\r\n                flashvars, params, attributes);', '', 3, 1, 0, '100', '2015-08-03 12:41:31'),
(3, 'ruletta-50', '/games/img/r_50.png', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Ruleta 50', 'Ruleta 50', 'flashvars["game"] = "lrr";\r\n                params.bgcolor = "#ca2b14";\r\n                attributes.id = "ruletta";\r\n                attributes.name = "ruletta";\r\n                attributes.align = "middle";\r\n                swfobject.embedSWF(\r\n                "http://dev.test.creawill.com/container.swf", "flashContent",\r\n                "1280", "800",\r\n                swfVersionStr, xiSwfUrlStr,\r\n                flashvars, params, attributes);', '', 4, 1, 0, '100', '2015-08-03 12:41:31'),
(4, 'ruletta-25', '/games/img/r_25.png', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Ruleta 25', 'Ruleta 25', 'flashvars["game"] = "lrg";\r\n                params.bgcolor = "#459b38";\r\n                attributes.id = "ruletta";\r\n                attributes.name = "ruletta";\r\n                attributes.align = "middle";\r\n                swfobject.embedSWF(\r\n                "http://dev.test.creawill.com/container.swf", "flashContent",\r\n                "1280", "800",\r\n                swfVersionStr, xiSwfUrlStr,\r\n                flashvars, params, attributes);', '', 5, 1, 0, '100', '2015-08-03 12:41:31'),
(5, 'ruletta-new-all', '/games/img/rn_all.png', '', NULL, NULL, NULL, NULL, NULL, NULL, 'NEW LUCKY RULETA', 'NEW LUCKY RULETA', 'flashvars["game"] = "lrrn";\r\n                params.bgcolor = "#E38E01";\r\n                attributes.id = "ruletta";\r\n                attributes.name = "ruletta";\r\n                attributes.align = "middle";\r\n                swfobject.embedSWF(\r\n                "http://dev.test.creawill.com/container.swf", "flashContent",\r\n                "1280", "800",\r\n                swfVersionStr, xiSwfUrlStr,\r\n                flashvars, params, attributes);', '', 1, 1, 0, '100', '2015-08-03 12:41:31'),
(6, 'ruletta-new-25', '/games/img/rn_25.png', '', NULL, NULL, NULL, NULL, NULL, NULL, 'NEW LUCKY RULETA 25', 'NEW LUCKY RULETA 25', 'flashvars["game"] = "lrgn";\r\n                params.bgcolor = "#459b38";\r\n                attributes.id = "ruletta";\r\n                attributes.name = "ruletta";\r\n                attributes.align = "middle";\r\n                swfobject.embedSWF(\r\n                "http://dev.test.creawill.com/container.swf", "flashContent",\r\n                "1280", "800",\r\n                swfVersionStr, xiSwfUrlStr,\r\n                flashvars, params, attributes);', '', 6, 1, 0, '100', '2015-08-03 12:41:31'),
(7, 'ruletta-new-100', '/games/img/rn_100.png', '', NULL, NULL, NULL, NULL, NULL, NULL, 'NEW LUCKY RULETA $1', 'NEW LUCKY RULETA $1', 'flashvars["game"] = "lrbn";\r\n                params.bgcolor = "#1584c8";\r\n                attributes.id = "ruletta";\r\n                attributes.name = "ruletta";\r\n                attributes.align = "middle";\r\n                swfobject.embedSWF(\r\n                "http://dev.test.creawill.com/container.swf", "flashContent",\r\n                "1280", "800",\r\n                swfVersionStr, xiSwfUrlStr,\r\n                flashvars, params, attributes);', '', 7, 1, 0, '100', '2015-08-03 12:41:31'),
(8, 'fairytale-ruletta', '/games/img/ftr.png', '', NULL, NULL, NULL, NULL, NULL, NULL, 'FAIRYTALE RULETA', 'FAIRYTALE RULETA', 'flashvars["game"] = "lran";\r\n                params.bgcolor = "#E38E01";\r\n                attributes.id = "ruletta";\r\n                attributes.name = "ruletta";\r\n                attributes.align = "middle";\r\n                swfobject.embedSWF(\r\n                "http://dev.test.creawill.com/container.swf", "flashContent",\r\n                "1280", "800",\r\n                swfVersionStr, xiSwfUrlStr,\r\n                flashvars, params, attributes);', '', 8, 1, 0, '100', '2015-08-03 12:41:31');

-- --------------------------------------------------------

--
-- Структура таблицы `game_log`
--

DROP TABLE IF EXISTS `game_log`;
CREATE TABLE IF NOT EXISTS `game_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `game_id` int(11) DEFAULT NULL,
  `type` tinyint(1) DEFAULT NULL,
  `dep_obj_id` bigint(20) DEFAULT NULL,
  `token` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `balance_before` decimal(10,2) DEFAULT NULL,
  `balance_after` decimal(10,2) DEFAULT NULL,
  `balance_delta` decimal(10,2) DEFAULT NULL,
  `status` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `changed_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_94657B00A76ED395` (`user_id`),
  KEY `IDX_94657B00E48FD905` (`game_id`),
  KEY `token_index` (`token`),
  KEY `dep_bonus_idx` (`dep_obj_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `game_log`
--

INSERT INTO `game_log` (`id`, `user_id`, `game_id`, `type`, `dep_obj_id`, `token`, `balance_before`, `balance_after`, `balance_delta`, `status`, `changed_at`, `created_at`) VALUES
(1, 1, 1, 0, NULL, '1', '0.00', '10.00', '10.00', 'initial', '2015-08-04 12:43:23', '2015-08-04 12:43:25');

-- --------------------------------------------------------

--
-- Структура таблицы `mailing`
--

DROP TABLE IF EXISTS `mailing`;
CREATE TABLE IF NOT EXISTS `mailing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_3ED9315E5DA0FB8` (`template_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `mailing`
--

INSERT INTO `mailing` (`id`, `template_id`, `title`, `slug`, `is_active`) VALUES
(1, 1, 'password resetting', 'en-resetpassword', 1),
(2, 2, 'Wager completion', 'en-wager-completed', 1),
(3, 1, 'Email activation', 'en-activation-mail', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `mailing_groups`
--

DROP TABLE IF EXISTS `mailing_groups`;
CREATE TABLE IF NOT EXISTS `mailing_groups` (
  `mailing_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`mailing_id`,`group_id`),
  KEY `IDX_21830AAB3931AB76` (`mailing_id`),
  KEY `IDX_21830AABFE54D947` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
CREATE TABLE IF NOT EXISTS `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `migration_versions`
--

INSERT INTO `migration_versions` (`version`) VALUES
('20150122190432'),
('20150122213046'),
('20150122213847'),
('20150312092926');

-- --------------------------------------------------------

--
-- Структура таблицы `news`
--

DROP TABLE IF EXISTS `news`;
CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `page`
--

DROP TABLE IF EXISTS `page`;
CREATE TABLE IF NOT EXISTS `page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_title_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_keywords_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_title_es` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description_es` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_keywords_es` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_es` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content_en` longtext COLLATE utf8_unicode_ci,
  `content_es` longtext COLLATE utf8_unicode_ci,
  `is_active` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `page`
--

INSERT INTO `page` (`id`, `slug`, `seo_title_en`, `seo_description_en`, `seo_keywords_en`, `seo_title_es`, `seo_description_es`, `seo_keywords_es`, `name_en`, `name_es`, `content_en`, `content_es`, `is_active`, `created_at`) VALUES
(1, 'about', NULL, NULL, NULL, NULL, NULL, NULL, 'About us', 'About us', 'About us Page', 'About us Page', 1, '2015-08-03 12:41:32'),
(2, 'contact', NULL, NULL, NULL, NULL, NULL, NULL, 'Contacts', 'Contacts', 'Contacts Page', 'Contacts Page', 1, '2015-08-03 12:41:32'),
(3, 'deposit', NULL, NULL, NULL, NULL, NULL, NULL, 'Deposit options', 'Deposit options', 'Deposit options Page', 'Deposit options Page', 1, '2015-08-03 12:41:32'),
(4, 'promo', NULL, NULL, NULL, NULL, NULL, NULL, 'PROMOTIONS', 'PROMOTIONS', 'PROMOTIONS Page', 'PROMOTIONS Page', 1, '2015-08-03 12:41:32');

-- --------------------------------------------------------

--
-- Структура таблицы `promocode`
--

DROP TABLE IF EXISTS `promocode`;
CREATE TABLE IF NOT EXISTS `promocode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bonus_id` int(11) DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `use_times` int(11) NOT NULL,
  `cond_start_date` datetime NOT NULL,
  `cond_end_date` datetime NOT NULL,
  `use_time_consts` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_7C786E0669545666` (`bonus_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `promocode_conds_groups`
--

DROP TABLE IF EXISTS `promocode_conds_groups`;
CREATE TABLE IF NOT EXISTS `promocode_conds_groups` (
  `id1` int(11) NOT NULL,
  `id2` int(11) NOT NULL,
  PRIMARY KEY (`id1`,`id2`),
  UNIQUE KEY `UNIQ_C30BD3E7101D69E` (`id2`),
  KEY `IDX_C30BD3EE8088724` (`id1`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `promocode_conds_not_groups`
--

DROP TABLE IF EXISTS `promocode_conds_not_groups`;
CREATE TABLE IF NOT EXISTS `promocode_conds_not_groups` (
  `id1` int(11) NOT NULL,
  `id2` int(11) NOT NULL,
  PRIMARY KEY (`id1`,`id2`),
  UNIQUE KEY `UNIQ_35BF8D5B7101D69E` (`id2`),
  KEY `IDX_35BF8D5BE8088724` (`id1`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `promocode_conds_not_users`
--

DROP TABLE IF EXISTS `promocode_conds_not_users`;
CREATE TABLE IF NOT EXISTS `promocode_conds_not_users` (
  `id1` int(11) NOT NULL,
  `id2` int(11) NOT NULL,
  PRIMARY KEY (`id1`,`id2`),
  UNIQUE KEY `UNIQ_7C0C30517101D69E` (`id2`),
  KEY `IDX_7C0C3051E8088724` (`id1`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `promocode_conds_users`
--

DROP TABLE IF EXISTS `promocode_conds_users`;
CREATE TABLE IF NOT EXISTS `promocode_conds_users` (
  `id1` int(11) NOT NULL,
  `id2` int(11) NOT NULL,
  PRIMARY KEY (`id1`,`id2`),
  UNIQUE KEY `UNIQ_F08F97B27101D69E` (`id2`),
  KEY `IDX_F08F97B2E8088724` (`id1`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `treasury_deposit`
--

DROP TABLE IF EXISTS `treasury_deposit`;
CREATE TABLE IF NOT EXISTS `treasury_deposit` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `balance_before` decimal(10,2) NOT NULL,
  `balance_after` decimal(10,2) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `status` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `from_money` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `to_money` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `detailsJson` tinytext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_648113DAA76ED395` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `treasury_deposit_bonuses`
--

DROP TABLE IF EXISTS `treasury_deposit_bonuses`;
CREATE TABLE IF NOT EXISTS `treasury_deposit_bonuses` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `deposit_id` bigint(20) DEFAULT NULL,
  `bonus_id` int(11) DEFAULT NULL,
  `promo_id` int(11) DEFAULT NULL,
  `balance_before` decimal(10,2) NOT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `changed_at` datetime DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_cashed_out` tinyint(1) NOT NULL,
  `expire_reason` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lifespan` datetime DEFAULT NULL,
  `wage` decimal(10,2) DEFAULT NULL,
  `total_wage` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_89C8D4D8A76ED395` (`user_id`),
  KEY `IDX_89C8D4D8D0C07AFF` (`promo_id`),
  KEY `FK_treasury_deposit_bonuses_bonuses` (`bonus_id`),
  KEY `FK_treasury_deposit_bonuses_treasury_deposit` (`deposit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `treasury_transactions`
--

DROP TABLE IF EXISTS `treasury_transactions`;
CREATE TABLE IF NOT EXISTS `treasury_transactions` (
  `user_id` int(11) DEFAULT NULL,
  `deposit_id` bigint(20) DEFAULT NULL,
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `created_at` datetime NOT NULL,
  `gateway` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `changed_at` datetime NOT NULL,
  `currency` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `order_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_err_msg` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_completed` tinyint(1) NOT NULL,
  `details_json` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json_array)',
  PRIMARY KEY (`Id`),
  KEY `IDX_E4D61DD1A76ED395` (`user_id`),
  KEY `IDX_E4D61DD19815E4B1` (`deposit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `treasury_withdraw`
--

DROP TABLE IF EXISTS `treasury_withdraw`;
CREATE TABLE IF NOT EXISTS `treasury_withdraw` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `deposit_id` bigint(20) DEFAULT NULL,
  `amount` decimal(10,2) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `changed_at` datetime NOT NULL,
  `details` tinytext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8C2CB6D29815E4B1` (`deposit_id`),
  KEY `IDX_8C2CB6D2A76ED395` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `current_bonus` bigint(20) DEFAULT NULL,
  `affiliated_by` int(11) DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nickname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tel` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` int(11) NOT NULL,
  `balance` decimal(10,2) NOT NULL,
  `create_date` datetime NOT NULL,
  `last_activity` datetime NOT NULL,
  `network` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `network_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `social_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:array)',
  `bonusBalance` decimal(10,2) NOT NULL,
  `api_token` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birth_day` date DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_1483A5E992FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_1483A5E9A0D96FBF` (`email_canonical`),
  UNIQUE KEY `UNIQ_1483A5E93B86087E` (`current_bonus`),
  KEY `IDX_1483A5E9BA080F8F` (`affiliated_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `current_bonus`, `affiliated_by`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`, `photo`, `nickname`, `tel`, `gender`, `balance`, `create_date`, `last_activity`, `network`, `network_uid`, `social_data`, `bonusBalance`, `api_token`, `first_name`, `last_name`, `birth_day`, `address`) VALUES
(1, NULL, NULL, 'share', 'share', '1337.um@gmail.com', '1337.um@gmail.com', 1, 'rlnwxmeqapwgw0ksw4k448gkg08woco', '0gke7VZCeFx17AvCKoJPeJ8r0n76QJtH/EqyZsIJdA9gt8M5jPhtKw+m4uoeap9oo30AagMo7ZEL0Jl0RfgG4g==', '2015-08-04 19:56:02', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, NULL, 'share', NULL, -1, '0.00', '2015-08-03 12:30:44', '2015-08-04 19:56:04', NULL, NULL, 'N;', '0.00', NULL, NULL, NULL, NULL, ''),
(2, NULL, NULL, 'admin3', 'admin3', 'admin@lotedo.nodeart.io', 'admin@lotedo.nodeart.io', 1, '8m878uetz604wk88kccowcsccs0w8ws', 'PmhWUR/sFn7xcQK9vs/x2xgpbGBXY+T0r+jzjYm+ubwEdlpD2dapjyw6vT/yXynIyeAlNaRhQ/pV8XHtHhIh8w==', '2015-08-05 13:15:22', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:16:"ROLE_SUPER_ADMIN";}', 0, NULL, NULL, 'admin', NULL, -1, '0.00', '2015-08-03 12:37:21', '2015-08-05 18:16:39', NULL, NULL, 'N;', '0.00', NULL, NULL, NULL, NULL, ''),
(3, NULL, NULL, 'test', 'test', 'test@example.com', 'test@example.com', 1, 'ib47fz6zfq0cswgk84koo8884o0g0ck', 'W+JW7LrMkqqmN3OuZuYi72DUmOSOxrImcLX1MvhJzvoSckQm92NtkOSxgg/77Xi7LXMxAk4lsEN7IaaOHaV5Ww==', '2015-08-04 15:20:25', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, NULL, 'test', NULL, -1, '0.00', '2015-08-04 15:18:57', '2015-08-04 15:20:26', NULL, NULL, 'N;', '0.00', NULL, NULL, NULL, NULL, ''),
(4, NULL, NULL, 'test3', 'test3', '1338.um@gmail.com', '1338.um@gmail.com', 0, 'caakxsovyqokoggwggwc0gg0w0s4w0c', '5hbpbWxxoyfvDJQjIHmPKg3exjG/k+Nlj9z1vs0/k2/oHE73L5stPHyIFTkax5FDfNP7LmlCcAI31PQlEbr/Ig==', NULL, 0, 0, NULL, '01HBaKPVohkevhqeAS5Xd3qJ8LUaD0FLJJwgbLlxKJQ', NULL, 'a:0:{}', 0, NULL, NULL, 'test3', NULL, -1, '0.00', '2015-08-04 17:25:48', '2015-08-04 17:25:48', NULL, NULL, 'N;', '0.00', NULL, NULL, NULL, NULL, ''),
(5, NULL, NULL, 'qwe', 'qwe', 'qwe@qwe.qwe', 'qwe@qwe.qwe', 0, 'rlkj2p3lqdc4k8o88wgc488o0k4o8k8', 'mq3Pe7kR7PXoe4Ey+nEmzGH6A+3a5JKMzMhb9tBF8eaGJdTq2KMAwmwSk3MhDpZMjsRblI0dpGwWOMfstUSx/g==', NULL, 0, 0, NULL, '3r4KzQt5VSd_GY4yvR6wemnoDECmRMDui3aljG2f5n0', NULL, 'a:0:{}', 0, NULL, NULL, 'qwe', NULL, -1, '0.00', '2015-08-04 19:38:36', '2015-08-04 19:38:36', NULL, NULL, 'N;', '0.00', NULL, NULL, NULL, NULL, '');

-- --------------------------------------------------------

--
-- Структура таблицы `users_bonuses`
--

DROP TABLE IF EXISTS `users_bonuses`;
CREATE TABLE IF NOT EXISTS `users_bonuses` (
  `user_id` int(11) NOT NULL,
  `bonus_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`bonus_id`),
  KEY `IDX_7535BB18A76ED395` (`user_id`),
  KEY `IDX_7535BB1869545666` (`bonus_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `users_groups`
--

DROP TABLE IF EXISTS `users_groups`;
CREATE TABLE IF NOT EXISTS `users_groups` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`group_id`),
  KEY `IDX_FF8AB7E0A76ED395` (`user_id`),
  KEY `IDX_FF8AB7E0FE54D947` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `user_address`
--

DROP TABLE IF EXISTS `user_address`;
CREATE TABLE IF NOT EXISTS `user_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `street_addr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zipcode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_5543718BA76ED395` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `user_affiliate`
--

DROP TABLE IF EXISTS `user_affiliate`;
CREATE TABLE IF NOT EXISTS `user_affiliate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `promo_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_599A282BA76ED395` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `user_group`
--

DROP TABLE IF EXISTS `user_group`;
CREATE TABLE IF NOT EXISTS `user_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `bonuses`
--
ALTER TABLE `bonuses`
  ADD CONSTRAINT `FK_8535CFD227DAFE17` FOREIGN KEY (`code_id`) REFERENCES `promocode` (`id`);

--
-- Ограничения внешнего ключа таблицы `game_log`
--
ALTER TABLE `game_log`
  ADD CONSTRAINT `FK_94657B00A76ED395` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK_94657B00E48FD905` FOREIGN KEY (`game_id`) REFERENCES `game` (`id`);

--
-- Ограничения внешнего ключа таблицы `mailing`
--
ALTER TABLE `mailing`
  ADD CONSTRAINT `FK_3ED9315E5DA0FB8` FOREIGN KEY (`template_id`) REFERENCES `email_templates` (`id`);

--
-- Ограничения внешнего ключа таблицы `mailing_groups`
--
ALTER TABLE `mailing_groups`
  ADD CONSTRAINT `FK_21830AAB3931AB76` FOREIGN KEY (`mailing_id`) REFERENCES `mailing` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_21830AABFE54D947` FOREIGN KEY (`group_id`) REFERENCES `user_group` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `promocode`
--
ALTER TABLE `promocode`
  ADD CONSTRAINT `FK_7C786E0669545666` FOREIGN KEY (`bonus_id`) REFERENCES `bonuses` (`id`);

--
-- Ограничения внешнего ключа таблицы `promocode_conds_groups`
--
ALTER TABLE `promocode_conds_groups`
  ADD CONSTRAINT `FK_C30BD3E7101D69E` FOREIGN KEY (`id2`) REFERENCES `user_group` (`id`),
  ADD CONSTRAINT `FK_C30BD3EE8088724` FOREIGN KEY (`id1`) REFERENCES `promocode` (`id`);

--
-- Ограничения внешнего ключа таблицы `promocode_conds_not_groups`
--
ALTER TABLE `promocode_conds_not_groups`
  ADD CONSTRAINT `FK_35BF8D5B7101D69E` FOREIGN KEY (`id2`) REFERENCES `user_group` (`id`),
  ADD CONSTRAINT `FK_35BF8D5BE8088724` FOREIGN KEY (`id1`) REFERENCES `promocode` (`id`);

--
-- Ограничения внешнего ключа таблицы `promocode_conds_not_users`
--
ALTER TABLE `promocode_conds_not_users`
  ADD CONSTRAINT `FK_7C0C30517101D69E` FOREIGN KEY (`id2`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK_7C0C3051E8088724` FOREIGN KEY (`id1`) REFERENCES `promocode` (`id`);

--
-- Ограничения внешнего ключа таблицы `promocode_conds_users`
--
ALTER TABLE `promocode_conds_users`
  ADD CONSTRAINT `FK_F08F97B27101D69E` FOREIGN KEY (`id2`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK_F08F97B2E8088724` FOREIGN KEY (`id1`) REFERENCES `promocode` (`id`);

--
-- Ограничения внешнего ключа таблицы `treasury_deposit`
--
ALTER TABLE `treasury_deposit`
  ADD CONSTRAINT `FK_648113DAA76ED395` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ограничения внешнего ключа таблицы `treasury_deposit_bonuses`
--
ALTER TABLE `treasury_deposit_bonuses`
  ADD CONSTRAINT `FK_89C8D4D869545666` FOREIGN KEY (`bonus_id`) REFERENCES `bonuses` (`id`),
  ADD CONSTRAINT `FK_89C8D4D89815E4B1` FOREIGN KEY (`deposit_id`) REFERENCES `treasury_deposit` (`id`),
  ADD CONSTRAINT `FK_89C8D4D8A76ED395` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK_89C8D4D8D0C07AFF` FOREIGN KEY (`promo_id`) REFERENCES `promocode` (`id`);

--
-- Ограничения внешнего ключа таблицы `treasury_transactions`
--
ALTER TABLE `treasury_transactions`
  ADD CONSTRAINT `FK_E4D61DD19815E4B1` FOREIGN KEY (`deposit_id`) REFERENCES `treasury_deposit` (`id`),
  ADD CONSTRAINT `FK_E4D61DD1A76ED395` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ограничения внешнего ключа таблицы `treasury_withdraw`
--
ALTER TABLE `treasury_withdraw`
  ADD CONSTRAINT `FK_8C2CB6D29815E4B1` FOREIGN KEY (`deposit_id`) REFERENCES `treasury_deposit` (`id`),
  ADD CONSTRAINT `FK_8C2CB6D2A76ED395` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ограничения внешнего ключа таблицы `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `FK_1483A5E93B86087E` FOREIGN KEY (`current_bonus`) REFERENCES `treasury_deposit_bonuses` (`id`),
  ADD CONSTRAINT `FK_1483A5E9BA080F8F` FOREIGN KEY (`affiliated_by`) REFERENCES `user_affiliate` (`id`);

--
-- Ограничения внешнего ключа таблицы `users_bonuses`
--
ALTER TABLE `users_bonuses`
  ADD CONSTRAINT `FK_7535BB1869545666` FOREIGN KEY (`bonus_id`) REFERENCES `bonuses` (`id`),
  ADD CONSTRAINT `FK_7535BB18A76ED395` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ограничения внешнего ключа таблицы `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `FK_FF8AB7E0A76ED395` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_FF8AB7E0FE54D947` FOREIGN KEY (`group_id`) REFERENCES `user_group` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `user_address`
--
ALTER TABLE `user_address`
  ADD CONSTRAINT `FK_5543718BA76ED395` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ограничения внешнего ключа таблицы `user_affiliate`
--
ALTER TABLE `user_affiliate`
  ADD CONSTRAINT `FK_599A282BA76ED395` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
