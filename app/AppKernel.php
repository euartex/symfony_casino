<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function __construct($environment, $debug)
    {
        date_default_timezone_set( 'America/New_York' );
        parent::__construct($environment, $debug);
    }

    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new FOS\UserBundle\FOSUserBundle(),
            new Knp\Bundle\MenuBundle\KnpMenuBundle(),
            new APY\DataGridBundle\APYDataGridBundle(),
            new Craue\ConfigBundle\CraueConfigBundle(),
            //new Laelaps\GearmanBundle\GearmanBundle(),
            new Hip\MandrillBundle\HipMandrillBundle(),
            new EWZ\Bundle\RecaptchaBundle\EWZRecaptchaBundle(),
            new Vich\UploaderBundle\VichUploaderBundle(),

            new Casino\DefaultBundle\CasinoDefaultBundle(),
            new Casino\UserBundle\CasinoUserBundle(),
            new Casino\NewsBundle\CasinoNewsBundle(),
            new Casino\MediaBundle\CasinoMediaBundle(),
            new Casino\GameBundle\CasinoGameBundle(),
            new Casino\PagesBundle\CasinoPagesBundle(),
            new Casino\TreasuryBundle\CasinoTreasuryBundle(),
            new Casino\MailingBundle\CasinoMailingBundle(),
            new Casino\BonusBundle\CasinoBonusBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }
}
